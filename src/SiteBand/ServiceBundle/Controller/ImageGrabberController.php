<?php

namespace SiteBand\ServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class ImageGrabberController extends Controller
{
    public function indexAction()
    {

        return $this->render('SiteBandServiceBundle:ImageGrabber:index.html.twig');
    }

    public function downloadAction()
    {
        $request = Request::createFromGlobals();
        $startPos = (int)trim($request->request->get('startPos'));
        $endPos = (int)trim($request->request->get('endPos'));
        $countTo = (int)trim($request->request->get('countTo'));
        $outputVar = array();
        $outputVar['startPos'] = $startPos;
        $outputVar['endPos'] = $endPos;
        $outputVar['countTo'] = $countTo;

        if (is_int($startPos) && is_int($endPos) && is_int($countTo))
        {

        }
        else
        {
            $error = "Неправильный формат значений. Все значения должны быть цифрами.";
            $outputVar['error'] = $error;
        }






        return $this->render('SiteBandServiceBundle:ImageGrabber:download.html.twig',$outputVar);
    }


    public function ajaxDownloadAction()
    {
        sleep(1);
        $request = Request::createFromGlobals();
        $url = trim($request->request->get('url'));
        $fileName = trim($request->request->get('fileName'));

        $img = '/home/srv36936/Symfony/upload_images/'.$fileName.'.jpeg';
        file_put_contents($img, file_get_contents($url));


        $response = array("code" => 100, "success" => true, "url" => $url, "fileName" => $fileName);
        return new Response(json_encode($response));
    }
}
