<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 11.12.13
 * Time: 16:04
 */

namespace SiteBand\ServiceBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder {
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class','nav navbar-nav');


        $menu->addChild('Home', array('route' => 'site_band_service_homepage'));
        $menu->addChild('Image Grabber', array(
            'route' => 'sbs_ig_index' ));
        return $menu;
    }
} 