<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 10.09.2014
 * Time: 15:47
 */

namespace SiteBand\WitnessBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SiteBand\WitnessBundle\Entity\Dictionary;
use JMS\SerializerBundle\JMSSerializerBundle;
use SiteBand\WitnessBundle\Utils\Helper;

class DictionaryCommand extends ContainerAwareCommand{

    protected function configure()
    {
        $this
            ->setName('siteband:dictjson')
            ->setDescription('Get json data from dictionary entity and put it to file');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName = 'titletags.json';
        $kernel = $this->getApplication()->getKernel();//$this->getContainer('kernel');
        $path = $kernel->getRootDir().'/../web/bundles/witness/json/'.$fileName;


        $repo = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository('SiteBandWitnessBundle:Dictionary');

        $dicts = $repo->findBy(array('dictionary' => 'Заголовок-тег'));


        $dictDataArray = array();
        foreach ($dicts as $dict)
        {
            $dictDataArray[] = array(
            'name' => $dict->getName()
            ,'value' => $dict->getName()
            );

        }
        // yes, here we are retrieving "_format" from routing. In our case it's json
        $serializedEntity = $this->getContainer()->get('serializer')->serialize($dictDataArray, 'json');
        $format = "json";//$request->getRequestFormat();

        $serializedEntityUnicodeToUtf = Helper::json_encode_cyr($serializedEntity);

        $output->write($serializedEntityUnicodeToUtf);
        file_put_contents($path, $serializedEntityUnicodeToUtf);

    }
}