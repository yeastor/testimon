<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 21.01.14
 * Time: 15:03
 */


namespace SiteBand\WitnessBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SiteBand\WitnessBundle\Entity\Event;
use SiteBand\WitnessBundle\Entity\LogEventLastId;
use SiteBand\WitnessBundle\Entity\XMLEvent;
use SiteBand\WitnessBundle\Utils\Helper;

class EventXMLCommand extends ContainerAwareCommand{
    protected function configure()
    {
        $this
            ->setName('siteband:eventtoxml')
            ->setDescription('Aggregate events to xml per day')
            ->addOption('force', null, InputOption::VALUE_NONE, 'If set, the task will get all events');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $helper = new Helper();

        $logEventLastIdRepo = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository(
            'SiteBandWitnessBundle:LogEventLastId'
        );
        $logEventLastDate = $logEventLastIdRepo->getMaxUpdateDate();
        //var_dump($logEventLastId);

        if ($input->getOption('force')) {
            $eventDateFrom = $helper->convertStringDateToDatetimeByFormat('0000-00-00 00:00:00', 'Y-m-d H:i:s');
        } else {

            $eventDateFrom = $logEventLastDate[0]['max_date'] ? $logEventLastDate[0]['max_date'] : '0000-00-00 00:00:00';
            // $output->writeln($eventDateFrom);
            $eventDateFrom = $helper->convertStringDateToDatetimeByFormat($eventDateFrom, 'Y-m-d H:i:s');
        }

        $eventRepo = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository('SiteBandWitnessBundle:Event');
        $eventMaxUpdateDate = $eventRepo->getMaxUpdateDateId();

        $eventDateTo = $eventMaxUpdateDate[0]['max_update_date'] ? $eventMaxUpdateDate[0]['max_update_date'] : '0000-00-00 00:00:00';
       // $output->writeln($eventDateTo);
        $eventDateTo = $helper->convertStringDateToDatetimeByFormat($eventDateTo,'Y-m-d H:i:s');



        if($eventDateFrom < $eventDateTo)
        {
            $groupedEvents = $eventRepo->findGroupedDateByDate($eventDateFrom,$eventDateTo);

            foreach($groupedEvents as $event)
            {
                $kernel = $this->getApplication()->getKernel();//$this->getContainer('kernel');
                $path = $kernel->locateResource('@SiteBandWitnessBundle/Resources/public/xsl/entitytoymapsml.xsl');

                $date = $event['date'];
                $output->writeln($date);

                $dateOnly = $helper->convertStringDateToDatetimeByFormat($date,'Y-m-d');
                $dateFrom = $helper->convertStringDateToDatetimeByFormat($date,'Y-m-d')->setTime(0,0,0);
                $dateTo = $helper->convertStringDateToDatetimeByFormat($date,'Y-m-d')->setTime(23,59,59);


                //$this->getContainer()->get('doctrine.orm.entity_manager')->getRepository('SiteBandWitnessBundle:Event')
                $events = $eventRepo
                    ->findByBetweenDate($dateFrom,$dateTo);


                //$this->getContainer()->get('doctrine.orm.entity_manager')->getRepository('SiteBandWitnessBundle:Event')
                $xml = $eventRepo
                    ->getXml($events);


                $XML = new \DOMDocument();
                $XML->loadXML( $xml );

# START XSLT
                $xslt = new \XSLTProcessor();
                $XSL = new \DOMDocument();


                $XSL->load( $path, LIBXML_NOCDATA);
                $xslt->importStylesheet( $XSL );
                $transformedXml = $xslt->transformToXML( $XML );

                $em = $this->getContainer()->get('doctrine.orm.entity_manager');
                $XMLEvent = $em->getRepository("SiteBandWitnessBundle:XMLEvent")->findOneBy(array ('date' => $dateOnly));
                if (!$XMLEvent)
                {
                    $XMLEvent = new XMLEvent();
                    $XMLEvent->setDate($dateOnly);
                    $XMLEvent->setXml($transformedXml);

                    $em->persist($XMLEvent);
                }
                else
                {
                    $XMLEvent->setDate($dateOnly);
                    $XMLEvent->setXml($transformedXml);
                }
                $em->flush();



#PRINT
                //print $xml->saveXML(); exit;
                // yes, here we are retrieving "_format" from routing. In our case it's json
                //$serializedEntity = $this->container->get('serializer')->serialize($eventData, 'yaml');




            }
            $em = $this->getContainer()->get('doctrine.orm.entity_manager');
            $logEventLastId = new LogEventLastId();
            $logEventLastId->setLastEvenUpdateDate($eventDateTo);
            $em->persist($logEventLastId);
            $em->flush();


        }
        else {
            $output->writeln("all events up to date");
        }


    }
} 