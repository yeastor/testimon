<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 16.12.2014
 * Time: 17:12
 */

namespace SiteBand\WitnessBundle\XML;


interface XMLedInterface {

    /**
     * @param mixed $obj
     * @return resource
     */
    public function getXml($obj);

    /**
     * @param mixed $obj
     * @return string
     */
    public function getXmlText($obj);
}