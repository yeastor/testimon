<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 16.12.2014
 * Time: 17:14
 */

namespace SiteBand\WitnessBundle\XML;

use SiteBand\WitnessBundle\Entity\Event;

class EventXML implements XMLedInterface {


    public function getXml($event)
    {
            return $event->getXML();
    }

    public function getXmlText($event)
    {
        return stream_get_contents($event->getXML());
    }
}