<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 17.12.2014
 * Time: 13:07
 */

namespace SiteBand\WitnessBundle\XML;


use Symfony\Component\HttpKernel\Kernel;

class FEventXML implements XMLedInterface {

    private $path;
    private $XSL;
    private $xslt;


    public function __construct($xslPath,Kernel $kernel)
    {
        $this->xslt = new \XSLTProcessor();
        $this->XSL = new \DOMDocument();
        $this->path = $kernel->locateResource($xslPath);
    }

    public function getXml($event)
    {
        return $event->getXML();
    }

    public function getXmlText($xmlText)
    {
        $this->XSL->load( $this->path, LIBXML_NOCDATA);
        $this->xslt->importStylesheet( $this->XSL );

        $XML = new \DOMDocument();
        $XML->loadXML($xmlText);
        return $transformedXml = $this->xslt->transformToXML( $XML );
    }
}