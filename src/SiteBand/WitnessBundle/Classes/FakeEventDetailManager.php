<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 30.12.2014
 * Time: 11:02
 */

namespace SiteBand\WitnessBundle\Classes;


use Doctrine\ORM\EntityManager;
use SiteBand\WitnessBundle\Entity\FakeEvent;
use SiteBand\WitnessBundle\Entity\FakeEventDetail;
use Doctrine\ORM\EntityRepository;

class FakeEventDetailManager {
    private $repository;
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('SiteBandWitnessBundle:FakeEventDetail');
    }

    /**
     * @param string $hash
     * @return FakeEventDetail
     */
    public function findOneByHash($hash)
    {
        return $this->repository->findOneByHash($hash);
    }

    /**
     * @param array $criteria
     * @return null|FakeEventDetail
     */
    public function findOneBy($criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param FakeEventDetail $fakeEventDetail
     */
    public function save(FakeEventDetail $fakeEventDetail)
    {
        $this->em->persist($fakeEventDetail);
        $this->em->flush();
    }

    /**
     * check if FakeDetail in search date
     * @param FakeEventDetail $fakeEventDetail
     * @param $dateFrom
     * @param null $dateTo
     * @return bool
     */
    public function isInDate(FakeEventDetail $fakeEventDetail,$dateFrom,$dateTo = null)
    {
        $valid = false;
        $fakeEventData = $fakeEventDetail->getDate();
        if ($dateTo)
        {
            if ($fakeEventData >= $dateFrom && $fakeEventData <= $dateTo){
                $valid = true;
            }

        }
        else
        {
            if ($fakeEventData == $dateFrom){
                $valid = true;
            }
        }
        return $valid;
    }
    /**
     * @param array $criteria
     * @return FakeEventDetail
     */
    public function findOneByBetweenDate(array $criteria)
    {
        if (!$res = $this->repository->findOneByBetweenDate($criteria)) return null;
        return $res[0];
    }


    /**
     * @param FakeEvent $fakeEvent
     * @param string $coords
     * @param string $address
     * @param \DateTime $date
     * @param string $hash
     */
    public function add($fakeEvent,$coords,$address,$date,$hash){
        $fakeEventDetails = new FakeEventDetail();
        $fakeEventDetails->setAddress($address);
        $fakeEventDetails->setCoords($coords);
        $fakeEventDetails->setDate($date);
        $fakeEventDetails->setHash($hash);
        $fakeEventDetails->setFakeEvent($fakeEvent);
        $this->save($fakeEventDetails);
    }

}