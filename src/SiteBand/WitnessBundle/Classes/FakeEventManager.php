<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 30.12.2014
 * Time: 11:48
 */

namespace SiteBand\WitnessBundle\Classes;

use SiteBand\WitnessBundle\Entity\FakeEvent;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
class FakeEventManager extends EntityRepository {
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('SiteBandWitnessBundle:FakeEvent');
    }

    /**
     * @param mixed $id
     * @return null|FakeEvent
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }


    /**
     * @return FakeEvent
     */
    public function getRandomFEvent()
    {
        return $this->repository->getRandomFEvent();
    }

    /**
     * @param FakeEvent $fakeEvent
     * @param string $coords
     * @param string $address
     * @param string $date
     * @return string
     */
    public function getXML($fakeEvent,$coords,$address,$date)
    {
        return $this->repository->getXML($fakeEvent,$coords,$address,$date);
    }


}