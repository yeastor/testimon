<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 24.10.2014
 * Time: 16:59
 */
namespace SiteBand\WitnessBundle\Mailer;

use FOS\UserBundle\Mailer\TwigSwiftMailer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SiteBand\WitnessBundle\Entity\Event;

class UserMailer  extends TwigSwiftMailer{

    public function  __construct(\Swift_Mailer $mailer, UrlGeneratorInterface $router, \Twig_Environment $twig, array $parameters){

        parent::__construct($mailer,$router,$twig,$parameters);
    }

    public function sendNewCommentEmailMessage(Event $event)
    {
        $user = $event->getUser();
        $template = $this->parameters['template']['new-comment'];
        $url = $this->router->generate('site_band_witness_event_get',array('id' => $event->getId()), true);
        $context = array(
            'user' => $user,
            'link' => $url
        );
        $this->sendMessage($template, $context, $this->parameters['from_email']['new-comment'], $user->getEmail());


    }
} 