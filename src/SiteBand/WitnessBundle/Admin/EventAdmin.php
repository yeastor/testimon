<?php
/**
 * Created by PhpStorm.
 * User: KUpriyanovym
 * Date: 02.06.2014
 * Time: 13:28
 */
namespace SiteBand\WitnessBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class EventAdmin extends Admin {
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('date' ,'date',
                array('label' => 'Date', 'widget' => 'single_text', 'format' => 'dd/MM/yyyy','attr' => array ('data-date-format' => "dd/mm/yyyy"
                ,'class' => 'form-control datepicker'
                )
                )
            )
            //->add('country','entity',array('label' => 'Country', 'class' => 'SiteBand\WitnessBundle\Entity\Country','property' => 'name' ))
            ->add('user', 'entity', array('class' => 'SiteBand\UserBundle\Entity\User'))
            //->add('city','entity',  array('class' => 'SiteBand\WitnessBundle\Entity\City','property' => 'name'))
            ->add('address','text')

            ->add('coords', 'text')

            //->add('isOnMap', 'hidden', array("mapped" => false, 'constraints' => array(new NotBlank(array('message' => 'Установите точку на карте так, что бы Вы её видели')) )))
            //->add('addressCenter', 'hidden', array("mapped" => false,'data' => $this->defaultAddressCenter,'attr' => array ('class' => "coords")))
            ->add('title','text', array('label' => 'Title'))
            ->add('description','textarea', array('label' => 'Description'))
            ->add('predate' ,'date',
                array('label' => 'Pre date','required' => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy','attr' => array ('data-date-format' => "dd/mm/yyyy"
                ,'class' => 'form-control datepicker'
                )

                )
            )
        ;


           //$formMapper->add('comments' ,'textarea', array('label' => 'Comment text', 'class' => 'SiteBand\WitnessBundle\Entity\Comment'));
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('user')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('date')
            ->add('user')
            ->add('updated')
        ;
    }

} 