<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 10.09.2014
 * Time: 14:46
 */

namespace SiteBand\WitnessBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class DictionaryAdmin extends Admin {
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

            ->add('name','text')

            ->add('dictionary', 'text')


            ->add('sortOrder','integer', array('label' => 'Order'))
        ;

    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('dictionary')
            ->add('sortOrder')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('dictionary')
            ->add('sortOrder')
        ;
    }

} 