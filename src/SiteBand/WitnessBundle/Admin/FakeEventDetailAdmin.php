<?php
/**
 * Created by PhpStorm.
 * User: Kupriyanovym
 * Date: 21.01.2015
 * Time: 14:01
 */

namespace SiteBand\WitnessBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FakeEventDetailAdmin extends Admin {
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

            ->add('hash')
            ->add('address')
            ->add('date')
            ->add('coords')
            ->add('updated');


    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('hash')
            ->add('updated')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('hash')
            ->add('updated')
        ;
    }

}