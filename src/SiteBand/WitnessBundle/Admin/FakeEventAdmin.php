<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 11.12.2014
 * Time: 16:09
 */

namespace SiteBand\WitnessBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FakeEventAdmin extends Admin {
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

            ->add('title','text')

            ->add('description', 'text');

    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('description')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('description')
            ->add('updated')
        ;
    }

} 