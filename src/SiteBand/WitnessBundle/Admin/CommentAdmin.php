<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 06.11.2014
 * Time: 12:39
 */

namespace SiteBand\WitnessBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CommentAdmin  extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('text','text')
            //->add('country','entity',array('label' => 'Country', 'class' => 'SiteBand\WitnessBundle\Entity\Country','property' => 'name' ))
            ->add('event','entity', array('class' => 'SiteBand\WitnessBundle\Entity\Event','property' => 'title'))
            ->add('user', 'entity', array('class' => 'SiteBand\UserBundle\Entity\User'))
            ->add('updated','date');
            //->add('city','entity',  array('class' => 'SiteBand\WitnessBundle\Entity\City','property' => 'name'))

        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('updated')
            ->add('text');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('text')
            ->add('updated')
            ->add('event', null, array('associated_property' => 'title'));
    }
}