<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 28.01.14
 * Time: 11:59
 */

namespace SiteBand\WitnessBundle\EventListener;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;

class OnConnect {
    public function postConnect( $event )
    {
        $conn = $event->getConnection();
        $conn->executeQuery("SET NAMES UTF8");
    }
}

