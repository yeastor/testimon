<?php

namespace SiteBand\WitnessBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SiteBandWitnessExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);



        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('admin.yml');

        $container->setAlias('site_band_witness.user_mailer', $config['service']['mailer']);
        $this->loadComment($config,$container,$config['comment']['from_email']);

    }

    private function loadComment(array $config, ContainerBuilder $container,  array $fromEmail)
    {
        $container->setParameter('site_band_witness.comment.notification.from_email', array($fromEmail['address'] => $fromEmail['sender_name']));
    }
}
