<?php

namespace SiteBand\WitnessBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {

        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('site_band_witness');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $this->addCommentSection($rootNode);
        $this->addServiceSection($rootNode);

        return $treeBuilder;
    }

    private function addCommentSection(ArrayNodeDefinition $node)
    {
        $node
        ->children()
            ->arrayNode('comment')
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('from_email')
                ->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('address')->defaultValue('webmaster@example.com')->isRequired()->cannotBeEmpty()->end()
                    ->scalarNode('sender_name')->defaultValue('webmaster')->cannotBeEmpty()->end()
                    ->end()
                ->end()
            ->end()
        ->end()
        ;

    }

    private function addServiceSection(ArrayNodeDefinition $node)
    {
        $node
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('service')
                ->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('mailer')->defaultValue('site_band_witness.user_mailer.default')->end()
                ->end()
            ->end();
    }
}
