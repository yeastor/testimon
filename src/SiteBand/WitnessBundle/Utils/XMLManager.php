<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 16.12.2014
 * Time: 16:41
 */

namespace SiteBand\WitnessBundle\Utils;


use SiteBand\WitnessBundle\XML\XMLedInterface;

class XMLManager {

    protected $xmlObj;

    public function __construct($version = "1.0", $encode = "UTF-8")
    {
        $this->xmlObj = new \DOMDocument();

    }
    public function loadXML($xmltring)
    {
        $this->xmlObj->loadXML($xmltring);
    }

    public function loadXMLFromStream($stream)
    {
        $this->xmlObj->loadXML(stream_get_contents($stream));
    }
    public function getFirstElementsByTagName($tagName)
    {
        return $this->xmlObj->getElementsByTagName($tagName)->item(0);
    }
    public function getElementsByTagName($nodename)
    {
        return $this->xmlObj->getElementsByTagName($nodename);
    }

    public function appendXmlChildFromObjectArray($xmledArray,$toNodeName,$nodesName,XMLedInterface $xmled)
    {
        foreach($xmledArray as $xmledObj){


            if ($xmledObj)
            {
                //$featureMembers =  $this->getFirstElementsByTagName($toNodeName);
                $xmlCurrentObj = new XMLManager(); //\DOMDocument();
                $xmlCurrentObj->loadXML($xmled->getXmlText($xmledObj));
                $nodesToInsert = $xmlCurrentObj->getElementsByTagName($nodesName);

                for ($i = 0; $i < $nodesToInsert->length; $i++) {
                    $nodeToInsert = $nodesToInsert->item($i);

                    $allDataInNode = $this->xmlObj->importNode($nodeToInsert,true);


                    $this->getFirstElementsByTagName($toNodeName)->appendChild($allDataInNode);

                }


            }
        }
    }

    public function saveXML()
    {
        return $this->xmlObj->saveXML();
    }

}