<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 17.12.2014
 * Time: 15:23
 */

namespace SiteBand\WitnessBundle\Utils;


class XMLEventManager extends XMLManager{

    private $rootNode;

    public function __construct($version = "1.0", $encode = "UTF-8")
    {
        parent::__construct($version,$encode);

        $this->rootNode = $this->xmlObj->createElement("events");

    }

    public function createEventsNode(array $elementArray)
    {
        $xml_event = $this->xmlObj->createElement("event");

        foreach($elementArray as $elementKey => $elementValue)
        {
            $xml_event->appendChild($this->xmlObj->createElement($elementKey , $elementValue));
        }


        return $this->rootNode->appendChild($xml_event);
    }

    public function appendRoot()
    {
        $this->xmlObj->appendChild($this->rootNode);
    }
}