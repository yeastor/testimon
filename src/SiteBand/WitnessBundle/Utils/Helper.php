<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 17.01.14
 * Time: 12:16
 */

namespace SiteBand\WitnessBundle\Utils;


use SiteBand\WitnessBundle\Entity\Event;

class Helper {
    public static function json_encode_cyr($str) {
        $arr_replace_utf = array('\u0410', '\u0430','\u0411','\u0431','\u0412','\u0432',
            '\u0413','\u0433','\u0414','\u0434','\u0415','\u0435','\u0401','\u0451','\u0416',
            '\u0436','\u0417','\u0437','\u0418','\u0438','\u0419','\u0439','\u041a','\u043a',
            '\u041b','\u043b','\u041c','\u043c','\u041d','\u043d','\u041e','\u043e','\u041f',
            '\u043f','\u0420','\u0440','\u0421','\u0441','\u0422','\u0442','\u0423','\u0443',
            '\u0424','\u0444','\u0425','\u0445','\u0426','\u0446','\u0427','\u0447','\u0428',
            '\u0448','\u0429','\u0449','\u042a','\u044a','\u042d','\u044b','\u042c','\u044c',
            '\u042d','\u044d','\u042e','\u044e','\u042f','\u044f');
        $arr_replace_cyr = array('А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
            'Ё', 'ё', 'Ж','ж','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н','О','о',
            'П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш',
            'Щ','щ','Ъ','ъ','Ы','ы','Ь','ь','Э','э','Ю','ю','Я','я');

        $str2 = str_replace($arr_replace_utf,$arr_replace_cyr,$str);
        $str2 = str_replace("\\/","/",$str2);
        return $str2;
    }

    /**
     * @param $stringDate
     * @param $dateFormat string of stringDate
     * @return \DateTime
     */
    public function convertStringDateToDatetimeByFormat($stringDate,$dateFormat)
    {
        $dateFormat = $dateFormat;
        return $date = \DateTime::createFromFormat($dateFormat,$stringDate );
    }

    /**
     * @param $dateTime
     * @param $dateFormat string output date format
     * @return string
     */
    public function convertDatetimeToStringByFormat($dateTime,$dateFormat)
    {

        return $date = $dateTime->format($dateFormat);
    }

    public function embedTags($text)
    {
        // The Regular Expression filter
        $reg_exUrl = "/\s((((http|https|ftp|ftps):\/\/)|www.)[a-zA-Z0-9-.]+\.[a-zA-Z]{2,4}(\/?\S*))/";

    // The Text you want to filter for URLs


    // add leading and trailing spaces they serve as URL delimiters in case
    // the URL is at the very beginning or end of $text
        $text = " ".$text." ";

    // Check if there is a url in the text
        if(preg_match_all($reg_exUrl, $text, $url)) {
            // make the URL hyper links
            $matches = array_unique($url[1]);

            foreach($matches as $match) {
                $replacement = "<a href='".$match."' target='_blank'>".$match."</a>";
                $text = str_replace($match, $replacement, $text);

            }
            // if URLs in the text, return the text after
            // removing the leading and trailing spaces

        }
        else {
            // if no URLs in the text, return the original text after
            // removing the leading and trailing spaces
            $text = trim($text, " ");
        }

        return $text;
    }



    function generate_image_thumbnail($source_image_path, $thumbnail_image_path,$maxWidth,$maxHeight)
    {
        list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
        switch ($source_image_type) {
            case IMAGETYPE_GIF:
                $source_gd_image = imagecreatefromgif($source_image_path);
                break;
            case IMAGETYPE_JPEG:
                $source_gd_image = imagecreatefromjpeg($source_image_path);
                break;
            case IMAGETYPE_PNG:
                $source_gd_image = imagecreatefrompng($source_image_path);
                break;
        }
        if ($source_gd_image === false) {
            return false;
        }
        $source_aspect_ratio = $source_image_width / $source_image_height;
        $thumbnail_aspect_ratio = $maxWidth / $maxHeight;
        if ($source_image_width <= $maxWidth && $source_image_height <= $maxHeight) {
            $thumbnail_image_width = $source_image_width;
            $thumbnail_image_height = $source_image_height;
        } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
            $thumbnail_image_width = (int) ($maxHeight * $source_aspect_ratio);
            $thumbnail_image_height = $maxHeight;
        } else {
            $thumbnail_image_width = $maxWidth;
            $thumbnail_image_height = (int) ($maxWidth / $source_aspect_ratio);
        }
        $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
        imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
        imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 90);
        imagedestroy($source_gd_image);
        imagedestroy($thumbnail_gd_image);
        return true;
    }

    /**
     *
     *
     */
    function addTitleToImg($html,$title){

        $pattern = "/<img\s+src/";
        $html = preg_replace($pattern,"<img title=\"".$title."\" alt=\"".$title."\" src",$html);
        return $html;
    }

    function createEventTitle(Event $event){
        return $title = $event->getTitle()." ".$this->convertDatetimeToStringByFormat($event->getDate(),"d.m.Y")." на ".$event->getAddress();
        //{{ event.title }} {{ event.date| localizeddate('short', 'none', app.request.locale ) }} на {{ event.address }} / Тестимон - поиск свидетелей
    }

    /**
     * Check if week equals current week
     * @param $currentDate \Datetime
     * @param $neededDate \Datetime
     * @return bool
     */
    public static function ifWeInCurrentWeek($currentDate,$neededDate)
    {
        return ($neededDate->format('Y-W') == $currentDate->format('Y-W'));
    }

    /**
     * Return date time array
     * @param $date \Datetime
     * @return \Datetime|Array
     */
    public static function getWeekDateInterval($date)
    {
        $res = array();
        $currentDate = new \DateTime('now');
        $currentDatedayOfWeek = $currentDate->format('N');
        $dayOfWeek = $date->format('N');
        $dateTo = clone $date;
        $dateFrom = clone $dateTo;
        if (self::ifWeInCurrentWeek($currentDate,$date)){
            $res['dateFrom'] =  $dateFrom->sub(new \DateInterval('P7D'));
            $res['dateTo'] = $dateTo->add(new \DateInterval('P'.($currentDatedayOfWeek-$dayOfWeek).'D'));
        }
        else{
            $res['dateFrom'] = $dateFrom->sub(new \DateInterval('P'.($dayOfWeek-1).'D'));
            $res['dateTo'] = $dateTo->add(new \DateInterval('P'.(7-$dayOfWeek).'D'));
        }

        return $res;
    }




    /**
     * Get parameters to send mail notification of new comment
     * @param $mailFromEmail
     * @param $mailFromName
     * @return array
     */
    public static function getNewCommentMailerParameters($mailFromEmail,$mailFromName)
    {
        return $parameters = array(
            'template' => array(
                'new-comment' => 'SiteBandWitnessBundle:Comment:email.txt.twig'),
            'from_email' =>
                array('new-comment' => array($mailFromEmail => $mailFromName)));
    }

    /**
     *  geterate uniq hash for user
     * @return string
     */
    public static function generateUserHash(){
       return  md5(uniqid(rand(), TRUE));
    }




} 