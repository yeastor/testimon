<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 23.09.2014
 * Time: 17:09
 */

namespace SiteBand\WitnessBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
class MapControllerTest  extends WebTestCase{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testXMLnodata(){


        $this->client->request('GET', '/map.xml');

        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'text/xml; charset=UTF-8'
            )
        );

    }

    public function testXMLwithdata(){

        $client = static::createClient();
        $params = array(
            'date' => '26/08/2014',
        );

        $client->request('GET', '/map.xml',$params);

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'text/xml; charset=UTF-8'
            )
        );



    }
} 