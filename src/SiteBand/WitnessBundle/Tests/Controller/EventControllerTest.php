<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 23.09.2014
 * Time: 10:35
 */

namespace SiteBand\WitnessBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
class EventControllerTest extends WebTestCase{

    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }


    public function testAddAnonymous()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/event-add/');

        //print $client->getResponse()->getContent();
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertGreaterThan(
            0,
            $crawler->filter('html:contains("alert.nologin")')->count()
        );

        $crawler = $client->request('POST', '/event-add/');
        $this->assertFalse($client->getResponse()->isSuccessful());

    }

    public function testAddAuthedUser()
    {
        $this->logIn();

        $crawler = $this->client->request('GET', '/event-add/');

        //print $this->client->getResponse()->getContent();

        $this->assertEquals(
            0,
            $crawler->filter('html:contains("alert.nologin")')->count()
        );

        $this->client->request('POST', '/event-add/');
        $this->assertTrue($this->client->getResponse()->isSuccessful());

    }

    private function logIn()
    {
        //$session = $this->client->getContainer()->get('session');

        $firewall = 'secured_area';

        $em = $this->client->getContainer()->get('doctrine')->getManager();
        $user = $em->getRepository('SiteBandUserBundle:User')->findOneByUsername('yeastor');

        $token = new UsernamePasswordToken($user, $user->getPassword(), $firewall, $user->getRoles());
        self::$kernel->getContainer()->get('security.context')->setToken($token);

        $session = $this->client->getContainer()->get('session');
        $session->set('_security_' . $firewall, serialize($token));
        $session->save();


        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

} 