<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 24.09.2014
 * Time: 13:58
 */
namespace SiteBand\WitnessBundle\Tests\Modules;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DomCrawler\Field\InputFormField;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class EventEntityTest  extends WebTestCase {

    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    /**
     *
     * test save preDate if Date is changed
     */
    public function testUpdateEvent(){

        $this->logIn();

        $crawler = $this->client->request('GET', '/adminhere/siteband/witness/event/8/edit');

        $buttonCrawlerNode = $crawler->filter('button[name=btn_update_and_edit]');

        $form = $buttonCrawlerNode->form();

        $dateInput = $crawler->filter('input[id*=_date]');
        $preDateInput = $crawler->filter('input[id*=_predate]');

        $preDate = $form[$preDateInput->attr('name')]->getValue();

        $currentDate = $form[$dateInput->attr('name')]->getValue();
        $oldCurrentDate = $currentDate;
        if (!$preDate || empty($preDate))
        {
            if ($currentDate != '16/01/2014'){
                $newDate =  '16/01/2014';
            }
            else {
                $newDate = '15/01/2014';
            }
        }
        else {
            $newDate = $preDate;
        }
        $form[$dateInput->attr('name')]->setValue($newDate);
        $this->client->submit($form);

        $crawler = $this->client->request('GET', '/adminhere/siteband/witness/event/8/edit');
        $buttonCrawlerNode = $crawler->filter('button[name=btn_update_and_edit]');
        $form = $buttonCrawlerNode->form();
        $dateInput = $crawler->filter('input[id*=_date]');
        $preDateInput = $crawler->filter('input[id*=_predate]');
        $preDate = $form[$preDateInput->attr('name')]->getValue();

        $currentDate = $form[$dateInput->attr('name')]->getValue();

        $this->assertEquals($currentDate,$newDate);

        $this->assertEquals($preDate,$oldCurrentDate);

    }



    private function logIn()
    {
        //$session = $this->client->getContainer()->get('session');

        $firewall = 'secured_area';

        $em = $this->client->getContainer()->get('doctrine')->getManager();
        $user = $em->getRepository('SiteBandUserBundle:User')->findOneByUsername('yeastor');

        $token = new UsernamePasswordToken($user, $user->getPassword(), $firewall, $user->getRoles());
        self::$kernel->getContainer()->get('security.context')->setToken($token);

        $session = $this->client->getContainer()->get('session');
        $session->set('_security_' . $firewall, serialize($token));
        $session->save();


        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }



} 