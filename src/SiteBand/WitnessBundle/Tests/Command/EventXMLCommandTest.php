<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 24.09.2014
 * Time: 12:12
 */
namespace SiteBand\WitnessBundle\Tests\Command;

use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use SiteBand\WitnessBundle\Command\EventXMLCommand;

class EventXMLCommandTest extends KernelTestCase{

    private $commandTester = null;

    public function setUp()
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new EventXMLCommand());

        $command = $application->find('siteband:eventtoxml');
        $this->commandTester = new CommandTester($command);

    }

    public function testExecuteForce()
    {
        $this->commandTester->execute(array('--force' => true));
        $this->assertRegExp('/2014-08-26/',  $this->commandTester->getDisplay());

        $this->commandTester->execute(array());

        $this->assertRegExp('/all events up to date/',  $this->commandTester->getDisplay());
    }

} 