/**
 * Created by kupriyanovym on 10.04.14.
 */
var Security = (function(){

    var secPath, regPath
    return{

        init : function(options){
           var $this = this;



            //$('#login-form').submitForm(secPath, function(){window.location.reload()});
            //$('#registeration-form').submitForm(regPath, function(){ $('#registeration-form').html('<div class="alert alert-success">'+data.message+'</div>');});


            $(window).on('showLoginForm', function(e){
                $this.showLoginModalFrom(options);
            });
        },

        showLoginModalFrom : function(options){
            var $this = this;
            $('#loginModal').modal(options);
            this.onFromSubmited($('#login-form'),secPath,function(){$this.onLoginSuccess(); $('#loginModal').modal('hide'); $('#ws_comment_text').focus(); });
            this.onFromSubmited($('#registeration-form'),regPath,function(){$('#registeration-form').html('<div class="alert alert-success">'+data.message+'</div>'); });
        },

        setSecPath : function(url){

            secPath = url;
        },

        setRegPath : function(url){

            regPath = url;
        },

        onFromSubmited : function(formContainer,url, successFn, failFn){
            form = $("form",formContainer);
            form.submit(function(e){e.preventDefault();
                form = $(e.target);
                $.ajax({
                    type        : form.attr( 'method' ),
                    url         : url,
                    data        : form.serialize(),
                    dataType    : "json",
                    success     : function(data, status, object) {

                        if(data.success == false) {
                            //$('.alert',this).removeClass('hidden').html(data.message);

                            errors = $('<ul></ul>');

                            $.each(data.message,function(i,val){

                                errors.append("<li>"+data.message[i]+"</li>");

                            })
                            $('.alert',formContainer).removeClass("hidden").html(errors);

                            failFn();
                        }
                        else {  successFn(data.message);  }
                    },
                    error: function(data, status, object){
                        alert(status);
                        console.log(data.message);
                    }
                });
            });
        },

        onLoginSuccess : function(){
            $(window).trigger('loginSuccess');
        }





    }
})();