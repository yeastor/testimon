<?php
/**
 * Created by PhpStorm.
 * User: KupriyanovYM
 * Date: 31.01.14
 * Time: 14:19
 */

namespace SiteBand\WitnessBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CityType extends AbstractType {

    protected $cityName;

    public function __construct ($cityName = null)
    {
        $this->cityName = $cityName ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name' ,null,
                array('label' => 'City'
                ,'label_attr' => array(
                    'data-hint' => "event_type_form.instruction.city",
                    'data-hint-image' => "city.png"
                )
                ,'data' => $this->cityName
                ,'attr' => array ('class' => "form-control city")
                )
            );

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SiteBand\WitnessBundle\Entity\City',
        ));
    }


    public function getName()
    {
        return "ws_city";
    }

} 