<?php
/**
 * Created by PhpStorm.
 * User: KupriyanovYM
 * Date: 31.01.14
 * Time: 13:59
 */

namespace SiteBand\WitnessBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use SiteBand\WitnessBundle\Utils\Helper;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;

class EventType extends AbstractType {

    protected $defaultCity;
    protected $defaultAddress;
    protected $defaultDate;

    public function __construct (array $defaultValues = null)
    {
        $this->defaultCity = isset ($defaultValues['city']) ? $this->defaultCity = $defaultValues['city'] : null;
        $this->defaultAddress = isset ($defaultValues['address']) ? $this->defaultAddress = $defaultValues['address'] : null;
        $this->defaultAddressCenter = isset ($defaultValues['addressCenter']) ? $this->defaultAddressCenter = $defaultValues['addressCenter'] : null;
        $helper = new Helper();

        $this->defaultDate = isset ($defaultValues['date']) ? $helper->convertStringDateToDatetimeByFormat($defaultValues['date'],'d/m/Y') : null;


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

    $builder
        ->add('date' ,'date',
            array('label' => 'Date', 'widget' => 'single_text', 'format' => 'dd/MM/yyyy','data' =>  $this->defaultDate
        ,'attr' => array (
                'data-date-format' => "dd/mm/yyyy",
                'class' => 'form-control datepicker',
                'hint-message' => "event_type_form.instruction.date"
            )
        ,'label_attr' => array(
                'data-hint' => "event_type_form.instruction.date",
                'data-hint-image' => "date.png"
                    )
            )
        )
        ->add('country','entity',
                array('label' => false,
                        'class' => 'SiteBand\WitnessBundle\Entity\Country',
                        'property' => 'name'
                        ,'attr' => array ('class' => "form-control country hidden")
                    ))
        //->add('city','text', array('label' => 'City','attr' => array('ac' => 'yes')

           // 'class' => 'SiteBand\WitnessBundle\Entity\City',
            //'property' => 'name'
        //))
        ->add('city',new CityType($this->defaultCity)
            ,array('label' => false
            ,'constraints' => array(new NotBlank())
            )
        )
        ->add('address','text'

            , array('label' => 'Address'
            ,'constraints' => array(new NotBlank())
            ,'data' => $this->defaultAddress
            ,'attr' => array (
                    'class' => "form-control address input"
                    ,'hint-message' => "event_type_form.instruction.address"
                    )
            ,'label_attr' => array(
                    'data-hint' => "event_type_form.instruction.address",
                    'data-hint-image' => "address.png"
                )
            )
        )
        ->add('update','button'
            , array('label' => 'Update address'

            //,'mapped' => false
            ,'attr' => array (
                    'data-type' => "update",
                    'href' => "#step-3",
                    'class' => 'btn btn-warning'
                    ,'glyps' => 'glyphicon glyphicon-refresh'
                    ,'hint-message' => "event_type_form.instruction.map"

                )
            )
        )
        ->add('coords', 'hidden',
            array('constraints' => array(new NotBlank(array('message' => 'Укажите точку на карте так, чтобы её было видно')))
                ,'data' => $this->defaultAddressCenter
                ,'attr' => array ('class' => "coords")
            )
        )
        //->add('isOnMap', 'hidden', array("mapped" => false, 'constraints' => array(new NotBlank(array('message' => 'Установите точку на карте так, что бы Вы её видели')) )))
        //->add('addressCenter', 'hidden', array("mapped" => false,'data' => $this->defaultAddressCenter,'attr' => array ('class' => "coords")))
        ->add('title',null,
            array('label' => 'Title'
            ,'constraints' => array(new NotBlank())
            ,'attr' => array (
                'class' => "form-control typeahead"
                ,'hint-message' => "event_type_form.instruction.title")
            ,'label_attr' => array(
            'data-hint' => "event_type_form.instruction.title")
            )
        )
        ->add('description',null,
            array('label' => 'Description'
            ,'constraints' => array(new NotBlank())
            ,'attr' => array (
                'hint-message' => "event_type_form.instruction.description"
            )
            ,'label_attr' => array(
            'data-hint' => "event_type_form.instruction.description")
            )
        )
        ->add('embed', 'hidden',
            array(
                'attr' => array ('class' => "images")
            )
        )
        ;

        //$builder->add('comments',new CommentType());
        $builder->add('comments' ,'textarea', array('label' => 'Comment text'
            ,'attr' => array (
                    'hint-message' => "event_type_form.instruction.comment"
                )
            ,'required' => false, 'label_attr' => array ('data-hint' => "event_type_form.instruction.comment")));

        $builder->add('save', 'submit',array(
            'attr' => array('class' => 'btn btn-success'
            ,'glyps' => 'glyphicon glyphicon-floppy-saved')
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SiteBand\WitnessBundle\Entity\Event',

        ));
    }

    public function getName()
    {
        return "ws_event";
    }

}