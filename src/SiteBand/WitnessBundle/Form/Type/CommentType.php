<?php
/**
 * Created by PhpStorm.
 * User: KupriyanovYM
 * Date: 31.01.14
 * Time: 14:19
 */

namespace SiteBand\WitnessBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentType extends AbstractType {

    public function __construct (array $defaultOptions = null)
    {
        $this->defaultEventId = isset ($defaultOptions['event_id']) ? $this->defaultEventId = $defaultOptions['event_id'] : null;
        $this->addCaptcha = isset ($defaultOptions['captcha']) ? true : false;

    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text' ,null, array('label' => 'comment.text'))
            ->add('event_id', 'hidden', array('mapped' => false, 'data' =>  $this->defaultEventId));
        //if ($this->addCaptcha){
         //  $builder->add('captcha', 'captcha', array('reload' => true, 'as_url'=>true));
        //}
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SiteBand\WitnessBundle\Entity\Comment',
        ));
    }


    public function getName()
    {
        return "ws_comment";
    }

} 