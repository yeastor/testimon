<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 20.01.14
 * Time: 15:01
 */

namespace SiteBand\WitnessBundle\Controller;
use SiteBand\WitnessBundle\Entity\FakeEventDetail;
use SiteBand\WitnessBundle\Utils\XMLManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SiteBand\WitnessBundle\Entity\City;
use SiteBand\WitnessBundle\Entity\Country;
use SiteBand\UserBundle\Entity\User;
use SiteBand\WitnessBundle\Entity\Event;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Security\Core\Util\ClassUtils;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Response;
use JMS\SerializerBundle\JMSSerializerBundle;
use SiteBand\WitnessBundle\Utils\Helper;
use Symfony\Component\HttpFoundation\Request;
use SiteBand\WitnessBundle\Utils\YmapMLMapsWorker;
use Symfony\Component\Config\FileLocator;
use SiteBand\WitnessBundle\Entity\XMLEvent;
use SiteBand\WitnessBundle\XML\EventXML;
use SiteBand\WitnessBundle\XML\FEventXML;
use Symfony\Component\HttpFoundation\Cookie;
use SiteBand\WitnessBundle\Classes\FakeEventDetailManager;
use SiteBand\WitnessBundle\Classes\FakeEventManager;

class MapController extends Controller{
    public function indexAction()
    {
        $request = Request::createFromGlobals();
        $response = new Response();
        $date = $request->query->get('date');
        $addFakeEvent = $request->query->get('fevent');
        if (!$date) return new Response( null,200,array('Content-Type' => 'text/xml'));
        $helper = new Helper();
        $dateObg = $helper->convertStringDateToDatetimeByFormat($date,'d/m/Y');
        $dateObg->setTime(0,0,0);
        $XMLEvent = $this->getDoctrine()
            ->getRepository('SiteBandWitnessBundle:XMLEvent')
            ->findOneBy(array('date' => $dateObg));

        $xml = null;


        $cookiesHash = null;
        $cookiesHash = $request->query->get('hash');
        if ($cookiesHash && $cookiesHash != 'undefined') {
            $res = $this->getFeventXmlFromCookie($cookiesHash,$dateObg,null,$helper);

            if (is_array($res)) $xml = $res['xml'];
        }
        if ($addFakeEvent == '1' && !$res && $cookiesHash != 'undefined'){
            $dateTo = $dateObg;
            $xml = $this->getFeventXmlNoCookie($request,$helper,$dateTo,$cookiesHash);
        }

        if (!$XMLEvent && !$xml) { $xml = null; }
        elseif (!$XMLEvent && $xml) {
            $FEventXMLed = new FEventXML('@SiteBandWitnessBundle/Resources/public/xsl/entitytoymapsml.xsl',$this->get('kernel'));
            $response->setContent($FEventXMLed->getXmlText($xml));
            $response->headers->set('Content-Type','text/xml');
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        else {
            $xmlManager = new XMLManager();
            $xmlManager->loadXML(stream_get_contents($XMLEvent->getXml()));

            $FEventXMLed = new FEventXML('@SiteBandWitnessBundle/Resources/public/xsl/entitytoymapsml.xsl',$this->get('kernel'));
            $xmlManager->appendXmlChildFromObjectArray(array($xml),'featureMembers','GeoObject',$FEventXMLed);

            $xml = $xmlManager->saveXML();
        }


        $response->setContent($xml);
        $response->headers->set('Content-Type','text/xml');
        $response->setStatusCode(Response::HTTP_OK);
        return $response;

    }

    /*
     * Get xml events from start date minus 7 day
     */
    public function weekAction()
    {
        $request = Request::createFromGlobals();
        $response = new Response();
        $date = $request->query->get('date');
        $addFakeEvent = $request->query->get('fevent');
        if (!$date) return new Response( null,200,array('Content-Type' => 'text/xml'));
        $helper = new Helper();

        $dateTo = $helper->convertStringDateToDatetimeByFormat($date,'d/m/Y');
        $dateTo->setTime(0,0,0);

           $datesFromTo = Helper::getWeekDateInterval($dateTo);

            $dateFrom = $datesFromTo['dateFrom'];
            $dateTo = $datesFromTo['dateTo'];



        $XMLEvents = $this->getDoctrine()->getManager()
            ->getRepository('SiteBandWitnessBundle:XMLEvent')
            ->findByBetweenDate($dateFrom,$dateTo);

        $xml = null;


        $cookiesHash = null;
        $cookiesHash = $request->query->get('hash');
        if ($cookiesHash && $cookiesHash != 'undefined') {
            $res = $this->getFeventXmlFromCookie($cookiesHash, $dateFrom, $dateTo, $helper);
            if (is_array($res)) $xml = $res['xml'];
        }
        if ($addFakeEvent == '1' && !$res && $cookiesHash != 'undefined'){


                    $minusDay = rand(0,6);
                    $dateTo = $dateTo->sub(new \DateInterval('P'.$minusDay.'D'));
                    $xml = $this->getFeventXmlNoCookie($request,$helper,$dateTo,$cookiesHash);
                    //$this->setCookies($response,$hash);
        }


        if (!$XMLEvents && !$xml){
            $response->setContent($xml);
            $response->headers->set('Content-Type','text/xml');
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        elseif(!$XMLEvents && $xml){
                $FEventXMLed = new FEventXML('@SiteBandWitnessBundle/Resources/public/xsl/entitytoymapsml.xsl',$this->get('kernel'));
                $response->setContent($FEventXMLed->getXmlText($xml));
                $response->headers->set('Content-Type','text/xml');
                $response->setStatusCode(Response::HTTP_OK);

                return $response;

        }

        $firstXml = current($XMLEvents);

        $xmlManager = new XMLManager();
        $xmlManager->loadXML(stream_get_contents($firstXml->getXML()));
        next($XMLEvents);
        $xmlManager->appendXmlChildFromObjectArray(array_slice($XMLEvents,1),'featureMembers','GeoObject',new EventXML());


        if ($xml){
            $FEventXMLed = new FEventXML('@SiteBandWitnessBundle/Resources/public/xsl/entitytoymapsml.xsl',$this->get('kernel'));
            $xmlManager->appendXmlChildFromObjectArray(array($xml),'featureMembers','GeoObject',$FEventXMLed);
        }

           $xml = $xmlManager->saveXML();

        $response->setContent($xml);
        $response->headers->set('Content-Type','text/xml');
        $response->setStatusCode(Response::HTTP_OK);

        return $response;

    }

    /**
     * @param Request $request
     * @param Helper $helper
     * @param $dateObg
     * @return string
     */
    private function getFeventXmlNoCookie(Request $request,Helper $helper,$dateObg,$hash)
    {
        $fakeEventManager = new FakeEventManager($this->getDoctrine()->getManager());

        $fakeEvent = $fakeEventManager->getRandomFEvent();

        $coords = $request->query->get('coords');
        $address = $request->query->get('name');
        $date = $dateObg;
        $xml = $fakeEventManager->getXML($fakeEvent,$coords,$address,$helper->convertDatetimeToStringByFormat($date,'d.m.Y'));
        $fakeEventDetailsManager = new FakeEventDetailManager($this->getDoctrine()->getManager());
        $fakeEventDetailsManager->add($fakeEvent,$coords,$address,$date,$hash);
        return $xml;
    }

    /**
     * try to get saved fake event data from FakeEventDetailManager
     * @param string $cookiesHash
     * @param \DateTime $date
     * @param Helper $helper
     * @return null|array
     */
    private function getFeventXmlFromCookie($cookiesHash,$dateFrom,$dateTo = null,Helper $helper)
    {
        $fakeEventDetailsManager = new FakeEventDetailManager($this->getDoctrine()->getManager());
        $criteria = array('hash' => $cookiesHash);
        $fakeEventDetails = $fakeEventDetailsManager->findOneBy($criteria);
        if (!$fakeEventDetails) return null;

        $valid  = $fakeEventDetailsManager->isInDate($fakeEventDetails,$dateFrom,$dateTo);

        if (!$valid) return array('valid' => $valid, 'xml' => null);
        $fakeEvent = $fakeEventDetails->getFakeEvent();
        $coords = $fakeEventDetails->getCoords();
        $address = $fakeEventDetails->getAddress();
        $date = $fakeEventDetails->getDate();
        $fakeEventManager = new FakeEventManager($this->getDoctrine()->getManager());
        $xml = $fakeEventManager->getXML($fakeEvent,$coords,$address,$helper->convertDatetimeToStringByFormat($date,'d.m.Y'));

        return  array('valid' => $valid, 'xml' => $xml);

    }

    private function setCookies(Response $response,$value)
    {
        $cookieGuest = array(
            'name'  => 'firstime',
            'value' => $value,
            'path'  => '/',
            'time'  => time() + 3600 * 24 * 365
        );

        $cookie = new Cookie($cookieGuest['name'], $cookieGuest['value'], $cookieGuest['time'], $cookieGuest['path'], null, false, false);

        $response->headers->setCookie($cookie);
    }
} 