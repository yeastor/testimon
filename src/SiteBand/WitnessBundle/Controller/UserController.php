<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 16.05.14
 * Time: 14:44
 */

namespace SiteBand\WitnessBundle\Controller;

use SiteBand\UserBundle\Entity\Setting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SiteBand\UserBundle\Form\Type\SettingListFormType;
use SiteBand\UserBundle\Form\Type\SettingFormType;
use SiteBand\UserBundle\Entity\Settings;
use SiteBand\UserBundle\Form\Type\SettingsFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/user")
 */
class UserController extends Controller {
    /**
     * @Route("/messages/", name="site_band_witness_user_messages")
     * @Template()
     */
    public function massageAction()
    {
        $currentUser = $this->container->get('security.context')->getToken()->getUser();

        $offset = 5;
        $repository = $this->getDoctrine()
            ->getRepository('SiteBandWitnessBundle:Event');

        $query = $repository->createQueryBuilder('e')
            ->where('e.user = :user')
            ->setParameter('user',$currentUser)
            ->orderBy('e.date', 'DESC')
            ->getQuery();
        //$events = $query->setMaxResults($offset)->getResult();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1)/*page number*/,
            4/*limit per page*/
        );


        return $this->render('SiteBandWitnessBundle:User:messages.html.twig', array("pagination" => $pagination ));

    }

    /**
     * @Route("/settings/", name="site_band_witness_user_settings")
     */
    public function settingAction()
    {
        $em = $this->getDoctrine()->getManager();
        $currentUser = $this->container->get('security.context')->getToken()->getUser();

        $settingsList = $this->getDoctrine()
            ->getRepository('SiteBandUserBundle:SettingList')
            ->findAllByUserJoinedToSetting($currentUser);

        $settingCollection = new Settings();
        foreach ($settingsList as $sl)
        {
            $settings = $sl->getSettings();
            $setting = $settings[0];
            if (!$setting || null === $setting->getValueInt())
            {
                $setting = new Setting();
                $setting->setSetting($sl);
                $setting->setType($sl->getType());
                $value = null;
                if ($sl->getType() == 0)
                {
                    $value = $sl->getDefaultValueInt();
                }
                $setting->setValueInt($value);
            }
            $settingCollection->getSettings()->add($setting);
        }

        $form = $this->createForm(new SettingsFormType(),$settingCollection,array(
                    'em' => $em));

        $request = Request::createFromGlobals();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $settings = $form->getData()->getSettings();
            foreach ($settings as $setting) {

                $setting->setUser($currentUser);
                $em->persist($setting);
            }

            $em->flush();
            return $this->redirect($this->generateUrl('site_band_witness_user_settings', array('success' => true)));
        }




        return $this->render('SiteBandWitnessBundle:User:settings.html.twig',array('collection' => $form->createView(),'success' => $request->get('success') ));
    }
} 