<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 04.04.14
 * Time: 16:17
 */

namespace SiteBand\WitnessBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SiteBand\WitnessBundle\Entity\City;
use SiteBand\WitnessBundle\Entity\Country;
use SiteBand\UserBundle\Entity\User;
use SiteBand\WitnessBundle\Entity\Event;
use SiteBand\WitnessBundle\Entity\Comment;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Util\ClassUtils;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Response;
use JMS\SerializerBundle\JMSSerializerBundle;
use SiteBand\WitnessBundle\Utils\Helper;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SiteBand\WitnessBundle\Form\Type\CommentType;
use SiteBand\WitnessBundle\Mailer\UserMailer;

/**
 * @Route("/comment")
 */
class CommentController extends Controller{


    /**
     * @Route("-add/", name="site_band_witness_comment_add", options={"expose"=true})
     * @Template()
     */
    public function addAction($eventId = null)
    {

        $defaultOptions['event_id'] = ($eventId) ? $eventId : null;
        $defaultOptions['captcha'] = true;



        $form = $this->createForm(new CommentType($defaultOptions));

        $request = Request::createFromGlobals();
        $form->handleRequest($request);








        if ($form->isValid()) {

            $helpUtils = new Helper();

            $formData = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $currentUser = $this->container->get('security.context')->getToken()->getUser();

            $event = $em->getReference('SiteBandWitnessBundle:Event',$form["event_id"]->getData());
            //$event = $em->getReference('SiteBandWitnessBundle:Event',$eventId);

            $comment = new Comment();
            $commentText = $formData->getText();
            $commentText = $helpUtils->addTitleToImg($commentText,$helpUtils->createEventTitle($event));
            $comment->setText($commentText);


            $comment->setUser($currentUser);
            $comment->setEvent($event);

            $em->persist($comment);
            $em->flush();


            //$translator = $this->get('translator');
            //$parameters = $helpUtils::getNewCommentMailerParameters($translator->trans('from_email.address'),$translator->trans('from_email.sender_name'));
            $em = $this->getDoctrine()->getManager();
            $user = $event->getUser();

            $newCommentNotification = $this->getDoctrine()
                ->getRepository('SiteBandUserBundle:SettingList')
                ->findOneJoinedToSetting($user,'notification.iWhantNewComment');
            if ($newCommentNotification == 1 && $currentUser != $user)
            {
                $userMailer = $this->get('site_band_witness.user_mailer');
                $userMailer->sendNewCommentEmailMessage($event);
            }
            // var_dump($settingsList); exit;
            $updateDate = $em->getRepository('SiteBandWitnessBundle:UserLastAction')->replace($currentUser);
            $array = array( 'success' => true); // data to return via JSON
            if ($request->isXmlHttpRequest())
            {
                $response = new Response( json_encode( $array ) );
                $response->headers->set( 'Content-Type', 'application/json' );
                return $response;
            }
            return new RedirectResponse($this->generateUrl('site_band_witness_event_get',array('id' => $form["event_id"]->getData())));


        }

        if ($request->isXmlHttpRequest() == 'POST')
        {
            $string = $this->getErrorMessages($form);

            $array = array( 'success' => false, 'message' => ($string)); // data to return via JSON
            $response = new Response( json_encode( $array ) );
            $response->headers->set( 'Content-Type', 'application/json' );

            return $response;
        }



        return $this->container->get('templating')->renderResponse('SiteBandWitnessBundle:Comment:add.html.twig', array(
            'form' => $form->createView(), 'eventId' => $eventId
        ));

    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }

} 