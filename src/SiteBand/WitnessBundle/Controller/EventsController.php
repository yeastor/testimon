<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 15.01.14
 * Time: 15:39
 */

namespace SiteBand\WitnessBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SiteBand\WitnessBundle\Entity\City;
use SiteBand\WitnessBundle\Entity\Country;
use SiteBand\UserBundle\Entity\User;
use SiteBand\WitnessBundle\Entity\Event;
use SiteBand\WitnessBundle\Entity\Comment;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Util\ClassUtils;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Response;
use JMS\SerializerBundle\JMSSerializerBundle;
use SiteBand\WitnessBundle\Utils\Helper;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SiteBand\WitnessBundle\Form\Type\EventType;
use SiteBand\WitnessBundle\Classes\FakeEventDetailManager;
use SiteBand\WitnessBundle\Classes\FakeEventManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * @Route("/event")
 */
class EventsController extends Controller{

    /**
     * @Route("-search/", name="site_band_witness_i_am_witness")
     * @Template()
     */
    public function searchAction()
    {
        $translator = $this->get('translator');
        $pageTitle = $translator->trans('I am witness');
        $counties = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Country')
            ->findAll();
        $response = $this->render('SiteBandWitnessBundle:Events:search.html.twig', array("counties" => $counties, "dateNow" => date("d/m/Y"),'title' => $pageTitle ));

        return $response;


    }

    /**
     * @Route("-wsearch/", name="site_band_witness_witness_search")
     * @Template()
     */
    public function witnesssearchAction()
    {
        $translator = $this->get('translator');
        $pageTitle = $translator->trans('Witness search');

        $counties = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Country')
            ->findAll();
        return $this->render('SiteBandWitnessBundle:Events:search.html.twig', array("counties" => $counties, "dateNow" => date("d/m/Y"),'title' => $pageTitle ));
    }

    public function searchFormAction()
    {
        // $event = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Event')
        //    ->find(7);

        $counties = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Country')
            ->findAll();


        $response =  $this->render('SiteBandWitnessBundle:Events:_search.html.twig', array("counties" => $counties, "dateNow" => date("d/m/Y") ));
        $response->setSharedMaxAge(600);

        return $response;
    }
    /**
     * @Route("-last/", name="site_band_witness_events_last", options={"expose"=true})
     */
    public function lastAction($offset = 1,$limit = 5,$sort = 'added')
    {

        $repository = $this->getDoctrine()
            ->getRepository('SiteBandWitnessBundle:Event');
        $queryBuilder = $repository->createQueryBuilder('e');

        $request = Request::createFromGlobals();

        if ($request->getMethod() == 'POST') {

            $offset = $request->get('offset');
            $limit = $request->get('limit');
            $sort = $request->get('sort');
            $operator = $request->get('operator');
            //$limit++; // if next record exist - show NEXT button
        }
        else {

            $offset =  ($request->get('page')) ? $request->get('page') : 1;
        }

        if ($sort == 'added'){
            $queryBuilder->orderBy('e.updated', 'DESC');
        }
        elseif($sort == 'bydate'){
            $queryBuilder->orderBy('e.date', 'DESC');
        }
        $query = $queryBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');
        $events = $paginator->paginate(
            $query,
            $offset /*page number*/,
            $limit /*limit per page*/
        );



       // $events = $query->setFirstResult($offset)->setMaxResults($limit)->getResult();

        if ($request->isXmlHttpRequest()) {

            $paginationData = ($events->getPaginationData());
            $next = array_key_exists("next",$paginationData);
            $prev = array_key_exists("previous",$paginationData);

            $result = array(
                "events" => $this->renderView('SiteBandWitnessBundle:Events:last.html.twig', array("events" => $events )),
                "is_next" => $next,
                "is_prev" => $prev,
                "operator" => $operator
            );

            $response = new Response( json_encode( $result ) );
            $response->headers->set( 'Content-Type', 'application/json' );
            return $response;
        }




       // $events = $query->setMaxResults($offset)->getResult();

        $response =  $this->render('SiteBandWitnessBundle:Events:last-get.html.twig', array("events" => $events ));
        //$response->setSharedMaxAge(600);

        return $response;

    }

    /**
     * @Route("/user/{user}", name="site_band_witness_user_messages")
     * @Template()

    public function userAction($user)
    {
        $repository = $this->getDoctrine()
            ->getRepository('SiteBandWitnessBundle:Event');

        $query = $repository->createQueryBuilder('e')
            ->orderBy('e.date', 'DESC')
            ->getQuery();
        $events = $query->setMaxResults($offset)->getResult();

        return $this->render('SiteBandWitnessBundle:Events:user.html.twig', array("events" => $events ));
    } */

    /**
     * @Route("-test", name="site_band_witness_test")
     * @Template()
     */
    public function testAction(Request $request)
    {
        // если пришли с search-event
        $defaultValue['city'] = ( $request->get('city')) ? $request->get('city') : null;
        $defaultValue['address'] = ( $request->get('address')) ? $request->get('address') : null;
        $defaultValue['date'] = ( $request->get('date')) ? $request->get('date') : null;
        $defaultValue['addressCenter'] = ( $request->get('addressCenter')) ? $request->get('addressCenter') : null;



        $form = $this->createForm(new EventType($defaultValue));

        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            //$helper = new Helper();
            //$dateObg = $helper->convertStringDateToDatetimeByFormat($form->get('date')->getViewData(),'d/m/Y');
            //$form->get('date')->setData($dateObg);
            ///var_dump($form->get('date')->getData());

            //echo "2"; exit;
        }
        if ($form->isValid()) {

            // perform some action, such as saving the task to the database

            $formData = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('SiteBandWitnessBundle:City');
            $cityName = $form->getData()->getCity()->getName();
            $cityInDb = $repository->findOneByName($cityName);
            if ($cityInDb){
                $formData->setCity($cityInDb);
            }
            else{
                $city = new City();
                $city->setName($cityName);
                $formData->setCity($city);

                $em->persist($city);


            }
            $currentUser = $this->container->get('security.context')->getToken()->getUser();

            //echo $formData->getCoords(); exit;
            $event = new Event();
            $event->setDate($formData->getDate());
            $event->setAddress($formData->getAddress());
            $event->setCoords($formData->getCoords());
            $event->setTitle($formData->getTitle());
            $event->setCity($formData->getCity());
            $event->setDescription($formData->getDescription());
            $event->setCountry($formData->getCountry());
            $event->setUser($currentUser);

            if ($form->getData()->getComments())
            {
                $comment = new Comment();
                $comment->setText($formData->getComments());
                $comment->setEvent($event);
                $comment->setUser($currentUser);
                $em->persist($comment);
            }

            $em->persist($event);

            $em->flush();

            return new RedirectResponse($this->generateUrl('site_band_witness_event_get',array('id' => $event->getId(),'new' => true)));


        }

        $view = $form->createView();

        return array('form' => $view );
    }
    /**
     * @Route("-intest/", name="site_band_witness_intest")
     * @Template()
     */
    public function inTestAction()
    {
        return $this->render('SiteBandWitnessBundle:Events:intest.html.twig');
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * События попадающие выбранный день
     */
    public function currentAction()
    {
        $request = Request::createFromGlobals();
        $date = $request->request->get('date');

        $events = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Event')
            ->findByBetweenDate($date);


        $eventData = array();
        foreach ($events as $event)
        {
            $eventData[] = array('id' => $event->getId()
                ,'address' => $event->getAddress()
                ,'coords' => $event->getCoords()

            );

        }
        // yes, here we are retrieving "_format" from routing. In our case it's json
        $serializedEntity = $this->container->get('serializer')->serialize($eventData, 'json');
        $format = "json";//$request->getRequestFormat();

        $serializedEntityUnicodeToUtf = Helper::json_encode_cyr($serializedEntity);

        return $this->render('SiteBandWitnessBundle:Events:current-'.$format.'.html.twig', array("events" => $serializedEntityUnicodeToUtf));

    }

    /**
     * @Route("-add/", name="site_band_witness_event_add", options={"expose"=true})
     * @Template()
     */
    public function addAction(Request $request)
    {

        // если пришли с search-event
        $defaultValue['city'] = ( $request->get('city')) ? $request->get('city') : null;
        $defaultValue['address'] = ( $request->get('address')) ? $request->get('address') : null;
        $defaultValue['date'] = ( $request->get('date')) ? $request->get('date') : null;
        $defaultValue['addressCenter'] = ( $request->get('addressCenter')) ? $request->get('addressCenter') : null;



        $form = $this->createForm(new EventType($defaultValue));

        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            //$helper = new Helper();
            //$dateObg = $helper->convertStringDateToDatetimeByFormat($form->get('date')->getViewData(),'d/m/Y');
            //$form->get('date')->setData($dateObg);
            ///var_dump($form->get('date')->getData());
            $isFormSubmitted = true;
            //echo "2"; exit;
        }
        if ($form->isValid()) {

            // perform some action, such as saving the task to the database

            $formData = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('SiteBandWitnessBundle:City');
            $cityName = $form->getData()->getCity()->getName();
            $cityInDb = $repository->findOneByName($cityName);
            if ($cityInDb){
                $formData->setCity($cityInDb);
            }
            else{
                $city = new City();
                $city->setName($cityName);
                $formData->setCity($city);

                $em->persist($city);


            }
            $currentUser = $this->container->get('security.context')->getToken()->getUser();

            //echo $formData->getCoords(); exit;
            $event = new Event();
            $event->setDate($formData->getDate());
            $event->setAddress($formData->getAddress());
            $event->setCoords($formData->getCoords());
            $event->setTitle($formData->getTitle());
            $event->setCity($formData->getCity());

            $event->setDescription($formData->getDescription());

            $helpUtils = new Helper();
            $imagesTags = $helpUtils->addTitleToImg($form['embed']->getData(),$helpUtils->createEventTitle($event));
            $event->setEmbed($imagesTags);

            $event->setCountry($formData->getCountry());
            $event->setUser($currentUser);

            if ($form->getData()->getComments())
            {
                $comment = new Comment();
                $comment->setText($formData->getComments());
                $comment->setEvent($event);
                $comment->setUser($currentUser);
                $em->persist($comment);
            }

            $em->persist($event);

            $em->flush();

            return new RedirectResponse($this->generateUrl('site_band_witness_event_get',array('id' => $event->getId(),'new' => true)));


        }

        $view = $form->createView();

        return array('form' => $view,'isFormSubmitted' => $form->isSubmitted() );
    }

    /**
     * Get fake event
     * @Route("/event-{id}/", name="site_band_witness_fake_event_get", options={"expose"=true})
     * @Template()
     */
    public function getFakeAction($id)
    {
        $response = new Response();
        $request = Request::createFromGlobals();
        $cookiesHash = $request->cookies->get('firstime');
        if (!$cookiesHash){
            throw new NotFoundHttpException('Sorry not existing!');
        }
        $fakeEventDetailsManager = new FakeEventDetailManager($this->getDoctrine()->getManager());
        $criteria = array('hash' => $cookiesHash);
        $fakeEventManager = new FakeEventManager($this->getDoctrine()->getManager());
        $fakeEvent = $fakeEventManager->find($id);
        $fakeEventDetails = $fakeEventDetailsManager->findOneBy($criteria);
        if (!$fakeEvent || !$fakeEventDetails){
            throw new NotFoundHttpException('Sorry not existing!');
        }
        $event = new Event();
        $event->setAddress($fakeEventDetails->getAddress());
        $event->setDate($fakeEventDetails->getDate());
        $event->setCoords($fakeEventDetails->getCoords());
        $event->setDescription($fakeEvent->getDescription());
        $event->setTitle($fakeEvent->getTitle());



        $response = $this->render('SiteBandWitnessBundle:Events:getfake.html.twig', array("event" => $event));
        return $response;
    }

    /**
     * @Route("/{id}/", name="site_band_witness_event_get", options={"expose"=true})
     * @Template()
     */
    public function getAction($id)
    {
        $request = Request::createFromGlobals();

        $new = $request->get("new");

        $event = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Event')
            ->find($id);

        $comments = $event->getComments();

        return $this->render('SiteBandWitnessBundle:Events:get.html.twig', array("event" => $event,"comments" => $comments, "new" => $new ));
    }


}