<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 12.12.2014
 * Time: 14:36
 */

namespace SiteBand\WitnessBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SiteBand\WitnessBundle\Entity\FakeEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
/**
 * Получение fake event реализовано в EventsController
 * @Route("/fevent")
 */
class FakeEventController extends Controller{

    /**
     * @Route("-list/", name="site_band_witness_fevent_list", options={"expose"=true})
     * @Template()
     */
    public function listAction()
    {
        $kernel = $this->get('kernel');
        $path = $kernel->locateResource('@SiteBandWitnessBundle/Resources/public/xsl/entitytoymapsml.xsl');

        $repo = $this->getDoctrine()
            ->getRepository('SiteBandWitnessBundle:FakeEvent');
        $fakeEvent = $repo->find(1);

        $xml = $repo->getXml($fakeEvent,"20.12.2024","Izm проезд","55.78808994992288,37.74886794958495");

        $XML = new \DOMDocument();
        $XML->loadXML( $xml );
# START XSLT
        $xslt = new \XSLTProcessor();
        $XSL = new \DOMDocument();

        $XSL->load( $path, LIBXML_NOCDATA);
        $xslt->importStylesheet( $XSL );
        $transformedXml = $xslt->transformToXML( $XML );


        if (!$transformedXml) { $xml = null; }
        else {
            $xml = $transformedXml;
        }

        $response = new Response( $xml,200,array('Content-Type' => 'text/xml'));
        $response->headers->setCookie(new Cookie("washere", 'bar',time() + 3600,'/',null,false,false));

        return $response;
    }


}