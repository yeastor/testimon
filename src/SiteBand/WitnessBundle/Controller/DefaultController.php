<?php

namespace SiteBand\WitnessBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SiteBand\WitnessBundle\Entity\City;
use SiteBand\WitnessBundle\Entity\Country;
use SiteBand\UserBundle\Entity\User;
use SiteBand\WitnessBundle\Entity\Event;
use SiteBand\WitnessBundle\Entity\LogEventLastId;
use SiteBand\WitnessBundle\Entity\XMLEvent;
use FOS\UserBundle\Doctrine\UserManager;
use SiteBand\WitnessBundle\Utils\Helper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yandex\Disk\DiskClient;
use Symfony\Component\HttpFoundation\Cookie;

class DefaultController extends Controller
{
    public function indexAction()
    {
       /* $helpUtils = new Helper();
        $text = $helpUtils->embedTags(strip_tags('http://www.ntv.ru/novosti/1182536/#ixzz39bnel0wV <a href="asdads">fgdfg</a>

четверг, 7 августа 2014 г. yeastor
 http://www.ntv.ru/novosti/1182536/#ixzz39bnel0wV
 1<a>2</a> http://www.ntv.ru/novosti/1182536/#ixzz39bnel0wV'));
        echo $text; exit;
      /*  $logEventLastIdRepo = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:LogEventLastId');
        $logEventLastId = $logEventLastIdRepo->getMaxLogId();
        //var_dump($logEventLastId);
        $eventIdFrom = $logEventLastId[0]['max_id'] ? $logEventLastId[0]['max_id'] : 1;

        $eventRepo = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Event');
        $eventMaxId = $eventRepo->getMaxLogId();

        $eventIdTo = $eventMaxId[0]['max_id'] ? $eventMaxId[0]['max_id'] : 1;

        if($eventIdFrom < $eventIdTo)
        {
            $groupedEvents = $eventRepo->findGroupedDate($eventIdFrom,$eventIdTo);
            foreach($groupedEvents as $event)
            {
                $kernel = $this->get('kernel');
                $path = $kernel->locateResource('@SiteBandWitnessBundle/Resources/public/xsl/entitytoymapsml.xsl');

                $request = Request::createFromGlobals();
                $date = $event['date'];
                $helper = new Helper();
                $dateOnly = $helper->convertStringDateToDatetimeByFormat($date,'Y-m-d');
                $dateFrom = $helper->convertStringDateToDatetimeByFormat($date,'Y-m-d')->setTime(0,0,0);
                $dateTo = $helper->convertStringDateToDatetimeByFormat($date,'Y-m-d')->setTime(23,59,59);



                $events = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Event')
                    ->findByBetweenDate($dateFrom,$dateTo);



                $xml = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Event')->getXml($events);

                echo utf8_encode($xml); exit;

                $XML = new \DOMDocument();
                $XML->loadXML( $xml );

# START XSLT
                $xslt = new \XSLTProcessor();
                $XSL = new \DOMDocument();


                $XSL->load( $path, LIBXML_NOCDATA);
                $xslt->importStylesheet( $XSL );
                $transformedXml = $xslt->transformToXML( $XML );

                $em = $this->getDoctrine()->getManager();
                $XMLEvent = $em->getRepository("SiteBandWitnessBundle:XMLEvent")->findOneBy(array ('date' => $dateOnly));
                if (!$XMLEvent)
                {
                $XMLEvent = new XMLEvent();
                $XMLEvent->setDate($dateOnly);
                $XMLEvent->setXml($transformedXml);

                $em->persist($XMLEvent);
                }
                else
                {
                    $XMLEvent->setDate($dateOnly);
                    $XMLEvent->setXml($transformedXml);
                }
                $em->flush();



#PRINT
                //print $xml->saveXML(); exit;
                // yes, here we are retrieving "_format" from routing. In our case it's json
                //$serializedEntity = $this->container->get('serializer')->serialize($eventData, 'yaml');




            }
            $em = $this->getDoctrine()->getManager();
            $logEventLastId = new LogEventLastId();
            $logEventLastId->setLastEventId($eventIdTo);
            $em->persist($logEventLastId);
            $em->flush();


        }*/


        //echo $eventIdTo; exit;



        //$event = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Event')
          //  ->findAll();
     /*   $diskClient = new DiskClient('18ccc3a14c984d8dba90f6848cfad332');
        $diskClient->setServiceScheme(DiskClient::HTTPS_SCHEME);
        $diskSpace = $diskClient->diskSpaceInfo();
        $target = '/События/картинка.jpg';
//Сохранение превьюшки


//Вывод превьюшки
        $size = 'XS';
        $file = $diskClient->getImagePreview($target, $size);
       //var_dump($file['headers']);
        //echo $file['body'];
        header('Content-Description: File Transfer');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Date: ' . $file['headers']['date']);
        header('Content-Type: image/jpeg');
        header('Content-Length: ' . $file['headers']['content-length']);
        header('Accept-Ranges: ' . $file['headers']['accept-ranges']);
        echo $file['body'];



        exit;*/

        //$conf = \HTMLPurifier_Config::createDefault();
        //$purifier = new \HTMLPurifier();
        //$clean_html = $purifier->purify($dirty_html);


        $response = $this->render('SiteBandWitnessBundle:Default:index.html.twig');
        $request = Request::createFromGlobals();
        $cookiesHash = $request->cookies->get('firstime');
        if (!$cookiesHash){
            $hash = Helper::generateUserHash();
            $this->setCookies($response,$hash);
        }

        return $response;
    }

    private function setCookies(Response $response,$value)
    {
        $cookieGuest = array(
            'name'  => 'firstime',
            'value' => $value,
            'path'  => '/',
            'time'  => time() + 3600 * 24 * 365
        );

        $cookie = new Cookie($cookieGuest['name'], $cookieGuest['value'], $cookieGuest['time'], $cookieGuest['path'], null, false, false);

        $response->headers->setCookie($cookie);
    }

    public function searchEventsAction($id)
    {
        $event = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Event')
            ->find($id);


        return $this->render('SiteBandWitnessBundle:Default:event.html.twig', array("event" => $event ));
    }

    public function saveAction($id = '0')
    {
        $city = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:City')
            ->find(1);
        $contry = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:Country')
            ->find(1);

        $user = $this->getUser();
        if (!$user) return new RedirectResponse($this->generateUrl('fos_user_security_login'));


        $event = new Event();
        $event->setCountry($contry);
        $event->setCity($city);
        $event->setUser($user);
        $event->setAddress('Район метро измайлово');
        $dateTime = new \DateTime();
        $date = $dateTime::createFromFormat('d.m.Y', '31.01.2014');
        // echo $date->format('Y-m-d'); exit;
        $event->setDate($date);
        $event->setTitle('Событие');
        $event->setDescription('И подскользнулась на разделительной полосе и обвинила одного из помогавших ей участников движения в совершении наезда на нее. Случилось все 14.01.2014 в районе 14:00. Кто что видел помогите пожалуйста, невиновному человеку грозит суд за то что он хотел сделать доброе дело Контактный телефон: 8-927-744-22-67 Владимир 8-937-646-37-15 Александр');
        $event->setCoords("55.79060467041366,37.77187057409667");
        $em = $this->getDoctrine()->getManager();
        $em->persist($event);
        $em->flush();


        return $this->render('SiteBandWitnessBundle:Default:save.html.twig', array( 'id' => $event->getId() ) );


    }
}
