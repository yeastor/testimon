<?php
/**
 * Created by PhpStorm.
 * User: KupriyanovYM
 * Date: 29.01.14
 * Time: 17:09
 */

namespace SiteBand\WitnessBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SiteBand\WitnessBundle\Utils\Helper;

class CityController extends Controller{

    public function allAction()
    {

        $cities = $this->getDoctrine()->getRepository('SiteBandWitnessBundle:City')
            ->findAll();


        $cityData = array();
        foreach ($cities as $city)
        {
            $cityData[] = array('label' => $city->getName()
            );

        }
        // yes, here we are retrieving "_format" from routing. In our case it's json
        $serializedEntity = $this->container->get('serializer')->serialize($cityData, 'json');
        $format = "json";//$request->getRequestFormat();

        $serializedEntityUnicodeToUtf = Helper::json_encode_cyr($serializedEntity);

        return $this->render('SiteBandWitnessBundle:City:all.html.twig', array("cities" => $serializedEntityUnicodeToUtf));


    }
} 