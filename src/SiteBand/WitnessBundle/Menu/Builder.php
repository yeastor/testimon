<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 11.12.13
 * Time: 16:04
 */

namespace SiteBand\WitnessBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

class Builder {
    public function mainMenu(FactoryInterface $factory, array $options)
    {


        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class','nav navbar-nav');


        $menu->addChild('Main Page', array('route' => 'site_band_witness_homepage'));
        $menu->addChild('Image Grabber', array(
            'route' => 'sbs_ig_index' ))->setLabel("Image Grabber");
        $menu->addChild('I-am-witness', array(
            'route' => 'site_band_witness_i_am_witness' ))->setLabel("I am witness");
        return $menu;
    }

    public function witnessMenu(FactoryInterface $factory, array $options)
    {


        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class','nav navbar-nav');


        $menu->addChild('Main Page', array('route' => 'site_band_witness_homepage'));
        $menu->addChild('I-am-witness', array(
            'route' => 'site_band_witness_i_am_witness' ))->setLabel("I am witness");
        $menu->addChild('Witness-search', array(
            'route' => 'site_band_witness_witness_search' ))->setLabel("Witness search");
        return $menu;
    }
} 