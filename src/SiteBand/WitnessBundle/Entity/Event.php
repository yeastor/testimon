<?php

namespace SiteBand\WitnessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use SiteBand\WitnessBundle\Utils\Helper;

/**
 * Event
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SiteBand\WitnessBundle\Entity\EventRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"address", "date", "coords","title","description"})
 */
class Event
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="text", nullable=true)
     */
    private $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="coords", type="string", length=255)
     */
    private $coords;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="embed", type="text", nullable=true)
     */
    private $embed;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="predate", type="datetime", nullable=true)
     */
    private $preDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="visitors", type="integer", nullable=true)
     */
    private $visitors;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="events")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="events")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
     */
    protected $city;

    /**
     * @ORM\ManyToOne(targetEntity="\SiteBand\UserBundle\Entity\User", inversedBy="events")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;


    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="event")
     */
    protected $comments;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Event
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Event
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set tags
     *
     * @param string $tags
     * @return Event
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Event
     */
    public function setDescription($description)
    {
        $conf = \HTMLPurifier_Config::createDefault();
        $purifier = new \HTMLPurifier($conf);
        $description = $purifier->purify($description); // clean_html

        $helpUtils = new Helper();
        $description = $helpUtils->embedTags($description);
        $this->description = strip_tags($description,'<a>');

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set country
     *
     * @param \SiteBand\WitnessBundle\Entity\Country $country
     * @return Event
     */
    public function setCountry(\SiteBand\WitnessBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \SiteBand\WitnessBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param \SiteBand\WitnessBundle\Entity\City $city
     * @return Event
     */
    public function setCity(\SiteBand\WitnessBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \SiteBand\WitnessBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }

   

    /**
     * Get user
     *
     * @return \SiteBand\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }



    /**
     * Set user
     *
     * @param \SiteBand\UserBundle\Entity\User $user
     * @return Event
     */
    public function setUser(\SiteBand\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Set coords
     *
     * @param string $coords
     * @return Event
     */
    public function setCoords($coords)
    {
        $this->coords = $coords;

        return $this;
    }

    /**
     * Get coords
     *
     * @return string 
     */
    public function getCoords()
    {
        return $this->coords;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add comments
     *
     * @param \SiteBand\WitnessBundle\Entity\Comment $comments
     * @return Event
     */
    public function addComment(\SiteBand\WitnessBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \SiteBand\WitnessBundle\Entity\Comment $comments
     */
    public function removeComment(\SiteBand\WitnessBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set comments
     *
     * @param integer $comments
     * @return Blog
     */
    public function setComments($comments) {
        $this->comments = $comments;
        return $this;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Event
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set embed
     *
     * @param string $embed
     * @return Event
     */
    public function setEmbed($embed)
    {
        $conf = \HTMLPurifier_Config::createDefault();
        $purifier = new \HTMLPurifier($conf);
        $embed = $purifier->purify($embed); // clean_html

        $helpUtils = new Helper();
        $embed = $helpUtils->embedTags($embed);
        $this->embed = strip_tags($embed,'<a><img>');

        return $this;
    }

    /**
     * Get embed
     *
     * @return string 
     */
    public function getEmbed()
    {
        return $this->embed;
    }

    /**
     * Save previous date if it changed for XML Event Update
     * @ORM\PreUpdate
     */
    public function preUpdate(\Doctrine\ORM\Event\PreUpdateEventArgs $event)
    {
        if ($event->hasChangedField('date')) {
           $this->preDate = $event->getOldValue('date');
        }
        else{
            $this->preDate = null;
        }

    }

    /**
     * Set preDate
     *
     * @param \DateTime $preDate
     * @return Event
     */
    public function setPreDate($preDate)
    {
        $this->preDate = $preDate;

        return $this;
    }

    /**
     * Get preDate
     *
     * @return \DateTime 
     */
    public function getPreDate()
    {
        return $this->preDate;
    }

    /**
     * Set visitors
     *
     * @param integer $visitors
     * @return Event
     */
    public function setVisitors($visitors)
    {
        $this->visitors = $visitors;

        return $this;
    }

    /**
     * Get visitors
     *
     * @return integer 
     */
    public function getVisitors()
    {
        return $this->visitors;
    }
}
