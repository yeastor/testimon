<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 21.01.14
 * Time: 16:47
 */

namespace SiteBand\WitnessBundle\Entity;
use Doctrine\ORM\EntityRepository;

class LogEventLastIdRepository extends EntityRepository {

    /*public function getMaxLogId()
    {
        $em = $this->getEntityManager();
        $repository = $em->getRepository('SiteBandWitnessBundle:LogEventLastId');
        $query = $repository->createQueryBuilder('l');
        $query->select('l, MAX(l.lastEventId) AS max_id');
        $query->getFirstResult();

        return $maxLogId = $query->getQuery()->getResult();

    }*/

    public function getMaxUpdateDate()
    {
        $em = $this->getEntityManager();
        $repository = $em->getRepository('SiteBandWitnessBundle:LogEventLastId');
        $query = $repository->createQueryBuilder('l');
        $query->select('l, MAX(l.lastEvenUpdateDate) AS max_date');
        $query->getFirstResult();

        return $maxLogUpdateDate = $query->getQuery()->getResult();
    }

} 