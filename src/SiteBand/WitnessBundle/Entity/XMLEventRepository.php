<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 08.10.2014
 * Time: 11:26
 */

namespace SiteBand\WitnessBundle\Entity;

use Doctrine\ORM\EntityRepository;

class XMLEventRepository  extends EntityRepository{

    public function findByBetweenDate($dateFrom,$dateTo)
    {
        // $date = "2014-01-15";
        // echo $date; exit;


        // $dateTo->format('Y-m-d H:i');
        $em = $this->getEntityManager();
        $repository = $em->getRepository('SiteBandWitnessBundle:XMLEvent');
        $query = $repository->createQueryBuilder('x');
        $query->where($query->expr()->between('x.date',':date_from',':date_to'))
            ->setParameter('date_from',$dateFrom,   \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('date_to',$dateTo, \Doctrine\DBAL\Types\Type::DATETIME);


        return $XMLevents = $query->getQuery()->getResult();

    }


} 