<?php

namespace SiteBand\WitnessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogEventLastId
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SiteBand\WitnessBundle\Entity\LogEventLastIdRepository")
 */
class LogEventLastId
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastEvenUpdateDate", type="datetime")
     */
    private $lastEvenUpdateDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastEventId
     *
     * @param integer $lastEventId
     * @return LogEventLastId
     */
    public function setLastEventId($lastEventId)
    {
        $this->lastEventId = $lastEventId;

        return $this;
    }

    /**
     * Get lastEventId
     *
     * @return integer 
     */
    public function getLastEventId()
    {
        return $this->lastEventId;
    }

    /**
     * Set lastEvenUpdateDate
     *
     * @param \DateTime $lastEvenUpdateDate
     * @return LogEventLastId
     */
    public function setLastEvenUpdateDate($lastEvenUpdateDate)
    {
        $this->lastEvenUpdateDate = $lastEvenUpdateDate;

        return $this;
    }

    /**
     * Get lastEvenUpdateDate
     *
     * @return \DateTime 
     */
    public function getLastEvenUpdateDate()
    {
        return $this->lastEvenUpdateDate;
    }
}
