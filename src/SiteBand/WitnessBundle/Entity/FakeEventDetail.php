<?php

namespace SiteBand\WitnessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * FakeEventDetail
 *
 * @ORM\Table(indexes={@ORM\Index(name="hash_indx", columns={"hash"})})
 * @ORM\Entity(repositoryClass="SiteBand\WitnessBundle\Entity\FakeEventDetailRepository")
 */
class FakeEventDetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="coords", type="string", length=255)
     */
    private $coords;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255)
     */
    private $hash;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\ManyToOne(targetEntity="FakeEvent", inversedBy="fakeeventsdetails")
     * @ORM\JoinColumn(name="fevent_id", referencedColumnName="id", nullable=false)
     */
    protected $fakeEvent;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return FakeEventDetail
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return FakeEventDetail
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set coords
     *
     * @param string $coords
     * @return FakeEventDetail
     */
    public function setCoords($coords)
    {
        $this->coords = $coords;

        return $this;
    }

    /**
     * Get coords
     *
     * @return string 
     */
    public function getCoords()
    {
        return $this->coords;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return FakeEventDetail
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return FakeEventDetail
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set fakeEvent
     *
     * @param \SiteBand\WitnessBundle\Entity\FakeEvent $fakeEvent
     * @return FakeEventDetail
     */
    public function setFakeEvent(\SiteBand\WitnessBundle\Entity\FakeEvent $fakeEvent)
    {
        $this->fakeEvent = $fakeEvent;

        return $this;
    }

    /**
     * Get fakeEvent
     *
     * @return \SiteBand\WitnessBundle\Entity\FakeEvent 
     */
    public function getFakeEvent()
    {
        return $this->fakeEvent;
    }
}
