<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 14.08.2014
 * Time: 15:24
 */

namespace SiteBand\YandexAPIBundle\Utils;

use Yandex\Disk\DiskClient;
use Symfony\Component\HttpFoundation\Response;

class YDisk {

    private $dickClient;


    public function __construct($accessToken){

        $this->dickClient = new DiskClient($accessToken);
        $this->dickClient->setServiceScheme(DiskClient::HTTPS_SCHEME);

        return $this->dickClient;
    }

    public function getThumb($target,$size){
        //Вывод превьюшки
        $file = $this->dickClient->getImagePreview($target, $size);
        $response = new Response();

        $response->headers->set('Content-Description','File Transfer');
        $response->headers->set('Connection','Keep-Alive');
        $response->headers->set('Expires','0');
        $response->headers->set('Cache-Control','must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Date',$file['headers']['date']);
        $response->headers->set('Content-Type','image/jpeg');
        $response->headers->set('Content-Length',$file['headers']['content-length']);
        //$response->headers->set('Accept-Ranges',$file['headers']['accept-ranges']);
        $response->setContent($file['body']);

        return $response;
    }

    public function moveImage($target,$destination){

        return $this->dickClient->move($target, $destination);
    }

    public function removeImg($target) {

        if ($this->dickClient->delete($target)) {
            return true;
        }
        return false;
    }
} 