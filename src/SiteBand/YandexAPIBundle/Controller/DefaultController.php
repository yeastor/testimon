<?php

namespace SiteBand\YandexAPIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SiteBand\YandexAPIBundle\Form\Type\ImageFileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Yandex\Disk\DiskClient;
use SiteBand\WitnessBundle\Utils\Helper;
use SiteBand\YandexAPIBundle\Utils\YDisk;

/*trait ControllerTrait
{
    protected function failUpload($messages){
        if (is_array($messages)) {
            $errors = $messages;
        }
        else {
            $errors = array();
            $errors[] = $messages;
        }
        $array = array( 'success' => false, 'message' => ($messages)); // data to return via JSON
        return $this->render('SiteBandYandexAPIBundle:Default:success.html.twig', array("result" => json_encode($array) ));
    }
}
*/

/**
 * @Route("/ystore")
 */
class DefaultController extends Controller
{
   // use ControllerTrait;

    /**
     * @Route("/upload/", name="site_band_yandex_api_upload", options={"expose"=true})
     * @Template()
     */
    public function uploadAction()
    {
        $translator = $this->get('translator');
        $request = Request::createFromGlobals();

        $form = $this->createForm(new ImageFileType());
        $form->handleRequest($request);

        if ($form->isValid()) {

            $formData = $form->getData();

            $internetUrl = $form['url']->getData();
            if ($internetUrl && $internetUrl != ""){
                if (!$this->checkInternetUrl($internetUrl)) return $this->failUpload($translator->trans('error.wrong-url'));
                $array = array( 'success' => true, 'url' => (array($internetUrl)), 'message' => (array($translator->trans('success.url')))); // data to return via JSON
                return $this->render('SiteBandYandexAPIBundle:Default:success.html.twig', array("result" => json_encode($array) ));
            }
            $fileName = $form['image']->getData();

            if ($fileName && !empty($fileName))
            {
                $allowedExtensions = array('jpe','jpeg','jpg','gif','png');
                $extension = $fileName->guessExtension();
                if (!in_array($extension,$allowedExtensions))
                {
                    return $this->failUpload($translator->trans('error.wrong-extension'));
                }

                $newName = hash_file('sha256', $fileName).'.'.$fileName->guessExtension();
                $helpUtils = new Helper();
                $helpUtils->generate_image_thumbnail($fileName,$fileName,800,600);
                $diskClient = new DiskClient('18ccc3a14c984d8dba90f6848cfad332');
                $diskClient->setServiceScheme(DiskClient::HTTPS_SCHEME);
                $diskClient->uploadFile(
                    '/tmp/',
                    array(
                        'path' => $fileName,
                        'size' => filesize($fileName),
                        'name' => $newName
                    )
                );
                $array = array( 'success' => true, 'new_file' => (array($newName)), 'message' => (array($translator->trans('success.file')))); // data to return via JSON
            }
            else {
                return $this->failUpload($translator->trans('error.big-file'));
            }

            return $this->render('SiteBandYandexAPIBundle:Default:success.html.twig', array("result" => json_encode($array) ));

        }

        $view = $form->createView();

        return array('form' => $view );
    }

    /**
     * show thumbnail image from Yandex Disk
     * @Route("/images/{folder}/{file}", name="site_band_yandex_api_images", options={"expose"=true})
     */
    public function imageAction($folder,$file)
    {
        define('YDISK_RESOURCE_NOT_FOUND','not found');
        $response = null;
        $allowedFolder = array('tmp','events');
        $sizes = array('tmp' => 'XS', 'events' => '800x600');
        $diskClient = new YDisk('18ccc3a14c984d8dba90f6848cfad332');
        $target = "/".$folder."/".$file;
        if ($folder == 'events'){
            $response = $diskClient->getThumb($target,$sizes[$folder]);

            if (stristr($response->getContent(),YDISK_RESOURCE_NOT_FOUND)) // if no file in events move it from tmp
            {
                $moveFrom = '/tmp'."/".$file;
                $res = $diskClient->moveImage($moveFrom,$target);
                $response = $diskClient->getThumb($target,$sizes[$folder]);
            }
        }
        elseif ($folder == 'tmp') {

            $response = $diskClient->getThumb($target,$sizes[$folder]);

        }
        return $response;
    }

    /**
     * show thumbnail image from Yandex Disk
     * @Route("/remove/{file}", name="site_band_yandex_api_remove", options={"expose"=true})
     */
    public function removeAction($file)
    {
        $request = Request::createFromGlobals();

        if ($request->isXmlHttpRequest())
        {
            $folder = 'tmp';
            $vowels = array("*", "/", ".."); // security url exclude path as "../file.jpeg, *.jpeg" etc.
            $file = str_replace($vowels,"",$file);

            $diskClient = new YDisk('18ccc3a14c984d8dba90f6848cfad332');
            $result = $diskClient->removeImg($folder."/".$file);
            if ($result)  $array = array( 'success' => true); // data to return via JSON
            else  $array = array( 'success' => false); // data to return via JSON

            $response = new Response( json_encode( $array ) );
            $response->headers->set( 'Content-Type', 'application/json' );
            return $response;
        }
        return new RedirectResponse($this->generateUrl('site_band_witness_homepage'));




    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }

    protected function checkInternetUrl($url){
        $pattern = "/^http:\/\//i";
        return preg_match($pattern, $url);
    }

    protected function failUpload($messages){
        if (is_array($messages)) {
            $errors = $messages;
        }
        else {
            $errors = array();
            $errors[] = $messages;
        }
        $array = array( 'success' => false, 'message' => ($errors)); // data to return via JSON
        return $this->render('SiteBandYandexAPIBundle:Default:success.html.twig', array("result" => json_encode($array) ));
    }
}
