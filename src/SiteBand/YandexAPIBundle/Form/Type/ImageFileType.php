<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 13.08.2014
 * Time: 14:41
 */

namespace SiteBand\YandexAPIBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ImageFileType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('image','file',array(
            'label' => 'label.image',
            'attr' => array('class' => 'img-uploader',
                            'accept' => 'image/jpeg,image/png,image/gif'),
            'required' => false
        ));

        $builder->add('url','text',array(
            'label' => 'label.url',
            'attr' => array('class' => 'img-url form-control'),
            'required' => false
        ));

        $builder->add('save', 'submit', array(
            'label' => 'label.add',
            'attr' => array('class' => 'btn btn-primary'),
        ));


    }

    public function getName()
    {
        return 'imagefile';
    }
} 