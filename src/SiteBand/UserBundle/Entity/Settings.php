<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 23.10.2014
 * Time: 14:00
 */

namespace SiteBand\UserBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

class Settings {

    protected $settings;

    public function __construct()
    {
        $this->settings = new ArrayCollection();
    }

    public function getSettings()
    {
        return $this->settings;
    }



} 