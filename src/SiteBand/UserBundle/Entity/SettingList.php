<?php

namespace SiteBand\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\UniqueConstraint;
/**
 * SettingList
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SiteBand\UserBundle\Entity\SettingListRepository")
 */
class SettingList
{

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_value_int", type="integer", nullable=true)
     */
    private $defaultValueInt;

    /**
     * @var string
     *
     * @ORM\Column(name="default_value_str", type="string", length=255, nullable=true)
     */
    private $defaultValueStr;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
     * @ORM\OneToMany(targetEntity="\SiteBand\UserBundle\Entity\Setting", mappedBy="setting")
     */
    protected $settings;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SettingList
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return SettingList
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set defaultValueInt
     *
     * @param integer $defaultValueInt
     * @return SettingList
     */
    public function setDefaultValueInt($defaultValueInt)
    {
        $this->defaultValueInt = $defaultValueInt;

        return $this;
    }

    /**
     * Get defaultValueInt
     *
     * @return integer 
     */
    public function getDefaultValueInt()
    {
        return $this->defaultValueInt;
    }

 

    /**
     * Set description
     *
     * @param string $description
     * @return SettingList
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set defaultValueStr
     *
     * @param string $defaultValueStr
     * @return SettingList
     */
    public function setDefaultValueStr($defaultValueStr)
    {
        $this->defaultValueStr = $defaultValueStr;

        return $this;
    }

    /**
     * Get defaultValueStr
     *
     * @return string 
     */
    public function getDefaultValueStr()
    {
        return $this->defaultValueStr;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->settings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add settings
     *
     * @param \SiteBand\UserBundle\Entity\Setting $settings
     * @return SettingList
     */
    public function addSetting(\SiteBand\UserBundle\Entity\Setting $settings)
    {
        $this->settings[] = $settings;

        return $this;
    }

    /**
     * Remove settings
     *
     * @param \SiteBand\UserBundle\Entity\Setting $settings
     */
    public function removeSetting(\SiteBand\UserBundle\Entity\Setting $settings)
    {
        $this->settings->removeElement($settings);
    }

    /**
     * Get settings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Set settings
     *
     * @param \SiteBand\UserBundle\Entity\Setting $settings
     * @return SettingList
     */
    public function setSettings(\SiteBand\UserBundle\Entity\Setting $settings = null)
    {
        $this->settings = $settings;

        return $this;
    }
}
