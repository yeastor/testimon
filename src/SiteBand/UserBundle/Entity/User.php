<?php
/**
 * Created by PhpStorm.
 * User: Kupriyanovym
 * Date: 27.12.13
 * Time: 14:29
 */

namespace SiteBand\UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */

class User extends BaseUser {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="\SiteBand\WitnessBundle\Entity\Event", mappedBy="user")
     */
    protected $events;

    /**
     * @ORM\OneToMany(targetEntity="\SiteBand\WitnessBundle\Entity\Comment", mappedBy="user")
     */
    protected $comments;

    /**
     * @ORM\OneToMany(targetEntity="\SiteBand\ContentBundle\Entity\StaticPage", mappedBy="user")
     */
    protected $staticpages;

    /**
     * @ORM\OneToMany(targetEntity="\SiteBand\UserBundle\Entity\Setting", mappedBy="user")
     */
    protected $settings;

    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->events = new ArrayCollection();
        $this->groups = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add events
     *
     * @param \SiteBand\WitnessBundle\Entity\Event $events
     * @return User
     */
    public function addEvent(\SiteBand\WitnessBundle\Entity\Event $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param \SiteBand\WitnessBundle\Entity\Event $events
     */
    public function removeEvent(\SiteBand\WitnessBundle\Entity\Event $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add comments
     *
     * @param \SiteBand\WitnessBundle\Entity\Comment $comments
     * @return User
     */
    public function addComment(\SiteBand\WitnessBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \SiteBand\WitnessBundle\Entity\Comment $comments
     */
    public function removeComment(\SiteBand\WitnessBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    protected  $groups;


    /**
     * Add staticpages
     *
     * @param \SiteBand\ContentBundle\Entity\StaticPage $staticpages
     * @return User
     */
    public function addStaticpage(\SiteBand\ContentBundle\Entity\StaticPage $staticpages)
    {
        $this->staticpages[] = $staticpages;

        return $this;
    }

    /**
     * Remove staticpages
     *
     * @param \SiteBand\ContentBundle\Entity\StaticPage $staticpages
     */
    public function removeStaticpage(\SiteBand\ContentBundle\Entity\StaticPage $staticpages)
    {
        $this->staticpages->removeElement($staticpages);
    }

    /**
     * Get staticpages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStaticpages()
    {
        return $this->staticpages;
    }

    /**
     * Add groups
     *
     * @param \Application\Sonata\UserBundle\Entity\Group $groups
     * @return User
     */
    public function addGroup(\FOS\UserBundle\Model\GroupInterface $groups)
    {
        $this->groups[] = $groups;

        return $this;
    }

    /**
     * Remove groups
     *
     * @param \Application\Sonata\UserBundle\Entity\Group $groups
     */
    public function removeGroup(\FOS\UserBundle\Model\GroupInterface $groups)
    {
        $this->groups->removeElement($groups);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroups()
    {
        return $this->groups;
    }

    

    /**
     * Get settings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Add settings
     *
     * @param \SiteBand\UserBundle\Entity\Setting $settings
     * @return User
     */
    public function addSetting(\SiteBand\UserBundle\Entity\Setting $settings)
    {
        $this->settings[] = $settings;

        return $this;
    }

    /**
     * Remove settings
     *
     * @param \SiteBand\UserBundle\Entity\Setting $settings
     */
    public function removeSetting(\SiteBand\UserBundle\Entity\Setting $settings)
    {
        $this->settings->removeElement($settings);
    }
}
