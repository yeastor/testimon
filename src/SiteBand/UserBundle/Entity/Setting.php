<?php

namespace SiteBand\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Setting
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SiteBand\UserBundle\Entity\SettingRepository")
 */
class Setting
{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="\SiteBand\UserBundle\Entity\User", inversedBy="settings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="\SiteBand\UserBundle\Entity\SettingList", inversedBy="settings")
     * @ORM\JoinColumn(name="setting", referencedColumnName="name", nullable=false)
     */
    private $setting;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="value_int", type="integer", nullable=true)
     */
    private $valueInt;

    /**
     * @var string
     *
     * @ORM\Column(name="value_str", type="string", length=255, nullable=true)
     */
    private $valueStr;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Setting
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Setting
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set valueInt
     *
     * @param integer $valueInt
     * @return Setting
     */
    public function setValueInt($valueInt)
    {
        $this->valueInt = $valueInt;

        return $this;
    }

    /**
     * Get valueInt
     *
     * @return integer 
     */
    public function getValueInt()
    {
        return $this->valueInt;
    }

    /**
     * Set valueStr
     *
     * @param string $valueStr
     * @return Setting
     */
    public function setValueStr($valueStr)
    {
        $this->valueStr = $valueStr;

        return $this;
    }

    /**
     * Get valueStr
     *
     * @return string 
     */
    public function getValueStr()
    {
        return $this->valueStr;
    }

    /**
     * Set user
     *
     * @param \SiteBand\UserBundle\Entity\User $user
     * @return Setting
     */
    public function setUser(\SiteBand\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SiteBand\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set setting
     *
     * @param \SiteBand\UserBundle\Entity\SettingList $setting
     * @return Setting
     */
    public function setSetting(\SiteBand\UserBundle\Entity\SettingList $setting)
    {
        $this->setting = $setting;

        return $this;
    }

    /**
     * Get setting
     *
     * @return \SiteBand\UserBundle\Entity\SettingList 
     */
    public function getSetting()
    {
        return $this->setting;
    }
}
