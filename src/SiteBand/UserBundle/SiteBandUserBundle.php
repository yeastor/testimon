<?php

namespace SiteBand\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SiteBandUserBundle extends Bundle
{
    public function getParent()
    {
        return 'ApplicationSonataUserBundle';
    }
}
