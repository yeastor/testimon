<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 22.10.2014
 * Time: 13:57
 */

namespace SiteBand\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SettingListAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

            ->add('name','text')

            ->add('type', 'integer')


            ->add('default_value_int','integer', array('required' => false))

            ->add('default_value_str','text', array('required' => false))

            ->add('description','text', array('required' => false))
        ;

    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('default_value_int')
            ->add('default_value_str')
        ;
    }

} 