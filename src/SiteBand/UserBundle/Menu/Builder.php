<?php
/**
 * Created by PhpStorm.
 * User: Kupriyanovym
 * Date: 20.05.14
 * Time: 11:30
 */

namespace SiteBand\UserBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

class Builder {

    public function profileMenu(FactoryInterface $factory, array $options)
    {

        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class','nav nav-pills nav-stacked');


        $menu->addChild('Profile', array('route' => 'fos_user_profile_show'))->setLabel("Profile");
        $menu->addChild('Change-password', array(
            'route' => 'fos_user_change_password' ))->setLabel("Change password");
        $menu->addChild('User-messages', array(
            'route' => 'site_band_witness_user_messages' ))->setLabel("My events");
        $menu->addChild('User-settings', array(
                'route' => 'site_band_witness_user_settings' ))->setLabel("profiler.settings");
        return $menu;
    }

} 