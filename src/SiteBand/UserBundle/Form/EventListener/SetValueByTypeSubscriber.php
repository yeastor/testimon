<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 23.10.2014
 * Time: 16:09
 */
namespace SiteBand\UserBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class SetValueByTypeSubscriber implements EventSubscriberInterface{
    public static function getSubscribedEvents()
    {
        // Tells the dispatcher that you want to listen on the form.pre_set_data
        // event and that the preSetData method should be called.
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $setting = $event->getData();
        $form = $event->getForm();

        $settingType = $setting->getType();

        if ($setting) {


            if ($settingType == 0)
            {
                $value = ($setting->getValueInt() == 0) ? false : true;
                $form->add('valueInt' ,'checkbox', array('label' => false, 'data' => $value, 'required' => false,'attr' => array('class' => 'onoffswitch-checkbox')));
            }
        }
    }

} 