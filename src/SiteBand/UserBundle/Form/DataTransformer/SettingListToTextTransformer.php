<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 23.10.2014
 * Time: 15:01
 */
namespace SiteBand\UserBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use SiteBand\UserBundle\Entity\Setting;

class SettingListToTextTransformer implements DataTransformerInterface{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  Issue|null $issue
     * @return string
     */
    public function transform($settingList)
    {
        if (null === $settingList) {
            return "";
        }

        return $settingList->getName();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $number
     *
     * @return Issue|null
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($name)
    {
        if (!$name) {
            return null;
        }

        $settingList = $this->om
            ->getRepository('SiteBandUserBundle:SettingList')
            ->findOneBy(array('name' => $name))
        ;

        if (null === $settingList) {
            throw new TransformationFailedException(sprintf(
                    'An issue with number "%s" does not exist!',
                    $settingList
                ));
        }

        return $settingList;
    }
} 