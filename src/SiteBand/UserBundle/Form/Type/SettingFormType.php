<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 22.10.2014
 * Time: 17:15
 */

namespace SiteBand\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use SiteBand\UserBundle\Form\DataTransformer\SettingListToTextTransformer;
use SiteBand\UserBundle\Form\EventListener\SetValueByTypeSubscriber;

class SettingFormType extends AbstractType {

    public function __construct (array $defaultOptions = null)
    {
        $this->em = isset ($defaultOptions['em']) ? $this->em = $defaultOptions['em'] : null;

    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $this->em;
        $transformer = new SettingListToTextTransformer($entityManager);

        $builder->add($builder->create('setting','hidden')
            ->addModelTransformer($transformer));

        $builder
            //->add('user' ,null, array('label' => 'name'))
            //->add('setting', 'hidden', array('label' => 'setting'))
            //->add('valueInt' ,'text', array('label' => 'valueInt'))
            //->add('valueStr' ,'text', array('label' => 'valueStr'))
            ->add('type' ,'hidden', array('label' => 'type'));

        $builder->addEventSubscriber(new SetValueByTypeSubscriber());

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'SiteBand\UserBundle\Entity\Setting',
            ));

    }


    public function getName()
    {
        return "ub_setting";
    }



} 