<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 16.05.14
 * Time: 14:14
 */

namespace SiteBand\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;

class ProfileFormType extends BaseType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // add your custom field
        $builder->remove('username');
        $builder->remove('email');
    }

    public function getName()
    {
        return 'siteband_user_profile';
    }

} 