<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 23.10.2014
 * Time: 13:56
 */

namespace SiteBand\UserBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SettingsFormType extends AbstractType {



    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];

        $builder
            ->add('settings', 'collection', array('label' => 'profiler.settings',  'options' => array('label' => false), 'type' => new SettingFormType(array('em' => $entityManager))));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'SiteBand\UserBundle\Entity\Settings',
            ))
            ->setRequired(array(
                    'em',
                ))
            ->setAllowedTypes(array(
                    'em' => 'Doctrine\Common\Persistence\ObjectManager',
                ));
    }


    public function getName()
    {
        return "ub_settings";
    }

} 