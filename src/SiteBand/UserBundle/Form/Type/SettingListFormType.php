<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 22.10.2014
 * Time: 17:49
 */

namespace SiteBand\UserBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SettingListFormType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name' ,null, array('label' => 'name'));
        //if ($this->addCaptcha){
        //  $builder->add('captcha', 'captcha', array('reload' => true, 'as_url'=>true));
        //}
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'SiteBand\UserBundle\Entity\SettingList',
            ));
    }


    public function getName()
    {
        return "ub_settinglist";
    }
} 