<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 31.03.14
 * Time: 14:44
 */

namespace SiteBand\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegistrationController extends BaseController{
    public function registerAction($modal = false)
    {

        $form = $this->container->get('fos_user.registration.form');
        $formHandler = $this->container->get('fos_user.registration.form.handler');
        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

        $process = $formHandler->process($confirmationEnabled);
        $request = Request::createFromGlobals();
        if ($process) {
            $user = $form->getData();
            $array = array();
            $authUser = false;
            if ($confirmationEnabled) {
                $this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());
                $route = 'fos_user_registration_check_email';
                $array = array( 'success' => true, 'resType' => 'registration_check_email', 'message' => $this->container->get('translator')->trans('registration.check_email',array('email' => $user->getEmail()), 'FOSUserBundle') ); // data to return via JSON
            } else {
                $authUser = true;
                $route = 'fos_user_registration_confirmed';
                $array = array( 'success' => true, 'resType' => 'registration_confirmed', 'message' => $this->container->get('translator')->trans('registration.back',array(), 'FOSUserBundle') ); // data to return via JSON
            }

            $this->setFlash('fos_user_success', 'registration.flash.user_created');
            $url = $this->container->get('router')->generate($route);
            $response = new RedirectResponse($url);

            if ($authUser) {
                $this->authenticateUser($user, $response);
            }

            if ($request->isXmlHttpRequest())
            {
                    $response = new Response( json_encode( $array ) );
                    $response->headers->set( 'Content-Type', 'application/json' );
                    return $response;
            }
            return $response;
        }

        if ($modal){


                return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:register-modal.html.'.$this->getEngine(), array(
                    'form' => $form->createView(),
                ));

        }
        elseif ($request->isXmlHttpRequest() == 'POST')
        {
            $string = $this->getErrorMessages($form);

            $array = array( 'success' => false, 'message' => ($string)); // data to return via JSON
            $response = new Response( json_encode( $array ) );
            $response->headers->set( 'Content-Type', 'application/json' );

            return $response;
        }

        else{

            return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:register.html.'.$this->getEngine(), array(
                    'form' => $form->createView(),
                ));
        }


    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }

} 