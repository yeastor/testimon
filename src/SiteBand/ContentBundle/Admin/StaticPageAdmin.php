<?php
/**
 * Created by PhpStorm.
 * User: KUpriyanovym
 * Date: 02.06.2014
 * Time: 13:28
 */
namespace SiteBand\ContentBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class StaticPageAdmin extends Admin {
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title','text', array('label' => 'Title'))
            ->add('content','textarea', array('label' => 'Content'))
            ->add('alias','text', array('label' => 'Alias'))
        ;


        //$formMapper->add('comments' ,'textarea', array('label' => 'Comment text', 'class' => 'SiteBand\WitnessBundle\Entity\Comment'));
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('user')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('alias')
            ->add('user')
            ->add('updated')
        ;
    }

} 