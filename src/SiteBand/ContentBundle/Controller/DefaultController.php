<?php

namespace SiteBand\ContentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function indexAction($name)
    {
        return $this->render('SiteBandContentBundle:Default:index.html.twig', array('name' => $name));
    }
}
