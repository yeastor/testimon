<?php
/**
 * Created by PhpStorm.
 * User: KUpriyanovym
 * Date: 04.06.2014
 * Time: 11:48
 */

namespace SiteBand\ContentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SiteBand\ContentBundle\Entity\StaticPage;

/**
 * @Route("/content")
 */
class PageController extends Controller {
    /**
     * @param $alias
     * @return mixed
     * @Route("/{alias}")
     * @Template()
     */
    public function getAction($alias)
    {
        $page = $this->getDoctrine()->getRepository('SiteBandContentBundle:StaticPage')
            ->findOneByAlias($alias);

        return array('page' => $page);
    }
} 