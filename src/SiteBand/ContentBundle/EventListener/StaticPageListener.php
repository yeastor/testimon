<?php
/**
 * Created by PhpStorm.
 * User: KUpriyanovym
 * Date: 04.06.2014
 * Time: 14:30
 */

namespace SiteBand\ContentBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use SiteBand\ContentBundle\Entity\StaticPage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StaticPageListener {

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function prePersist(LifeCycleEventArgs $args)
    {
        $entity = $args->getEntity();


        if ($entity instanceof StaticPage)
        {
            $securityContext = $this->container->get('security.context');
            $user = $securityContext->getToken()->getUser();
            $entity->setUser($user);
        }
    }
} 