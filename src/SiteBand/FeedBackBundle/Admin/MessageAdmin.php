<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 05.08.2014
 * Time: 15:57
 */

namespace SiteBand\FeedBackBundle\Admin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MessageAdmin extends Admin {
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('category','entity',array('class' => 'SiteBand\FeedBackBundle\Entity\CategoryMessage','property' => 'name'))
            ->add('message','text')
            ->add('email','text')
            ->add('userInfo','textarea')
            ->add('updated','date')

        ;


        //$formMapper->add('comments' ,'textarea', array('label' => 'Comment text', 'class' => 'SiteBand\WitnessBundle\Entity\Comment'));
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('updated')

        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('updated')


        ;
    }
} 