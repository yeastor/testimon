<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 24.07.2014
 * Time: 15:39
 */

namespace SiteBand\FeedBackBundle\Admin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CategoryMessageAdmin extends Admin {
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name','text')
       ;


        //$formMapper->add('comments' ,'textarea', array('label' => 'Comment text', 'class' => 'SiteBand\WitnessBundle\Entity\Comment'));
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')

        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')


        ;
    }

} 