<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 24.07.2014
 * Time: 15:18
 */

namespace SiteBand\FeedBackBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MessageType extends AbstractType {

    public function __construct (array $defaultOptions = null)
    {
        $this->addCaptcha = ($defaultOptions['no-captcha']) ? false : true;

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message','textarea',array(
                'label' => 'feedback.message',
                'attr' => array (
                        'class' => "form-control")
                )
            )
            ->add('email','text',
                array(
                    'required' => false,
                    'label' => 'E-mail',
                    'constraints' => array(
                        new Email()
                    )
                ,'attr' => array (
                    'class' => "form-control")
                )
            )
            ->add('category','entity',
                array('label' => 'feedback.category',
                    'class' => 'SiteBand\FeedBackBundle\Entity\CategoryMessage',
                    'property' => 'name'
                ,'attr' => array ('class' => "form-control message")
                ))
            ->add('userInfo', 'hidden', array('attr' => array('class' => 'user-info')));
            if ($this->addCaptcha) {
                $builder->add('messagecaptcha', 'captcha', array('label' => 'captcha.name', 'reload' => true, 'as_url' => true));
            }
            $builder->add('save', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SiteBand\FeedBackBundle\Entity\Message',
        ));
    }

    public function getName()
    {
        return 'message';
    }

} 