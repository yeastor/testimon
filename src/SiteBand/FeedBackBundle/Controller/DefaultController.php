<?php

namespace SiteBand\FeedBackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SiteBand\FeedBackBundle\Form\Type\MessageType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SiteBand\FeedBackBundle\Entity\Message;
use Symfony\Component\HttpFoundation\RedirectResponse;
/**
 * @Route("/feedback")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/add/", name="site_band_feedback_add", options={"expose"=true})
     * @Template()
     */
    public function addAction()
    {
        $request = Request::createFromGlobals();

        if (!$request->isXmlHttpRequest()) {
            return new RedirectResponse($this->generateUrl('site_band_witness_homepage'));
        }

        $isLoggedUser = $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY');

        $defaultOptions['no-captcha'] = ($isLoggedUser) ? true : false;

        $form = $this->createForm(new MessageType($defaultOptions));



        $form->handleRequest($request);

        if ($form->isValid()) {

            $formData = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $message = new Message();
            $message->setMessage($formData->getMessage());
            $message->setEmail($formData->getEmail());
            $message->setUserInfo($formData->getUserInfo());
            $message->setCategory($formData->getCategory());

            $em->persist($message);
            $em->flush();
            $array = array( 'success' => true); // data to return via JSON
            if ($request->isXmlHttpRequest())
            {
                $response = new Response( json_encode( $array ) );
                $response->headers->set( 'Content-Type', 'application/json' );
                return $response;
            }
            return new RedirectResponse($this->generateUrl('site_band_witness_homepage'));


        }

        if ($request->isXmlHttpRequest() && $request->getMethod() == 'POST' )
        {
            $string = $this->getErrorMessages($form);

            $array = array( 'success' => false, 'message' => ($string)); // data to return via JSON
            $response = new Response( json_encode( $array ) );
            $response->headers->set( 'Content-Type', 'application/json' );

            return $response;
        }

        $view = $form->createView();

        return array('form' => $view );
    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
