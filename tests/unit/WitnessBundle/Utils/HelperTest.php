<?php
/**
 * Created by PhpStorm.
 * User: kupriyanovym
 * Date: 30.09.2014
 * Time: 14:59
 */
use Codeception\Util\Stub;
use SiteBand\WitnessBundle\Utils\Helper;


class HelperTest extends \Codeception\TestCase\Test
{

    /**
     * @var \CodeGuy
     */
    protected $codeGuy;

    private $helper;

    protected function _before()
    {

    }

    protected function _after()
    {
    }

    // tests
    public function testConvertStringDateToDatetimeByFormat()
    {

        $this->helper = new Helper();
        $strDate = '26/08/2014';
        $dateTime = $this->helper->convertStringDateToDatetimeByFormat($strDate,'d/m/Y');
        $this->assertEquals($strDate,$dateTime->format( 'd/m/Y' ));

    }


    public function testConvertDatetimeToStringByFormat()
    {

        $this->helper = new Helper();
        $dateTime = new \DateTime('26.08.2014 00:00:00');
        $dateFormat = 'd.m.Y';
        $strDate = $this->helper->convertDatetimeToStringByFormat($dateTime,$dateFormat);
        $this->assertEquals($strDate,$dateTime->format($dateFormat));

    }

    public function testEmbedTags()
    {
        $this->helper = new Helper();
        $text = "html data with tag for example http://ya.ru this tag";
        $embed = $this->helper->embedTags($text);
        $this->assertEquals(" html data with tag for example <a href='http://ya.ru' target='_blank'>http://ya.ru</a> this tag ",$embed);

        $text = "html data with tag for example http://ya.ru#tag this tag";
        $embed = $this->helper->embedTags($text);
        $this->assertEquals(" html data with tag for example <a href='http://ya.ru#tag' target='_blank'>http://ya.ru#tag</a> this tag ",$embed);

        $text = "Source: http://www.nn.ru/news/incidents/2014/08/29/ text go";

        $embed = $this->helper->embedTags($text);
        $this->assertEquals(" Source: <a href='http://www.nn.ru/news/incidents/2014/08/29/' target='_blank'>http://www.nn.ru/news/incidents/2014/08/29/</a> text go ",$embed);

        $text = "Source: http://www.nn.ru/news/incidents/2014/08/29/";
        $embed = $this->helper->embedTags($text);
        $this->assertEquals(" Source: <a href='http://www.nn.ru/news/incidents/2014/08/29/' target='_blank'>http://www.nn.ru/news/incidents/2014/08/29/</a> ",$embed);

        //\Codeception\Util\Debug::debug(">".$embed."<");
    }

    public  function  testGetWeekDateInterval(){

        $neededDate = new \DateTime('2014-08-06');
        $datesFromTo = Helper::getWeekDateInterval($neededDate);
        $this->assertEquals($datesFromTo['dateFrom']->format('d/m/Y'),'04/08/2014');
        $this->assertEquals($datesFromTo['dateTo']->format('d/m/Y'),'10/08/2014');

        $neededDate = new \DateTime('2014-08-04');
        $datesFromTo = Helper::getWeekDateInterval($neededDate);
        $this->assertEquals($datesFromTo['dateFrom']->format('d/m/Y'),'04/08/2014');
        $this->assertEquals($datesFromTo['dateTo']->format('d/m/Y'),'10/08/2014');

        $neededDate = new \DateTime('2014-08-04');
        $datesFromTo = Helper::getWeekDateInterval($neededDate);
        $this->assertEquals($datesFromTo['dateFrom']->format('d/m/Y'),'04/08/2014');
        $this->assertEquals($datesFromTo['dateTo']->format('d/m/Y'),'10/08/2014');

        $neededDate = new \DateTime('2014-08-10');
        $datesFromTo = Helper::getWeekDateInterval($neededDate);
        $this->assertEquals($datesFromTo['dateFrom']->format('d/m/Y'),'04/08/2014');
        $this->assertEquals($datesFromTo['dateTo']->format('d/m/Y'),'10/08/2014');

        $neededDate = new \DateTime('now');
        $dateFrom = clone $neededDate;
        $dateFrom->sub(new \DateInterval('P7D'));
        $datesFromTo = Helper::getWeekDateInterval($neededDate);
        $this->assertEquals($datesFromTo['dateFrom']->format('d/m/Y'),$dateFrom->format('d/m/Y'));


    }
}