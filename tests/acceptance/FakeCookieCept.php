<?php
use Codeception\Util\Stub;

/**
 * Created by PhpStorm.
 * User: Kupriyanovym
 * Date: 21.01.2015
 * Time: 16:06
 */
$I = new WebGuy($scenario);
$I->wantTo('test fake event');
$I->amOnPage("/");
$I->seeCookie('firstime');


$cookie =  'bb321b0242000cdc64f1551cb004c367';
$date = "10%2F12%2F2014";
$url = 'week-map.xml?date='.$date.'&rand=505&fevent=1&coords=55.804997%2C37.543084&name=%D1%83%D0%BB%D0%B8%D1%86%D0%B0+%D0%90%D0%BA%D0%B0%D0%B4%D0%B5%D0%BC%D0%B8%D0%BA%D0%B0+%D0%98%D0%BB%D1%8C%D1%8E%D1%88%D0%B8%D0%BD%D0%B0&hash='.$cookie;
//$I->amOnPage($url);
/*$params = array('date' => $date
                ,'fevent' => 1
                ,'rand' => 505
                ,'coords' => '55.804997%2C37.543084'
                ,'name' => '%D1%83%D0%BB%D0%B8%D1%86%D0%B0+%D0%90%D0%BA%D0%B0%D0%B4%D0%B5%D0%BC%D0%B8%D0%BA%D0%B0+%D0%98%D0%BB%D1%8C%D1%8E%D1%88%D0%B8%D0%BD%D0%B0'
                ,'hash' => $cookie);*/
$I->sendGET($url);
$I->seeResponseIsXml();
$I->seeResponseContains('event-');

$date = "14%2F12%2F2014";
$url = 'week-map.xml?date='.$date.'&rand=505&fevent=1&coords=55.804997%2C37.543084&name=%D1%83%D0%BB%D0%B8%D1%86%D0%B0+%D0%90%D0%BA%D0%B0%D0%B4%D0%B5%D0%BC%D0%B8%D0%BA%D0%B0+%D0%98%D0%BB%D1%8C%D1%8E%D1%88%D0%B8%D0%BD%D0%B0&hash='.$cookie;
$I->sendGET($url);
$I->seeResponseIsXml();
$I->seeResponseContains('event-');

$date = "15%2F12%2F2014";
$url = 'week-map.xml?date='.$date.'&rand=505&fevent=1&coords=55.804997%2C37.543084&name=%D1%83%D0%BB%D0%B8%D1%86%D0%B0+%D0%90%D0%BA%D0%B0%D0%B4%D0%B5%D0%BC%D0%B8%D0%BA%D0%B0+%D0%98%D0%BB%D1%8C%D1%8E%D1%88%D0%B8%D0%BD%D0%B0&hash='.$cookie;
$I->sendGET($url);
$I->dontSeeResponseContains('event-');


$date = "09%2F12%2F2014";
$url = 'map.xml?date='.$date.'&rand=505&fevent=1&coords=55.804997%2C37.543084&name=%D1%83%D0%BB%D0%B8%D1%86%D0%B0+%D0%90%D0%BA%D0%B0%D0%B4%D0%B5%D0%BC%D0%B8%D0%BA%D0%B0+%D0%98%D0%BB%D1%8C%D1%8E%D1%88%D0%B8%D0%BD%D0%B0&hash='.$cookie;
$I->sendGET($url);
$I->seeResponseIsXml();
$I->seeResponseContains('event-');

$date = "10%2F12%2F2014";
$url = 'map.xml?date='.$date.'&rand=505&fevent=1&coords=55.804997%2C37.543084&name=%D1%83%D0%BB%D0%B8%D1%86%D0%B0+%D0%90%D0%BA%D0%B0%D0%B4%D0%B5%D0%BC%D0%B8%D0%BA%D0%B0+%D0%98%D0%BB%D1%8C%D1%8E%D1%88%D0%B8%D0%BD%D0%B0&hash='.$cookie;
$I->sendGET($url);
$I->dontSeeResponseContains('event-');
