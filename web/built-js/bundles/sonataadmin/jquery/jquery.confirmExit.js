/*!
* jQuery confirmExit plugin
* https://github.com/dunglas/jquery.confirmExit
*
* Copyright 2012 Kévin Dunglas <dunglas@gmail.com>
* Released under the MIT license
* http://www.opensource.org/licenses/mit-license.php
*/

(function(e){e.fn.confirmExit=function(){return e(this).attr("data-original",e(this).serialize()),e(this).on("submit",function(){e(this).removeAttr("data-original")}),e(this)},e(window).on("beforeunload",function(t){var n=t||window.event,r=window.SONATA_TRANSLATIONS.CONFIRM_EXIT,i=!1;e("form[data-original]").each(function(){if(e(this).attr("data-original")!==e(this).serialize()){i=!0;return}});if(i)return n&&(n.returnValue=r),r})})(jQuery);