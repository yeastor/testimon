<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"  xmlns:ymaps="http://maps.yandex.ru/ymaps/1.x" xmlns:gml="http://www.opengis.net/gml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">

    <ymaps:ymaps xmlns:ymaps="http://maps.yandex.ru/ymaps/1.x" xmlns:repr="http://maps.yandex.ru/representation/1.x" xmlns:gml="http://www.opengis.net/gml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maps.yandex.ru/schemas/ymaps/1.x/ymaps.xsd">
        <ymaps:GeoObjectCollection >
                <gml:name>Объекты карте</gml:name>
                <gml:featureMembers>
                    <xsl:apply-templates select="//event"></xsl:apply-templates>
                </gml:featureMembers>
            </ymaps:GeoObjectCollection>
        </ymaps:ymaps>
    </xsl:template>

    <xsl:template match="event">
        <ymaps:GeoObject>
                <gml:id><xsl:value-of select="id"></xsl:value-of></gml:id>
                <gml:name><xsl:value-of select="title"></xsl:value-of></gml:name>
                <gml:description><xsl:value-of select="description"></xsl:value-of></gml:description>
                <gml:Point>
                    <gml:pos><xsl:apply-templates select="coords"/></gml:pos>
                </gml:Point>
                <gml:metaDataProperty>
                    <ymaps:AnyMetaData>
                        <address><xsl:value-of select="address"></xsl:value-of></address>
                        <date><xsl:value-of select="date"></xsl:value-of></date>
                        <description><xsl:value-of select="description"></xsl:value-of></description>
                    </ymaps:AnyMetaData>
                </gml:metaDataProperty>
        </ymaps:GeoObject>
    </xsl:template>

    <xsl:template match="coords">
        <xsl:value-of select="substring-after(.,',')"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="substring-before(.,',')"/>
    </xsl:template>

</xsl:stylesheet>
