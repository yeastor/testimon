define(['jquery','model/map-model','view/map-view','view/form-view','view/menu-view','utils','fosjsrouting','fosjsroutes'],
    function($,MapModel,MapView,FormView,MenuView,Utils){

    var SetCenter = function(map,form,menuContainer)
    {
        this._model = new MapModel(map);
        this._mapView = new MapView(map);
        this._formView = new FormView(form);
        this._menuView = new MenuView(menuContainer);
        this._verticalView = null;
        this.feventCallCoint = 0;
        this.events = $({});

        this.init();
        this._attachHandlers();
    }

    SetCenter.prototype = {
        constructor : SetCenter,
        _attachHandlers: function () {
            this._formView.events
                .on('searchaddress', $.proxy(this._onSearchAddress, this))
                .on('queryevents', $.proxy(this._onQueryEvents, this))
                .on('checkPoint', $.proxy(this._onCheckPoint, this));

            this._mapView.events
                .on("clusterready", $.proxy(this._onClusterReady, this))
                .on("pointready", $.proxy(this._onPointReady, this))


        },
        init: function() {
          this._mapView.bindBoundChange();
        },
        _onClusterReady: function(e){
            var geoObjectsInsideMap = e.geoObjects;
            if (geoObjectsInsideMap.getLength() == 0)
            {
               this.showFEvent();
            }
            else {
                //this._menuView.setMenuByMapWisibleObjects(geoObjectsInsideMap);
            }
            this._menuView.setMenuByMapWisibleObjects(geoObjectsInsideMap);
        },
        showFEvent: function(){
            if (this.feventCallCoint == 0 && this._formView.getAddressObj().val().length > 3){
                this.feventCallCoint++;
                //currentCenterCoords = this._model.getCurrentMapCoords();
                currentCenterCoords = this._formView.getAddressCenter();

                var promise =  this._model.search(currentCenterCoords,{kind: 'street'});
                promise.then(
                    $.proxy(this._onSearchKindSuccess, this),
                    $.proxy(this._onSearchError, this)
                )

            }

        },
        _onSearchKindSuccess: function(result){

            var dateObj = this._formView.getDateObj(),
            eventBy = this._formView.getEventByValue();

            this._queyEvents(dateObj.val(),eventBy,{fevent: 1,coords: this._model.getCoords().join(), name: this._model.getName()});

            this._menuView.setMenuByMapWisibleObjects(geoObjectsInsideMap);

        },
        _onQueryKindSuccess: function(){
            if (this._model.getQueryResult()) {

            }
        },
        _onQueryEvents: function(e){

            this._mapView.loaderOn();
            this._menuView.showEventMenuContainer();
            this._menuView.showAddMenuButton();
            this._menuView.clearErrors();

            this._queyEvents(e.date,e.eventsBy);

        },
        _queyEvents: function(date,eventsBy,options){
            var fevent = 0,
            washere = Utils.getCookie('firstime');

            if (options === undefined) options = {};

            $xmlUrl = 'site_band_witness_map_index';
            if (eventsBy === undefined) eventsBy = 'week';
            if (eventsBy == 'week') $xmlUrl = 'site_band_witness_map_week';

            if (options.fevent === undefined || washere === undefined) fevent = 0;
            else fevent = options.fevent;


            var promise = this._model.xml(Routing.generate($xmlUrl,{ date: date, rand: new Date().getMilliseconds(), fevent: fevent, coords: options.coords, name: options.name, hash: washere}));
            this._mapView.clearClusterer();

            promise.then(
                $.proxy(this._onXmlSuccess, this),
                $.proxy(this._onXmlError, this)
            ).done($.proxy(this._onQueryDone, this));
        },

        _onSearchAddress: function(e){
            this._mapView.clearPoint();
            var promise = this._model.search(e.address);

            promise.then(
                $.proxy(this._onSearchSuccess, this),
                $.proxy(this._onSearchError, this)
            )

        },
        _onPointReady: function(e) {
            var promise = this._model.search(e.coords);

            promise.then(
                $.proxy(this._onPointSearchSuccess, this),
                $.proxy(this._onSearchError, this)
            )
        },
        _onPointSearchSuccess: function(result) {
            this._mapView.updatePoint(result);
            this._formView.setCityObjValByText(this._mapView.getObjectText(result));
            this._formView.setAddressObjVal(this._mapView.getObjectName(result));
        },
        setLocation : function(){
            var promise = this._model.getCurrentLocation();

            promise.then(
                $.proxy(this._onLocationSuccess, this),
                $.proxy(this._onLocationFailed, this) // try to get location for Safari
            );

        },
        addInit: function(){
            this._formView.attachFormHandlers();
            this._mapView.loaderOn();
            if (this._formView.getCoordsObject().val() == "") {
                this.setLocation();
            }
            else {
                this._setCenterByCoords();
            }

            this._menuView.showEventMenuContainer();
            this._menuView.showAddMenuButton();
            this._menuView.clearErrors();
            this._queyEvents(this._formView.getDateObj().val());
            this._mapView.bindMapClick();
        },
        _setCenterByCoords: function() {
          var zoom = (this._formView.getAddressObj().val() == "") ? 9 : 15;
           this._mapView.setCenter(this._formView.getCoordsObject().val().split(','),zoom);
           Utils.kladrOn(this._formView.getForm());
        },
        _onXmlSuccess: function(res){
            var promise =  this._model.query(this._model.getXmlResults());

            promise.then(
                //function(){ $this._mapView.renderClusterer(promise) },
                $.proxy(this._onQuerySuccess, this),
                $.proxy(this._onSearchError, this)
            )
        },
        _onQueryDone: function(){

          this._mapView.loaderOff();
        },
        _onXmlError: function(res){
            this._showNoObjectsAlert();
        },
        _onQuerySuccess: function(){
                if (this._model.getQueryResult()) {
                this._mapView
                    .renderClusterer(this._model.getQueryResult());
                }
                else{
                    this._showNoObjectsAlert();
                }

        },
        _onSearchSuccess: function (result) {
            var geoObject = this._model.getGeoObject();
            if(geoObject) {
                this._mapView
                    .setCenterByAddress(result);
                this._formView.setAddressCenter(this._mapView.getObjectCoordinate(this._mapView.getGeoObjects(result)));
            }
            else {
               // this._formView
                 //   .showMessage("Ничего не найдено.");
            }
        },
        _onSearchError: function (e) {

        },
        _onLocationSuccess: function (result) {
            var geoObject = this._model.getGeoObject();
            if(geoObject) {
                this._mapView
                    .setLocation(result);
                var addressList = this._model.getAddressList();
                var city = addressList[1];
                this._formView.setCity(city);
                Utils.kladrOn(this._formView.getForm());

            }
            else {
              //  this._formView
              //      .showMessage("Ничего не найдено.");
            }
        },
        _onLocationFailed: function (e) {

        },
        _showNoObjectsAlert: function() {
            this.showFEvent();
            this._menuView.showAddMenuButton("Добавить первым");
            this._menuView.setMenuIfNoObjects("На карте объекты не найдены.");
        },
        getFormView: function(){
            return this._formView;
        },
        _onCheckPoint: function() {
            var geoObjects = this._model.queryNoPromise(this._mapView.getPoint());
            var objectsInMap = this._mapView.getObjectsInsideMap(geoObjects);
            if (objectsInMap != 1) {
                this._formView.setAddressCenter("");
                this._formView.showCheckPointError("<strong>Это просто!</strong> Укажите точку на карте так, чтобы её было видно.");

            } else {
                this._formView.setAddressCenter(this._mapView.getObjectCoordinate(geoObjects));
                this.events.trigger($.Event('success'),{ name: 'GoodPointOnMap'  });
                this._verticalView.gotoStep("#step-4");
                this._verticalView.hideShadow($("#step-3"));
                this._formView.showCheckPointSuccess("<strong>Отлично!</strong> Точка установлена верно.");

            }
        },
        setVerticalView : function(verticalView){
          this._verticalView = verticalView;
        }
    }

    return SetCenter;


})
