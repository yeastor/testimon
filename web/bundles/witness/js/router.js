/**
 * Created by kupriyanovym on 05.05.14.
 */
// Filename: router.js
define([
    'jquery',
    'underscore',
    'backbone',
    'event/test',
    'event/intest'
], function($, _, Backbone, Test, InTest){
    var AppRouter = Backbone.Router.extend({
        routes: {
            // Define some URL routes
            '/event-test/;': 'test',
            '/event-intest': 'intest',

            // Default
            '*actions': 'defaultAction'
        },

        current : function() {
            var Router = this,
                fragment = Backbone.history.fragment,
                routes = _.pairs(Router.routes),
                route = null, params = null, matched;

            matched = _.find(routes, function(handler) {
                route = _.isRegExp(handler[0]) ? handler[0] : Router._routeToRegExp(handler[0]);
                return route.test(fragment);
            });

            if(matched) {
                // NEW: Extracts the params using the internal
                // function _extractParameters
                params = Router._extractParameters(route, fragment);
                route = matched[1];
            }

            return {
                route : route,
                fragment : fragment,
                params : params
            };
        }
    });

    var initialize = function(){
        var app_router = new AppRouter;
       // alert( this.routes[Backbone.history.fragment] );
        app_router.on('route:test', function(){
            // Call render on the module we loaded in via the dependency array
            alert("test");
        });
        // As above, call render on our loaded module
        // 'views/users/list'
        app_router.on('route:intest', function(){
            alert("intest");
        });
        app_router.on('route:defaultAction', function(actions){
            // We have no matching route, lets just log what the URL was
            alert('No route:', actions);
        });
        Backbone.history.start({pushState: true});
    };
    return {
        initialize: initialize
    };
});