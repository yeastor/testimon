/**
 * Created by kupriyanovym on 16.01.14.
 */
var mapHelper = {
    clusterer :  0,
    maxObjectCountInMenu : 10,
    'mapInit' : function()
    {
        var myMap = new ymaps.Map('map', {
            center: [55, 34],
            zoom: 9,
            behaviors: ['default', 'scrollZoom'],
            controls: ['smallMapDefaultSet']
        });
       // myPlacemark



        return  myMap
    },

    setCity : function(map,cityObj,addressObj,setCoords) {

        ymaps.geolocation.get({ autoReverseGeocode: true }).then(
            function(res){
                var geoObject = res.geoObjects.get(0);

                var re = /\s*,\s*/
                var addressList = geoObject.properties.get('description').split(re);
                var coords = geoObject.geometry.getCoordinates();

                if (addressList[1])
                {
                    var city = addressList[1];
                    cityObj.val(city);
                    setCoords = typeof setCoords !== 'undefined' ?  setCoords(coords) : null;

                }

                map.setCenter(geoObject.properties.get('boundedBy')[0],8, {
                    checkZoomRange: true,
                    duration: 1
                });

                jsHelper.kladrOn(cityObj,addressObj);

            });

    },

    getAddress : function(placemark,options){

        placemark.properties.set('iconContent', 'поиск...');
        var resGeoObject = ymaps.geocode(placemark.geometry.getCoordinates()).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);

            placemark.properties
                .set({
                    iconContent: firstGeoObject.properties.get('name'),
                    balloonContent: firstGeoObject.properties.get('text')
                })
            if (options.setAddressToFormElement)
            {
                var formElementAddress = options.formElementAddress;
                var formElementCity = options.formElementCity;

                if (formElementCity)
                {
                    var re = /\s*,\s*/;
                    var addressList = firstGeoObject.properties.get('text').split(re);
                    if (addressList[1])
                    {
                        var city = addressList[1];
                        formElementCity.val(city);
                        if (options.triggerKladrCity)
                        {
                            formElementCity.change();
                        }

                    }
                }
                if (formElementAddress)
                {
                    formElementAddress.val(firstGeoObject.properties.get('name'));
                }

            };


        });


    },

    createPlacemark : function(coords) {
        return new ymaps.Placemark(coords, {
        iconContent: 'поиск...'
        }, {
            preset: 'islands#violetStretchyIcon',
            draggable: true
        });
    },

    setDragedPlacemark : function(placemark,cityObj,addressObj){
        placemark.events.add('dragend', function () {
            currentAddress = this.getAddress(myPlacemark,{
                setAddressToFormElement : true,
                formElementAddress: addressObj,
                formElementCity: cityObj,
                triggerKladrCity : true});
        });
    },

    setCenterByAddress : function(map,address,options)
    {

    ymaps.geocode(address, {
        results: 1 // Если нужен только один результат, экономим трафик пользователей
    }).then(function (res) {
            // Выбираем первый результат геокодирования.
            var firstGeoObject = res.geoObjects.get(0),
            coords = firstGeoObject.geometry.getCoordinates(), // Координаты геообъекта.
            bounds = firstGeoObject.properties.get('boundedBy'); // Область видимости геообъекта.

            //ищем объекты рядом
            if (options.setCoordsToFormElement)
            {
                var formElement = options.formElement;
                if (formElement)
                {
                    formElement.val(coords);
                }
            };

            map.setBounds(bounds, {
                checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
            });
            /*map.setCenter(bounds[0],13, {
                checkZoomRange: true,
                duration: 1
            });*/
        });

    },

    setCenter : function setCenter (map,coords,zoom) {
        map.setCenter(coords,zoom);
    },

    showEventsOnMap : function (map,clusterer,date, fn)
    {
    var $this = this;


        $this.loaderOn();
    if (clusterer)
    {
        clusterer.removeAll();
    };

    //myMap.geoObjects.remove(myCollection);
    result = ymaps.geoQuery(ymaps.geoXml.load(Routing.generate('site_band_witness_map_index',{ date: date, rand: new Date().getMilliseconds()})  ));


    result.then(function() {

         $this.setMenuByMapWisibleObjects(map,result);

         var newClusterer = result.clusterize();
         newClusterer.options.set({
            preset: 'islands#invertedVioletClusterIcons',
            /**
             * Ставим true, если хотим кластеризовать только точки с одинаковыми координатами.
             */
            groupByCoordinates: false

        });
        map.geoObjects.add(newClusterer); // Добавление геообъектов на карту
        fn(newClusterer,result);
         // Найдем объекты, попадающие в видимую область карты.

         $this.loaderOff();
    }, function (error){   // Вызывается в случае неудачной загрузки YMapsML
         $this.loaderOff();
        if (error)
            console.log('Ошибка: ' + error);

    });




},
    setMenuByMapWisibleObjects : function(map,objects)
{
    var i = 1;
    var menu = $('<div class="ev-menu-inner"></div>');
    var maxObjectCountInMenu = this.maxObjectCountInMenu;
    var objectsInsideMap = objects.searchInside(map);
    objectsInsideMap.each(function(point){
        if (i > maxObjectCountInMenu)
        {var menuItem = $('<div id="event-max">Показаны первые '+maxObjectCountInMenu+' событий</div>');  menuItem.appendTo(menu); return;}
        var menuItem = mapHelper.createMenu(point);
        menuItem.appendTo(menu);

        // open balloon by click
        menuItem.find('a').click(function () {
            point.balloon.open();
        });

        point.events.add(['balloonopen','balloonclose'], function (e) {
            if (e.get('type') == 'balloonopen')
                mapHelper.showEventDetails(mapHelper.createMenu(point));
            else mapHelper.showEventDetails('');
        });

        i++;
    });
   //alert (objectsInsideMap.length());

    $(".ev-menu").html(menu);
},
    setMenuIfNoObjects: function(){
        var noObjectHtml = $('<div class="alert alert-warning">На карте объекты пока не найдены.</div>');
        $(".ev-menu").html(noObjectHtml);
    },
    showEventMenuContainer : function(){
        $('#ev-menu-container').removeClass('hidden');
    },
    createMenu : function (point)
    {
        var eventId = point.properties.get('id');
    var menuItem = $('<div id="event-'+eventId+'">'+point.properties.get('metaDataProperty.AnyMetaData.date')+' <a class="pointMenu" href="'+Routing.generate('site_band_witness_event_get',{ id: eventId})+'">' + point.properties.get('name') + '</a> <br />' +
        point.properties.get('metaDataProperty.AnyMetaData.address')+'</div>');




    var description = $('<p id="event-description-'+point.properties.get('id')+'"></p>');
    var descriptionHtml = $('<p class="event-description">'+point.properties.get('description')+'</p>');

    var showHideDescriptionButton = $('<button class="btn btn-sm btn-default" type="button">Показать описание</button>').click(
        function(){
            descriptionHtml.toggle('slow');
        }
    )
    descriptionHtml.appendTo(description);
    description.appendTo(menuItem);
    showHideDescriptionButton.appendTo(menuItem);

    return menuItem;
    },
    setMenuByMapWisibleObjects : function(map,objects)
    {
    var i = 1,
    menu = $('<div class="ev-menu-inner"></div>'),
    maxObjectCountInMenu = this.maxObjectCountInMenu;
    objects.searchInside(map).each(function(point){

        if (i > maxObjectCountInMenu)
        {var menuItem = $('<div id="event-max">Показаны первые '+maxObjectCountInMenu+' событий</div>');  menuItem.appendTo(menu); return;}
        var menuItem = mapHelper.createMenu(point);
        menuItem.appendTo(menu);

        // open balloon by click
        menuItem.find('a').click(function () {
            point.balloon.open();
        });

        point.events.add(['balloonopen','balloonclose'], function (e) {
            if (e.get('type') == 'balloonopen')
                mapHelper.showEventDetails(mapHelper.createMenu(point));
            else mapHelper.showEventDetails('');
        });

        i++;
    });

    $(".ev-menu").html(menu);
    },
    showEventDetails : function(html)
    {
        $('#event-detail').html(html);
    },

    loaderOn : function loaderOn()
    {
        $(".loader").show();
    },
    loaderOff: function ()
    {
        $(".loader").hide();
    }


}




function init() {
    var myPlacemark,
        maxObjectCountInMenu = 15,

        myMap = new ymaps.Map('map', {
            center: [57.751574, 38.573856],
            zoom: 9,
            behaviors: ['default', 'scrollZoom'],
            controls: ['smallMapDefaultSet']
        }),

    clusterer = 0,
    myCollection = 0;
    var result = 0;



    ymaps.geolocation.get({ autoReverseGeocode: true }).then(
    function(res){
        var geoObject = res.geoObjects.get(0);

        var re = /\s*,\s*/
        var addressList = geoObject.properties.get('description').split(re);
        if (addressList[1])
        {
            var city = addressList[1];
            $("#city").val(city);
        }

        myMap.setCenter(geoObject.properties.get('boundedBy')[0],8, {
            checkZoomRange: true,
            duration: 1
        });
        jsHelper.kladrOn($('#city'),$('#address'));
        var date = $('#date').val();
        showEventsOnMap(date);

    });

    myMap.events.add('boundschange', function () {
        if (result)
        {
            setMenuByMapWisibleObjects(result);
        }
    });


    // Слушаем клик на карте
   /* myMap.events.add('click', function (e) {
        var coords = e.get('coords');

        // Если метка уже создана – просто передвигаем ее
        if(myPlacemark) {
            myPlacemark.geometry.setCoordinates(coords);
        }
        // Если нет – создаем.
        else {
            myPlacemark = createPlacemark(coords);
            myMap.geoObjects.add(myPlacemark);
            // Слушаем событие окончания перетаскивания на метке.
            myPlacemark.events.add('dragend', function () {
                getAddress(myPlacemark.geometry.getCoordinates());
            });
        }

        $('#coordinates').val(coords);
        getAddress(coords);
    });*/


    $( "#eventForm" ).submit(function( event ) {
        //alert(event.currentTarget);
        if (event.relatedTarget.id == "searchEvent"){
            setCenterByAddress(createAddress($('#country').val(),$('#city').val(),$('#address').val()));
            var date = $('#date',this).val();
            showEventsOnMap(date);
            showAddEventButton();
            event.preventDefault();
        }
    });

    function showEventDetails(html)
    {
        $('#event-detail').html(html);
    }

    /**
     * создает div с отображением деталей события
     * @param point
     * @returns {*|jQuery|HTMLElement}
     */
    function createMenu(point)
    {
        var menuItem = $('<div id="event-'+point.properties.get('id')+'">'+point.properties.get('metaDataProperty.AnyMetaData.date')+' <a class="pointMenu" href="#">' + point.properties.get('name') + '</a> <br />' +
            point.properties.get('metaDataProperty.AnyMetaData.address')+'</div>');




        var description = $('<p id="event-description-'+point.properties.get('id')+'"></p>');
        var descriptionHtml = $('<p class="event-description">'+point.properties.get('description')+'</p>');

        var showHideDescriptionButton = $('<button class="btn btn-sm btn-default">Показать описание</button>').click(
            function(){
                descriptionHtml.toggle('slow');
            }
        )
        descriptionHtml.appendTo(description);
        description.appendTo(menuItem);
        showHideDescriptionButton.appendTo(menuItem);

        return menuItem;
    }

    /**
     * Формирует левое меню по объектам, которые видны на карте
     * @param objects
     */
    function setMenuByMapWisibleObjects(objects)
    {
        var i = 1;
        var menu = $('<div class="ev-menu-inner"></div>');
        objects.searchInside(myMap).each(function(point){

            if (i > maxObjectCountInMenu)
            {var menuItem = $('<div id="event-max">Показаны первые '+maxObjectCountInMenu+' событий</div>');  menuItem.appendTo(menu); return;}
            var menuItem = createMenu(point);
            menuItem.appendTo(menu);

            // open balloon by click
            menuItem.find('a').click(function () {
                point.balloon.open();
            });

            point.events.add(['balloonopen','balloonclose'], function (e) {
                if (e.get('type') == 'balloonopen')
                showEventDetails(createMenu(point));
                else showEventDetails('');
            });

            i++;
        });

        $(".ev-menu").html(menu);
    }
    // Создание метки
    function createPlacemark(coords) {
        return new ymaps.Placemark(coords, {
            iconContent: 'поиск...'
        }, {
            preset: 'islands#violetStretchyIcon',
            draggable: true
        });
    }

    // Определяем адрес по координатам (обратное геокодирование)
    function getAddress(coords) {
        myPlacemark.properties.set('iconContent', 'поиск...');
        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);

            myPlacemark.properties
                .set({
                    iconContent: firstGeoObject.properties.get('name'),
                    balloonContent: firstGeoObject.properties.get('text')
                })
        });
    }

    function createAddress(country,city,address)
    {
        return country+","+city+","+address;
    }

    function showAddEventButton()
    {
        var addEventButton = $('<input type="submit" value="Добавить событие"/>');
        addEventButton.addClass('btn btn-danger');
        $(".ev-actions").html(addEventButton);
    }

    function showEventsOnMap(date)
    {
        //if (myCollection) {
        //    myMap.geoObjects.remove(myCollection);
       // };
        loaderOn();
        if (clusterer)
        {
            clusterer.removeAll();
        };

        //myMap.geoObjects.remove(myCollection);
        result = ymaps.geoQuery(ymaps.geoXml.load(Routing.generate('site_band_witness_map_index',{ date: date, rand: new Date().getMilliseconds()})  ))

        result.then(function (res) {

            setMenuByMapWisibleObjects(result);

                clusterer = result.clusterize();
            clusterer.options.set({
                preset: 'islands#invertedVioletClusterIcons',
                /**
                 * Ставим true, если хотим кластеризовать только точки с одинаковыми координатами.
                 */
                groupByCoordinates: false

            });
                myMap.geoObjects.add(clusterer); // Добавление геообъектов на карту

            // Найдем объекты, попадающие в видимую область карты.

                    loaderOff();
            }, function (error){   // Вызывается в случае неудачной загрузки YMapsML
            loaderOff();
            if (error)
            alert('Ошибка: ' + error);

            });

    }


    function setCenterByAddress(address)
    {


        ymaps.geocode(address, {
            /**
             * Опции запроса
             * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/geocode.xml
             */
            // boundedBy: myMap.getBounds(), // Сортировка результатов от центра окна карты
            // strictBounds: true, // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy
            results: 1 // Если нужен только один результат, экономим трафик пользователей
        }).then(function (res) {
                // Выбираем первый результат геокодирования.
                var firstGeoObject = res.geoObjects.get(0),
                // Координаты геообъекта.
                    coords = firstGeoObject.geometry.getCoordinates(),
                // Область видимости геообъекта.
                    bounds = firstGeoObject.properties.get('boundedBy');

                // Добавляем первый найденный геообъект на карту.
                //myMap.geoObjects.add(firstGeoObject);
                // Масштабируем карту на область видимости геообъекта.

                //ищем объекты рядом
                $('#addressCenter').val(coords);

                //передаем дату

                //getCurrentEvents(coords,date);
                //.then(onGeoXmlLoad);
               // e.target.disabled = true;


                myMap.setBounds(bounds, {
                    checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                });


            });

    }






    function onGeoXmlLoad (res) {

        myMap.geoObjects.add(res.geoObjects);
        if (res.mapState) {
            res.mapState.applyToMap(myMap);
        }
    }

    function getCurrentEvents(coords,date)
    {

        $.ajax({
            type: "POST",
            url: Routing.generate('site_band_witness_events_current_json'),
            //contentType: 'application/json; charset=UTF-8',
            data: {date : date},
            success: function(data, dataType)
            {
                $('#log').html(data);
            },

            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                alert('Error : ' + errorThrown);
            }
        });

    }

    function loaderOn()
    {
        $(".loader").show();
    }
    function loaderOff()
    {
        $(".loader").hide();
    }
}


