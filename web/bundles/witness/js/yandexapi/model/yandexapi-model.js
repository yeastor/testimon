/**
 * Created by kupriyanovym on 14.08.2014.
 */
/**
 * Created by kupriyanovym on 14.08.2014.
 */
/**
 * Created by kupriyanovym on 13.08.2014.
 */
define(['jquery'], function($){

    var YModel = function(path){
        this._path = path;
        this._tmp_folder = 'tmp';
        this._remove_path = null;
        this._real_folder = 'events';

    }

    YModel.prototype = {
        constructor: YModel,
        setTmpFolder: function(url){
            this._tmp_folder = url;
        },
        getTmpFolder: function(){
            return this._tmp_folder;
        },
        getImageControllerPath: function(){
            return this._path;
        },
        getImagePath: function(){
            //temp
          return this._path+'/'+this._tmp_folder+'/';
        },
        setRemovePath: function(path){
            this._remove_path = path
        },
        getRemovePath: function(){
          return this._remove_path;
        },
        getPath: function(){
            return this._path+'/';
        },
        getRealPath: function(){
            return this._path+'/'+this._real_folder+'/';
        }
    }

    return YModel;

})
