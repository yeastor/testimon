/**
 * Created by kupriyanovym on 13.08.2014.
 */
define(['jquery','security/security','form/form-controller','yandexapi/view/yandexapi-form-view','yandexapi/model/yandexapi-model','','fosjsrouting','fosjsroutes'], function($,Security,FormController,YFormView,YModel){

    var Yapi = function(form_container,img_add_btn){
        this._form_container = form_container;
        this._form_controller = new FormController(this._form_container);
        this._file_form_view = this._form_controller.getView();
        this._img_add_btn = $(img_add_btn);
        this._model = new YModel(Routing.generate('site_band_yandex_api_images'));
        this._yform_view = new YFormView(this._form_container,this._img_add_btn);
        this.events = $({});
        this._attacheHandlers();
    }

    Yapi.prototype = {
        constructor: Yapi,
        _attacheHandlers: function(){
            this._file_form_view.onFileFromSubmited(null,$.proxy(this._onFormSuccess,this), $.proxy(this._onFormError,this));
            this._yform_view.events.on('deleteclick', $.proxy(this._onDeleteImage,this));
        },
        _onFormSuccess: function(data){
            var img_url,
                img_tag,
                path = "",
                tmp_path = "";
            if (data.url != undefined)
            {
                this._yform_view.clearInputs();
                img_url = data.url;
                this._file_form_view.showSuccessMessage(data.message);
            }
            else{
                img_url = data.new_file;
                this._file_form_view.showSuccessMessage(data.message);
                this._yform_view.clearInputs();
                tmp_path = this._model.getImagePath();
                path = this._model.getRealPath();
            }
            img_tag = this._yform_view.createImgTag(path+img_url);
            this._yform_view.appendImage(tmp_path,img_url);
            this.events.trigger($.Event('onuploadsuccess',{ img_tag: img_tag  }));
            return false;
        },
        _onFormError: function(data){
            this._yform_view.clearInputs();
            this._file_form_view.showErrorMessage("Произошла неизвестная ошибка на сервере.");
        },
        _onDeleteImage: function(params){
            if (params.url === undefined) return false;
            var img_tag, path;
            var patt = /^http:\/\//i;
            this.events.trigger($.Event('onremoveimagestart'));
            if (patt.test(params.url)){ // url image
                img_tag = this._yform_view.createImgTag(params.url);
                //this._yform_view.removeImageFromForm(params.url);
                this.events.trigger($.Event('onremoveimagesuccess',{ img_tag: img_tag  }));
            }
            else{ // image from file
                this._model.setRemovePath(Routing.generate('site_band_yandex_api_remove'));
                this._yform_view.deleteImage(this._model.getRemovePath(),params.url,$.proxy(this._removeSuccess,this), $.proxy(this._removeError,this));

            };

        },
        _removeSuccess: function(data,img_url){
            var path,img_tag

            //this._yform_view.removeImageFromForm(img_url);

            path = this._model.getRealPath();
            img_tag = this._yform_view.createImgTag(path+img_url);
            this.events.trigger($.Event('onremoveimagesuccess',{ img_tag: img_tag  })); // renove success

        },
        _removeError: function(){
            alert("removeError");
        }
    }

    return Yapi;

})
