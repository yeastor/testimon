/**
 * Created by kupriyanovym on 14.08.2014.
 */
/**
 * Created by kupriyanovym on 13.08.2014.
 */
define(['jquery'], function($){

    var YFormFiew = function(formContainer,add_img_btn){
        this._form_container = $(formContainer);
        this._img_btn = add_img_btn;

        this.events = $({});

        this._attacheHandlers();
    }

    YFormFiew.prototype = {
        constructor: YFormFiew,
        _attacheHandlers: function(){
           this._img_btn.on('click', $.proxy(this._onImgBtnClick,this));
        },
        _onImgBtnClick: function(){
            this._form_container.toggleClass('hidden');
        },
        clearInputs: function(){
            this._form_container.find(":file").each(function(i){  $(this).val("");})
            this._form_container.find(":text").each(function(i){  $(this).val("");})
        },
        appendImage: function(path,img_url) {
            if (img_url === undefined) return false;
            var container = $("<div class='img-container'></div>");
            var delete_bnt = $("<button class='btn btn-danger' title='Удалить изображение' data-href='"+img_url+"'><span class='glyphicon glyphicon-remove black'></span></button>")
                .on('click', $.proxy(this._onDeleteClick,this,img_url));

            var img = $('<img src="'+path+img_url+'" />');
            container.append(img);
            container.append(delete_bnt);
            this._form_container.append(container);
        },
        _onDeleteClick: function(img_url){
            this.removeImageFromForm(img_url);
            this.events.trigger($.Event('deleteclick',{
                url: img_url}))

        },
        deleteImage: function(call_url,img_url,successFn,FailFn)
        {
            var url = call_url+'/'+img_url;
            $.get(url)
                .done(function(data){
                    if(data.success == false) {
                        FailFn(data);

                    }
                    else {
                        successFn(data,img_url);
                    }


                })
                .fail(function() {
                    alert( "Произошла ошибка на сервере" );
                })
        },
        removeImageFromForm: function(data_href){
            if (data_href === undefined) return false;
            imgContainer = this._form_container.find($("button[data-href='"+data_href+"']")).parent();
            imgContainer.remove();
        },
        createImgTag: function(img_url){
            return '<img src="'+img_url+'" ></img>';
        }


    }

    return YFormFiew;

})
