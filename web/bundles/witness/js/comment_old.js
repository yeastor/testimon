define(['jquery','security/security'],function($,Security){

        var Comments = function(options)
        {
                this._url =  typeof options.url !== 'undefined' ?  options.url : null;
                this._showLoginForm = typeof options.show_login_form !== 'undefined' ?  options.show_login_form : null;
        }

    return {

        init: function(options){

           var $this = this,
           url = typeof options.url !== 'undefined' ?  options.url : null,
           show_login_form  = typeof options.show_login_form !== 'undefined' ?  options.show_login_form : null;

            $('#addComment').click(function(){
                if (show_login_form){

                    $this.onShowLoginForm();
                    $(window).on('loginSuccess', function(e){
                        $this.showCommentAddForm();
                    });

                }
                else
                {
                    $this.showCommentAddForm();
                }
            });

            Security.onFromSubmited($('#commentFormContainer'),url,function(){window.location.reload(); },function(){
               $("img[title='captcha']").attr('src','/generate-captcha/gcb_captcha?n='+(new Date()).getTime()); //reloadC
           });
        },

        onShowLoginForm : function(){
            $(window).trigger('showLoginForm');
        },

        showCommentAddForm : function(){ $('#commentFormContainer').removeClass('hidden')},


        submit: function(form){



        }




    }
}
);
