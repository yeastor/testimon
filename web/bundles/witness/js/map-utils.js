define(['ymaps'],function(){

    var mapInit, loaderOn, loaderOff;
    mapInit = function (controls) {
        var myMap = new ymaps.Map('map', {
            center: [55.76, 37.64],
            zoom: 9,
            behaviors: ['default', 'scrollZoom'],
            controls: controls
        });
        return  myMap
    };
    loaderOn = function loaderOn() {
        $(".loader").show();
    };
    loaderOff = function () {
        $(".loader").hide();
    };

    return {mapInit : mapInit, loaderOn: loaderOn, loaderOff: loaderOff }

})