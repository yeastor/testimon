/**
 * Created by kupriyanovym on 04.02.14.
 */
(function($){

    $.fn.createDatePicker = function(setNowDate) {


        /** datepicker **/
        var obj = $(this);
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        return $(this).datepicker({weekStart : 1,onRender: function(date) {

            if (setNowDate)  obj.val(('0'+now.getDate()).slice(-2)+"/"+	('0'+(now.getMonth()+1)).slice(-2)+"/"+ now.getFullYear()  );
            return date.valueOf() > now.valueOf() ? 'disabled' : '';
        }
        }).on('changeDate', function(ev){
                $(this).datepicker('hide');
            });

    },

     $.fn.submitForm  =  function submitForm( url, successFn)
        {
            var $this = this;
            form = $("form",this);

            form.submit(function(e){e.preventDefault();
                alert(e.currentTarget.serialize());
                $.ajax({
                    type        : form.attr( 'method' ),
                    url         : url,
                    data        : form.serialize(),
                    dataType    : "json",
                    success     : function(data, status, object) {

                        if(data.success == false) {
                            //$('.alert',this).removeClass('hidden').html(data.message);

                            errors = $('<ul></ul>');

                            $.each(data.message,function(i,val){

                                errors.append("<li>"+data.message[i]+"</li>");

                            })
                            $('.alert',$this).removeClass("hidden").html(errors);

                        }
                        else {  successFn(data.message); }
                    },
                    error: function(data, status, object){

                        console.log(data.message);
                    }
                });
            });
        },



    $.fn.createAutoComplete = function(route) {
     var obj = $(this);
        return $.ajax({
        url: Routing.generate(route),
        dataType: "json",
        success: function (data) {
            json = data;

            obj.autocomplete({
                source: json,
                minLength: 2
            });
        },
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
            console.log('Error : ' + errorThrown);
        }
    });

    }


})(jQuery);

var jsHelper = {
    createAddress :function (country,city,address)
    {
        return country+","+city+","+address;
    },

    kladrOn : function(cityObj,addressObj)
    {

                var city = cityObj;
                var street = addressObj;
                // Подключение автодополнени// Автодополнение населённых пунктов

        cityObj.kladr({
            token: '52f3362d31608f796e000002',
            key: 'fec91b1ef87f3037ae274e94dde3ffbf7612d3dd',
            type: $.kladr.type.city,
            verify: true,
            check: function( obj ) {

                if (obj){
                // Изменения родительского объекта для автодополнения улиц
                addressObj.kladr('parentId', obj.id);
                addressObj.kladr('parentType',$.kladr.type.city);
                }
                else
                {
                    addressObj.kladr('parentType',false);
                    //cityObj.css('color','red');
                }
            }
            });

            // Автодополнение улиц

            addressObj.kladr({
            token: '52f3362d31608f796e000002',
            key: 'fec91b1ef87f3037ae274e94dde3ffbf7612d3dd',
            type: $.kladr.type.street,
            parentType: $.kladr.type.city,
            valueFormat: function(obj, query) {

                    var label = '';
                    if(obj.typeShort){
                        label += obj.typeShort + '. ';
                    }
                    label += obj.name;
                    return label;
            }

            });

        cityObj.change();

    }

}
