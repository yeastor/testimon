/**
 * Created by kupriyanovym on 14.08.2014.
 */
define(['jquery','bootstrap','libs/jquery.iframe-transport'],
    function($){

        var FileFormView = function(formContainer){
            this._form_container = formContainer;
            this._options = null;
            this.events = $({});
        }

        FileFormView.prototype = {
            constructor: FileFormView,
            onFileFromSubmited : function(url, successFn, failFn){

                form = $("form",this._form_container);
                formContainer = this._form_container;
                if (!url) url = form.prop('action');
                form.submit(function(e){e.preventDefault();
                    form = $(e.target);
                    form.addClass('loading');
                    $.ajax({
                        type: form.attr('method'),
                        url: url,
                        data: form.serializeArray(),
                        dataType: "json",
                        files: form.find(":file"),
                        iframe: true,
                        processData: false
                    })
                        .always( function() { form.removeClass('loading');})
                        .done(function(data, status, object) {

                            if(data.success == false) {

                                errors = $('<ul></ul>');

                                $.each(data.message,function(i,val){

                                    errors.append("<li>"+data.message[i]+"</li>");

                                })
                                $('.alert',formContainer).removeClass("alert-success");
                                $('.alert',formContainer).addClass("alert-danger");
                                $('.alert',formContainer).removeClass("hidden").html(errors);
                                //if (typeof failFn !== 'undefined'){
                                //    failFn();
                               // }

                            }
                            else {  successFn(data);  }
                        })
                        .fail(function(data, status, object){
                            if (typeof failFn !== 'undefined'){
                                failFn(data);
                            }
                        })
                });
            },
            showSuccessMessage: function(message){
                this.clearAlert();
                var alert = $('.alert',this._form_container);
                alert.removeClass("alert-danger");
                alert.addClass("alert-success");
                alert.html("<p>"+message+"</p>");
                alert.removeClass("hidden");
            },
            clearAlert: function(){
                    $('.alert',this._form_container).html("");
            },
            showErrorMessage: function(message) {
                this.clearAlert();
                var alert = $('.alert', this._form_container);
                alert.removeClass("alert-success");
                alert.addClass("alert-danger");
                alert.html("<p>" + message + "</p>");
                alert.removeClass("hidden");
            }
        }

        return FileFormView;
    })
