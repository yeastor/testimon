/**
 * Created by kupriyanovym on 14.08.2014.
 */
define(['jquery','bootstrap','form/view/file-form-view'],

    function($,Bootstrap,FileFormView){

        var FormController = function(formContainer){

            this._view = new FileFormView(formContainer);
        }

        FormController.prototype = {
            constructor: FormController,
            getView: function(){
                return this._view;
            }
        }

        return FormController;


    })