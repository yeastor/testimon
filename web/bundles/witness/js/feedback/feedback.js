/**
 * Created by kupriyanovym on 25.07.2014.
 * feedaback controller
 */
/**
 * Created by kupriyanovym on 25.07.2014.
 */
define(['jquery','security/security','view/feedback-link-view','view/feedback-form-view','libs/browser/browser'],function($,Security,FeedBackLinkView,FeedBackFormView,Browser){

    var FeedBack = function(fb_link,form_container,form_url){
        this._fb_link = fb_link;
        this._form_container = form_container;
        this._fb_link_view = new FeedBackLinkView(this._fb_link);
        this._form_url = form_url;
        this._fb_form_view = new FeedBackFormView(this._form_container,this._form_url);
        this._security = new Security();
        this._attachHandlers();
    }

    FeedBack.prototype = {
        constructor: FeedBack,
        _attachHandlers: function(){
            this._fb_link_view.events
                .on('fblinkclick', $.proxy(this._onFbClick,this))

            this._fb_form_view.events
                .on('formloaded', $.proxy(this._onFormLoaded,this))
        },
        _onFbClick: function(options){
                this._fb_form_view.toggle();
        },
        /**
         *
         * @private
         * when form loaded allow it to ajax
         */
        _onFormLoaded: function(){
            this._security.getSecurityView().onFromSubmited(this._form_container,this._form_url, $.proxy(this._onFormSuccess,this), $.proxy(this._onFormError,this));

            var browser = new Browser();
            this._fb_form_view.setUserInfo(browser.getInfo());
        },
        _onFormSuccess: function(){
            this._fb_form_view.showSuccessMessage();
        },
        _onFormError: function(){
           // $("img[title='captcha']").attr('src','/generate-captcha/gcb_captcha?n='+(new Date()).getTime());
            this._fb_form_view.reloadCaptcha();
        }
    }

    return FeedBack;

})