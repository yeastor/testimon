define([
    'jquery',
    'bootstrap',
    'security/security',
    'feedback/feedback',
    'fosjsrouting',
    'fosjsroutes'

], function($,Bootstrap,Security,Feedback){
    var App = function(options){
        // Pass in our Router module and call it's initialize functions edit and go to dev
        this._security = new Security();
        this._securityView = this._security.getSecurityView();
        this._security.init(Routing.generate('fos_user_security_check'),Routing.generate('fos_user_registration_register'),{backdrop : true});
        this._loginLink = $('.login-link');

        this._feedback = new Feedback($('#feedback-link'),$('#feedback-container'),Routing.generate('site_band_feedback_add'));
        this._attachHandlers();

    }

    App.prototype = {
        constructor: App,
        _attachHandlers: function(){
            this._loginLink
                .on('click',$.proxy(this._showLoginMainForm,this))
        },
        _attachFormHandlers: function(){
            this._securityView.events
                .on('loginsuccess', $.proxy(this._onLoginSuccess,this))
                .on('hidemodal', $.proxy(this._onHideLoginForm,this))

        },
        /**
         *
         * @private
         * detach handlers
         */
        _detachFormHandlers: function () {
            this._securityView.events
                .off("loginsuccess");
        },
        _onHideLoginForm: function(){
               this._detachFormHandlers();
        },
        _showLoginMainForm: function(e){
            this._security.showLoginFormByUser({show_login_form: options.app_user});
            this._attachFormHandlers();
            e.preventDefault();
        },
        _onLoginSuccess: function(){
            window.location.reload();
        }


    }

    return App;

});