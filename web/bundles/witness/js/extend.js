define(['jquery'],function($){

    $this = this;

    $.fn.extend({

        createDatePicker : function(setNowDate) {
            /** datepicker **/
            var obj = $(this);
            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

            return $(this).datepicker({weekStart : 1,onRender: function(date) {

                if (setNowDate)  obj.val(('0'+now.getDate()).slice(-2)+"/"+	('0'+(now.getMonth()+1)).slice(-2)+"/"+ now.getFullYear()  );
                return date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
            }).on('changeDate', function(ev){
                $(this).datepicker('hide');
            });

        },

        createAutoComplete: function(url) {
        var obj = $(this);
        return $.ajax({
            url: url,
            dataType: "json",
            success: function (data) {
                json = data;

                obj.autocomplete({
                    source: json,
                    minLength: 2
                });
            },
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                console.log('Error : ' + errorThrown);
            }
        });

    }
    })
    return{

    }
})
