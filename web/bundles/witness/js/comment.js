define(['jquery','security/security'],function($,Security){

        var Comments = function(options)
        {
                this._url =  typeof options.url !== 'undefined' ?  options.url : null;
                this._showLoginForm = typeof options.show_login_form !== 'undefined' ?  options.show_login_form : null;
                this._security = new Security();

                this._submitBtn = $('#commentFormContainer').find($(':submit'));

                this.events = $({});

                this._attachHandlers();
        }

        Comments.prototype = {
            constructor: Comments,
            _attachHandlers: function(){
                $('#addComment')
                    .on('click', $.proxy(this._onAddComment,this));
                this.attachCommentFormHandlers();
            },
            _focus: function(){
                $('#ws_comment_text').focus();
            },
            _onAddComment: function(){
                if (this._showLoginForm){

                    this._onShowLoginForm();
                    //$(window).on('loginSuccess', function(e){
                    //    $this.showCommentAddForm();
                    //});

                }
                else
                {
                    this.showCommentAddForm();
                }
            },
            _onShowLoginForm: function(){
              this.events.trigger($.Event('showloginform'));
            },
            _showCommentAddForm : function(){ $('#commentContainer').removeClass('hidden')},
            showCommentAddForm: function(){
                this._showCommentAddForm();
                this._focus();

            },
            attachCommentFormHandlers: function(){
                this._security.getSecurityView().onFromSubmited($('#commentFormContainer'),this._url, $.proxy(this._onFormSuccess,this), $.proxy(this._onFormError,this));
            },
            _onFormSuccess: function(){
                window.location.reload();
            },
            _onFormError: function(){
                //$("img[title='captcha']").attr('src','/generate-captcha/gcb_captcha?n='+(new Date()).getTime());
            },
            appendText: function(text){
                var commentText = $('#commentFormContainer').find($('#ws_comment_text'));
                commentText.val(commentText.val() +"\r\n"+ text);
            },
            removeText: function(text){
                var commentText = $('#commentFormContainer').find($('#ws_comment_text'));
                commentText.val(commentText.val().replace(text,""));
            },
            disableSubmit: function(){
                this._submitBtn.addClass('loading');
                this._submitBtn.prop('disabled', true);
            },
            enableSubmit: function(){
                this._submitBtn.removeClass('loading');
                this._submitBtn.prop('disabled', false);
            }
        }

        return Comments;

}
);
