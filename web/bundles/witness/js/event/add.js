/**
 * Created by kupriyanovym on 30.04.14.
 */
define([
    'jquery','map/set-center','security/security','comment','map-utils','yandexapi/yandexapi','event/view/add-form-view','view/vertical-filed-view','extend','datepicker','ymaps','fosjsrouting','fosjsroutes'
],
    function($,SetCenter,Security,Comment,MapUtils,Yapi,AddFormView,VerticalView){


        var Add = function(options){

            var addForm = $('#event_add');
            var verticalView = new VerticalView(addForm,options.is_form_submitted);

            this._security = new Security();
            this._securityView = this._security.getSecurityView();
            this._security.init(Routing.generate('fos_user_security_check'),Routing.generate('fos_user_registration_register'),{backdrop : true});
            this._security.showLoginFormByUser({show_login_form: options.app_user});


            this._add_form_view = new AddFormView(addForm);



            $('.datepicker').createDatePicker($('#ws_event_date').val() == "");
            //$('input[ac]').createAutoComplete(Routing.generate('site_band_witness_city_json'));

            this._yapi = new Yapi('#image-container','#add_img_btn');

            this._attachHandlers();

            var map = null;



            ymaps.ready(function() {
                MapUtils.loaderOn();
                map = MapUtils.mapInit(['smallMapDefaultSet']);
                var setCenter = new SetCenter(map,addForm,$('#ev-menu-container'));

                setCenter.addInit();
                setCenter.setVerticalView(verticalView);

                MapUtils.loaderOff();
            })

            //var hintView = new HintView(addForm);



        }

        Add.prototype = {
            constructor: Add,
            _attachHandlers: function(){
                this._securityView.events
                    .on('loginsuccess', $.proxy(this._onLoginSuccess,this))
                this._yapi.events
                    .on('onuploadsuccess',$.proxy(this._onUploadSuccess,this))
                    .on('onremoveimagestart',$.proxy(this._onRemoveImageStart,this))
                    .on('onremoveimagesuccess',$.proxy(this._onRemoveImageSuccess,this));
            },
            _onLoginSuccess: function(){
                window.location.reload();
            },
            _onUploadSuccess: function(params)
            {
                this._add_form_view.appendText(params.img_tag);

            },
            _onRemoveImageStart: function()
            {
                this._add_form_view.disableSubmit();
            },
            _onRemoveImageSuccess: function(params)
            {
                this._add_form_view.removeText(params.img_tag);
                this._add_form_view.enableSubmit();
            }


        }

        return Add

    })