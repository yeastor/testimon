/**
 * Created by kupriyanovym on 30.04.14.
 */
define([
    'jquery','map/set-center','map-utils','event/view/show-form-view','extend','datepicker','ymaps','fosjsrouting','fosjsroutes'
],
    function($,SetCenter,MapUtils,ShowFormView){

        var init = function(options){

            var showFromView = new ShowFormView($('#eventForm'));
            $('#date').createDatePicker(false).on('changeDate', function(ev){
                 showFromView.setFromToDate($(this).val());
            });

            var map = null;

            ymaps.ready(function() {
                MapUtils.loaderOn();
                map = MapUtils.mapInit(['smallMapDefaultSet']);
                var setCenter = new SetCenter(map,$('#eventForm'),$('#ev-menu-container'));

                setCenter.setLocation();
                MapUtils.loaderOff();
            });

        }

        return { init: init }
})