/**
 * Created by kupriyanovym on 28.05.2014.
 */
/**
 * Created by kupriyanovym on 27.05.2014.
 */
define([
        'jquery','view/list-events-view'
    ],
    function($,ListEventsView){
        /**
         *
         * @param container - container in which placed Events items and controls (button, etc)
         * @constructor
         */

        var ListEvents = function (container) {

            this._listEventsView = new ListEventsView(container);

            this._listEventsView.getEvents('+');

            this._attachHandlers();
        };

        ListEvents.prototype = {
            constructor: ListEvents,
            _attachHandlers: function(){
                this._listEventsView.events
                    .on("getprevevents", $.proxy(this._getPrevEvents,this))
                    .on("getnextevents", $.proxy(this._getNextEvents,this))

            },
            _getPrevEvents: function(){
                this._listEventsView.getEvents('-');
            },
            _getNextEvents: function(){
                this._listEventsView.getEvents('+');
            }
        }

        return ListEvents;

    })