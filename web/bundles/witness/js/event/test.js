/**
 * Created by kupriyanovym on 30.04.14.
 */
define([
    'jquery','map/set-center','security/security','comment','map-utils','extend','datepicker','ymaps','fosjsrouting','fosjsroutes'
],
    function($,SetCenter,Security,Comment,MapUtils){


        var Test = function(options){

            this._security = new Security();
            this._securityView = this._security.getSecurityView();
            this._security.init(Routing.generate('fos_user_security_check'),Routing.generate('fos_user_registration_register'));
            this._security.showLoginFormByUser({show_login_form: options.app_user});

            $('.datepicker').createDatePicker($('#ws_event_date').val() == "");
            //$('input[ac]').createAutoComplete(Routing.generate('site_band_witness_city_json'));

            this._attachHandlers();

            var map = null;

            ymaps.ready(function() {
                MapUtils.loaderOn();
                map = MapUtils.mapInit(['smallMapDefaultSet']);
                var setCenter = new SetCenter(map,$('#event_add'),$('#ev-menu-container'));

                setCenter.addInit();


                MapUtils.loaderOff();
            })
        }

        Test.prototype = {
            constructor: Test,
            _attachHandlers: function(){
                this._securityView.events
                    .on('loginsuccess', $.proxy(this._onLoginSuccess,this))
            },
            _onLoginSuccess: function(){
                window.location.reload();
            }


        }

        return Test

    })



