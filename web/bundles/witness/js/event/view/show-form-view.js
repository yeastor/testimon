/**
 * Created by kupriyanovym on 09.10.2014.
 */
define(['jquery'], function($)
{
    var ShowFormView = function(form){

            this._form = form;
            this._fromtodate = 0;
            this._serachButton = form.find('button[data-type=update]');
            this._eventbyRadio = form.find("input[name='eventby']");
            this._arratchHandlers();

    }

    ShowFormView.prototype = {
        constructor: ShowFormView,
        _arratchHandlers: function(){

            this._form.on('keypress', $.proxy(this._onFormSubmit,this));

            this._serachButton.on('click', $.proxy(this._onSearchClick,this))

            this._eventbyRadio.on('change',$.proxy(this._onEventByChange,this))

        },
        _onEventByChange:  function(){  this.setFromToDate($('#date').val());  },
        _onSearchClick: function(){

            if (this._fromtodate){  this.setEventFromHtml(this._fromtodate); }
        },
        _onFormSubmit: function(e){

            if (e.keyCode == 13) {
                return false;
            }
        },
        setFromToDate: function(selectedDateStr)
        {
            var selectedDateStrArr, eventsBy, selectedDate;

            selectedDateStrArr = selectedDateStr.split("/");

            selectedDate = new Date(selectedDateStrArr[2],selectedDateStrArr[1] - 1,selectedDateStrArr[0]);

            eventsBy = this._form.find("input[name='eventby']:checked").val();

            if (eventsBy == 'week') {
                this._fromtodate = this._getWeekDateInterval(selectedDate, new Date(selectedDate.getTime()));
            }
            else{
                this._fromtodate = "за "+ selectedDate.getDate() +"."+ (selectedDate.getMonth()+1);
            }

            this.setEventFromHtml(this._fromtodate);

        },
        setEventFromHtml: function(html){
            $('.eventfromto').html(html);
        },
        _getWeekDateInterval: function(dateFrom,dateTo)
        {
            currentDate = new Date();
            currentDatedayOfWeek = (currentDate.getDay() == 0) ? 7 : currentDate.getDay();
            dayOfWeek = (dateFrom.getDay() == 0) ? 7 : dateFrom.getDay();

            Date.prototype.getWeek = function() {
                var onejan = new Date(this.getFullYear(), 0, 1);
                return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
            }


            if ((currentDate.getYear() + "-"+currentDate.getWeek() == dateFrom.getYear() + "-" + dateFrom.getWeek())){
                dateFrom.setDate(dateFrom.getDate() - 7);
                dateTo.setDate(dateTo.getDate() + (currentDatedayOfWeek - dayOfWeek));
            }
            else{
                dateFrom.setDate(dateFrom.getDate() - (dayOfWeek - 1));
                dateTo.setDate(dateTo.getDate() + (7 - dayOfWeek));
            }
            return "c " + dateFrom.getDate() +"."+ (dateFrom.getMonth()+1) + " по " + dateTo.getDate() +"."+ (dateTo.getMonth()+1);
        }




    }

    return ShowFormView;
})