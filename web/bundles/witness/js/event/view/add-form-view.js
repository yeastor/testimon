/**
 * Created by kupriyanovym on 19.08.2014.
 */
define(['jquery','typeahead'],function($){

    var AddFromView = function(form){

        this._form = form;
        this._imagesTxt = form.find($('.images'));
        this._submitBtn = form.find($(':submit'));

        this._typeaheadEnable(form.find($('#ws_event_title')));

    }
    AddFromView.prototype = {
        constructor: AddFromView,
        appendText: function(text){
            var imagesText = this._imagesTxt;
            imagesText.val(imagesText.val() +" \r\n"+ text);
        },
        removeText: function(text){
            var imagesText = this._imagesTxt;
            imagesText.val(imagesText.val().replace(text,""));
        },
        disableSubmit: function(){
            this._submitBtn.addClass('loading');
            this._submitBtn.prop('disabled', true);
        },
        enableSubmit: function(){
            this._submitBtn.removeClass('loading');
            this._submitBtn.prop('disabled', false);
        },
        _typeaheadEnable: function(input){
            var tags = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                limit: 10,
                prefetch: {
                    // url points to a json file that contains an array of country names, see
                    // https://github.com/twitter/typeahead.js/blob/gh-pages/data/countries.json
                    url: '/bundles/witness/json/titletags.json'//,
                    // the json file contains an array of strings, but the Bloodhound
                    // suggestion engine expects JavaScript objects so this converts all of
                    // those strings
                    //filter: function(list) {
                    //    return $.map(list, function(tags) { return { name: tags }; });
                   // }
                }
            });

        // kicks off the loading/processing of `local` and `prefetch`
            tags.initialize();

            // passing in `null` for the `options` arguments will result in the default
            // options being used
            input.typeahead(null, {
                name: 'tags',
                displayKey: 'name',
                // `ttAdapter` wraps the suggestion engine in an adapter that
                // is compatible with the typeahead jQuery plugin
                source: tags.ttAdapter()
            });
        }
    }


    return AddFromView;
})
