/**
 * Created by kupriyanovym on 30.04.14.
 */
define([
    'jquery','security/security','comment','map-utils','yandexapi/yandexapi','ymaps','fosjsrouting','fosjsroutes'
],
    function($,Security,Comment,MapUtils,Yapi){


        var Get = function(options){

            this._security = new Security();
            this._securityView = this._security.getSecurityView();
            this._comment = new Comment({url:  Routing.generate('site_band_witness_comment_add'), show_login_form: options.app_user});

            this._security.init(Routing.generate('fos_user_security_check'),Routing.generate('fos_user_registration_register'),{backdrop : true});



            //$('#date').createDatePicker(false);
            var map = null;

            ymaps.ready(function() {
                MapUtils.loaderOn();
                map = MapUtils.mapInit(['zoomControl']);
                map.setCenter(options.coords,15);
                var myPlacemark = new ymaps.Placemark(options.coords, {
                    hintContent: options.event_title,
                    balloonContentHeader: options.event_title,
                    balloonContentBody: options.event_description
                });
                map.geoObjects.add(myPlacemark);
                MapUtils.loaderOff();
            })

            this._yapi = new Yapi('#image-container','#add_img_btn');

            this._attachHandlers();

        }

        Get.prototype = {
            constructor: Get,
            _attachHandlers: function(){
                this._comment.events
                    .on('showloginform', $.proxy(this._onShowLoginForm, this));
                this._securityView.events
                    .on('loginsuccess', $.proxy(this._onLoginSuccess,this));
                this._yapi.events
                    .on('onuploadsuccess',$.proxy(this._onUploadSuccess,this))
                    .on('onremoveimagestart',$.proxy(this._onRemoveImageStart,this))
                    .on('onremoveimagesuccess',$.proxy(this._onRemoveImageSuccess,this))
            },
            _onShowLoginForm: function(){
                this._security.showLoginForm();
            },
            _onLoginSuccess: function(){
                this._comment.showCommentAddForm();
            },
            _onUploadSuccess: function(params)
            {
                this._comment.appendText(params.img_tag);

            },
            _onRemoveImageStart: function()
            {
                this._comment.disableSubmit();
            },
            _onRemoveImageSuccess: function(params)
            {
                this._comment.removeText(params.img_tag);
                this._comment.enableSubmit();
            }


        }

        return Get

    })



