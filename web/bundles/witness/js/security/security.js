define(['jquery','bootstrap','model/security-model','view/security-view'],

function($,Bootstrap,SecurityModel,SecurityView){

        var Security = function(){

            this._securityModel = new SecurityModel();
            this._securityView = new SecurityView();

            this._options = null;

        }

        Security.prototype = {
            constructor: Security,
            init: function(secPath,regPath,options){
                this._securityModel.init(secPath,regPath);
                this._securityView.setOptions(options);
                //this._securityView.showLoginModalFrom();
            },
            showLoginForm: function(){
                this._securityView.showLoginModalFrom();
                this._securityView.attachSecFormHandlers(this._securityModel.getSecPath(),this._securityModel.getRegPath());
            },
            getSecurityView: function(){
                return this._securityView;
            },
            showLoginFormByUser: function(options){
                var showLoginForm = typeof options.show_login_form !== 'undefined' ?  options.show_login_form : null;
                if (showLoginForm){ this.showLoginForm();}
            }


        }

        return Security;


})