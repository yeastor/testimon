/**
 * Created by kupriyanovym on 25.07.2014.
 */
/**
 * Created by kupriyanovym on 25.07.2014.
 */
define(['jquery','bootstrap'],function($){

    var FeedBackFormView = function(form_container,form_url){
        this._form_container = form_container;
        this.modal_body = this._form_container.find('.form-content');
        this._form_url = form_url;
        this._is_form_load = false;

        this.events = $({});

        this._attachHandlers();
    }

    FeedBackFormView.prototype = {
        constructor: FeedBackFormView,
        _attachHandlers: function(){
            this._form_container.find('#form-close').on('click',$.proxy(this._onClose,this))
        },
        toggle: function(){
            this._form_container.detach().appendTo('.navbar-right');
            if (!this._is_form_load) {this._loadForm();}
            this._is_form_load = true;
            this._form_container.toggle('show');
        },
        _loadForm: function(){
            this.modal_body.load(this._form_url, $.proxy(this._onFormLoad,this));

        },
        _onFormLoad: function(response, status, xhr){
            this.events.trigger($.Event('formloaded'));
        },
        showSuccessMessage: function(){
            this._form_container.find(".modal-body").html("Спасибо за ваше сообщение.");
            this._form_container.find("input[type='submit']").hide();
            this._form_container.delay(2000).hide("slow");
        },
        _onClose: function(){
            this._form_container.hide("slow");
        },
        setUserInfo: function(val) {
            this._form_container.find('.user-info').val(val);
        },
        reloadCaptcha: function(){
            var reloader = this._form_container.find(".captcha_reload");
            if (reloader.length != 0) {
                eval(reloader.attr('href').replace('javascript:', ''));
            }
        }

    }
    return FeedBackFormView;
})