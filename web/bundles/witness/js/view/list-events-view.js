/**
 * Created by kupriyanovym on 27.05.2014.
 */
define([
        'jquery','fosjsrouting','fosjsroutes'
    ],
    function($){

        var ListEventsView = function(container){
            this._container = container;
            this._prevBtn = container.find('.event-list-controls .prev');
            this._nextBtn = container.find('.event-list-controls .next');
            this._dataContainer = container.find('.event-list-data');
            this._loader = container.find('.loader');
            this.events = $({});
            this._attachHandlers();
            this._offset = 0; //page

        }

        ListEventsView.prototype = {
                constructor: ListEventsView,
                _attachHandlers: function(){
                    this._prevBtn
                        .on('click', $.proxy(this._onPrevClick,this));
                    this._nextBtn
                        .on('click', $.proxy(this._onNextClick,this));
                },
                _onPrevClick: function(){
                    if (this._prevBtn.hasClass('disabled')) { e.preventDefault();}
                    else {
                        this.events.trigger($.Event('getprevevents'))
                    }
                },
                _onNextClick: function(){
                    if (this._nextBtn.hasClass('disabled')) { e.preventDefault();}
                    else {
                        this.events.trigger($.Event('getnextevents'))
                    }
                },
                getEvents: function(operator){
                    var defaultConfig = this._defaultConfig();
                    var requestUrl = Routing.generate('site_band_witness_events_last');
                    var config = this._getConfigValues(this._container,operator);
                    var request = $.extend(config ,defaultConfig);
                    this._loaderOn();

                    this._dataSource(requestUrl,request,$.proxy(this._render, this),$.proxy(this._loaderOff, this));
                },
                _dataSource: function(url,data,callback,callbackAllways){
                    $.ajax({
                        type        : 'POST',
                        url         : url,
                        data        : data,
                        dataType    : "json",
                        success     : function(data, status, object) {

                            callback(data);

                        },
                        error: function(data, status, object){
                            //alert(status);
                            console.log(data.message);
                        }


                    }).always(function() {
                        callbackAllways();
                    });
                },
                _defaultConfig: function(){
                    return {limit: 4}
                },
                _getConfigValues: function(container,operator){
                    var sortType;
                    if (container.attr('id').indexOf('added') != -1) {
                        sortType = 'added';
                    }
                    if (container.attr('id').indexOf('by-date') != -1) {
                        sortType = 'bydate';
                    }
                    var offset = this._offset;
                    var operators =  {
                        '+' : function() { return ++offset  },
                        '-' : function() { return --offset  }
                    }

                    return {offset: operators[operator](), sort: sortType, operator: operator}
                },
                _render: function(data){
                        (data.is_next) ? this._nextBtn.removeClass('disabled') : this._nextBtn.addClass('disabled');
                        (data.is_prev) ? this._prevBtn.removeClass('disabled') :  this._prevBtn.addClass('disabled');

                    this._switchPage(data.operator);

                    this._dataContainer.html(data.events);
                },
                _switchPage: function(operator){
                    var offset = this._offset
                    var operators =  {
                        '+' : function() { return ++offset  },
                        '-' : function() { return --offset  }
                    }
                    this._offset = operators[operator]();
                },
                _loaderOn: function loaderOn() {
                    this._loader.show();
                },
                _loaderOff: function () {
                    this._loader.hide();
                }


        }


        return ListEventsView;
    })