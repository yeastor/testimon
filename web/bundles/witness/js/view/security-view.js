define(['jquery','bootstrap'],
function($){

        var SecurityView = function(){

            this._options = null;
            this.events = $({});
        }

        SecurityView.prototype = {
            constructor: SecurityView,
            showLoginModalFrom: function(){
                $('#loginModal').modal(this._options);

            },

            attachSecFormHandlers: function(secPath,regPath){
                this.onFromSubmited($('#login-form'),secPath,$.proxy(this._onloginSuccess, this));
                this.onFromSubmited($('#registeration-form'),regPath,$.proxy(this._onRegistrationSuccess, this));
                $('#loginModal').on('hidden.bs.modal',$.proxy(this._onHideModal, this));

                $('#fos_user_registration_form_username').on('input',$.proxy(this._onEnteringUserName, this));
            },
            _onEnteringUserName: function(){
                $('#username').val($('#fos_user_registration_form_username').val());
            },
            _onHideModal: function(){
                this.events.trigger($.Event('hidemodal'));
            },
            _onloginSuccess: function(){
                $('#loginModal').modal('hide');
                this.events.trigger($.Event('loginsuccess'));
            },
            _onRegistrationSuccess: function(text){
                $('#registeration-form').html('<div class="alert alert-success">'+text+'</div>');
            },
            setOptions : function(options){

                this._options = options;
            },
            onFromSubmited : function(formContainer,url, successFn, failFn){

                form = $("form",formContainer);
                form.submit(function(e){e.preventDefault();
                    form = $(e.target);
                    form.addClass('loading');
                    $.ajax({
                        type        : form.attr( 'method' ),
                        url         : url,
                        data        : form.serialize(),
                        dataType    : "json",
                        success     : function(data, status, object) {

                            if(data.success == false) {
                                //$('.alert',this).removeClass('hidden').html(data.message);

                                errors = $('<ul></ul>');

                                $.each(data.message,function(i,val){

                                    errors.append("<li>"+data.message[i]+"</li>");

                                })
                                $('.alert',formContainer).removeClass("hidden").html(errors);
                                if (typeof failFn !== 'undefined'){
                                    failFn();
                                }

                            }
                            else {  successFn(data.message);  }
                        },
                        error: function(data, status, object){
                            alert(status);
                            console.log(data.message);
                        }
                    }).always( function() { form.removeClass('loading');});
                });
            }


        }

        return SecurityView;
})
