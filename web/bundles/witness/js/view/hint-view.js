/**
 * Created by kupriyanovym on 11.07.2014.
 * show hint on form
 */
define(['jquery'],function($){

    var HintView = function(form){

        this._form = form;

        this._initAllHints();
        this._attachHandlers();
    }

        HintView.prototype = {
            constructor: HintView,
            _attachFocusHandlers: function () {
                //this._serachButton.on('click', $.proxy(this._onSearchClick,this))

                this._form.find("input[type='text']").on('focus',  $.proxy(this._showHint,this));
                this._form.find("textarea").on('focus', $.proxy(this._showHint,this));
            },
            _attachHandlers: function () {
                //this._serachButton.on('click', $.proxy(this._onSearchClick,this))
                this._attachFocusHandlers();
               // this._form.find("input[type='text']").on('focusout',  $.proxy(this._hideAllBox,this));
               // this._form.find("textarea").on('focusout', $.proxy(this._hideAllBox,this));
                this._form.find(".boxclose").on('click', $.proxy(this._hideBox,this));
            },
            _detachFocusHandlers: function(){
                this._form.find("input[type='text']").off('focus', $.proxy(this._showHint,this));
                this._form.find("textarea").off('focus',  $.proxy(this._showHint,this));
            },
            _initAllHints: function(){
                this._form.find("label[data-hint]").each(function(){

                    var hint = $("<p class='hint'><a class='boxclose' id='boxclose'></a>"+$(this).data("hint")+"</p>");
                    if ($(this).data("hint-image")){
                        var img = $("<img src='/images/instruction/"+$(this).data("hint-image")+"'/>");
                        hint.append(img);
                    };
                    $(this).append(hint);

                })
            },
            _onInputFocus: function(){
                    this._showHint();
            },
            _showHint: function(e){
                this._hideAllBox();
                var hint = $('label p',$(e.target).parents(".form-group"));

                if (hint.data('hidden') != '1') {
                    hint.show('fast')
                }
                else {
                    hint.data('hidden','0');
                }
            },
            _hideBox: function(e)
            {
                $(e.target).parent().hide('slow').data('hidden','1');

            },
            _hideAllBox: function(e){
                this._form.find($('label p')).hide('slow');

            }


        }



  return HintView;
})
