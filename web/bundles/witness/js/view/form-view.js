define(['jquery','utils'],function($,Utils){

    var FromView = function(form){

        this._form = form;
        this._cityObj = form.find('.city');
        this._addressObj = form.find('.address');
        this._countryObj = form.find('.country');
        this._dateObj = form.find('.datepicker');
        this._coordsObj = form.find('.coords');
        this._serachButton = form.find('[data-type=update]');
        this._utils = Utils;
        this.events = $({});
        this._attachHandlers();
        this._maxObjectCountInMenu = 10;
    }
        FromView.prototype = {
            constructor: FromView,
            _attachHandlers: function () {
                this._serachButton.on('click', $.proxy(this._onSearchClick,this))


            },
            attachFormHandlers: function(){
                this._form
                    .on('submit', $.proxy(this._onFormSubmit, this));
                this._form.find('.checkpoint')
                    .on('click', $.proxy(this._onCheckPointClick,this));
            },
            _onSearchClick: function(e){
              this.events.trigger($.Event('searchaddress',{
                  address: this._utils.createAddress(this._countryObj.text(),this._cityObj.val(),this._addressObj.val())

              }))
                  .trigger($.Event('queryevents',{
                      date: this._dateObj.val(),
                      eventsBy: this._form.find("input[name='eventby']:checked").val()
                  }))

            },
            _onFormSubmit: function(e) {
                this.events.trigger($.Event('checkPoint'));
            },
            _onCheckPointClick: function(e){
                this.events.trigger($.Event('checkPoint'));
            },
            setCity: function(cityName){

                this._cityObj.val(cityName);
            },
            getForm: function(){
                return this._form;
            },
            getCityObj: function(){
                return this._cityObj;
            },
            getDateObj: function(){
              return this._dateObj;
            },
            getAddressObj: function(){
                return this._addressObj;
            },
            getCoordsObject: function(){
                return this._coordsObj;
            },
            getEventByValue: function(){
                return this._form.find("input[name='eventby']:checked").val()
            },
            setAddressCenter: function(coords){
                this._coordsObj.val(coords);
            },
            getAddressCenter: function(){
              return this._coordsObj.val().split(',');
            },
            setCityObjValByText: function(text) {
                var re = /\s*,\s*/;
                var addressList = text.split(re);
                if (addressList[1])
                {
                    var city = addressList[1];
                    this._cityObj.val(city);
                     this._cityObj.change(); // update kladr
                }
            },
            setAddressObjVal: function(value) {
                this._addressObj.val(value)
            },
            showCheckPointError: function(messageHtml) {
                this._showMessageContainer('#chkperr',messageHtml,'alert alert-danger');
            },
            showCheckPointSuccess: function(messageHtml) {
                this._showMessageContainer('#chkperr',messageHtml,'alert alert-success');
            },
            _showMessageContainer: function(containerSelector,messageHtml,containerClass)
            {
                var checkpointErrorContainer = $(containerSelector);
                checkpointErrorContainer.html(messageHtml);
                checkpointErrorContainer.attr( "class", containerClass );

            }


        }




    return FromView;
})
