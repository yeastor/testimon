/**
 * Created by kupriyanovym on 18.11.2014.
 */
/**
 * Created by kupriyanovym on 11.07.2014.
 * show hint on form
 */
define(['jquery'],function($){

    var VerticalView = function(form,isFormSubmitted){

        this._form = form;
        if (isFormSubmitted != true) {
            this.hideAll();
            this.start();
        }
        else{
            this.focusError();
        }

        this._attachHandlers();


    };

    VerticalView.prototype = {
        constructor: VerticalView,

        _attachHandlers: function ()
        {
                $('.next',this._form).click($.proxy(this._getStep,this));
        },
        start: function()
        {
                this._showStep('#step-1');
        },
        _getStep: function(e)
        {
            var step = $(e.currentTarget).attr('href');
            this.gotoStep(step);
            this.hideShadow(
                $(e.currentTarget).parent(".shadow2")
            );
            $(e.currentTarget).hide();


        },
        hideShadow: function(el)
        {
            el
                .removeClass('shadow2')
        },
        _showStep: function(step)
        {
            $(step).removeClass('hidden');
        },
        _setFocus: function(step)
        {
            $(step+' .input').trigger('focus');//.focus();
            //$(step+' textarea').focus();
        },
        gotoStep: function(step)
        {
            this._showStep(step);
            this._setFocus(step);
            window.location.hash = step;
        },
        expandAll: function()
        {
            $(".step").removeClass('hidden');
        },
        hideAll: function()
        {
            $(".step").addClass('hidden');
        },
        focusError: function()
        {
            //alert( $(".alert",this._form).first().next('.form-control').first().focus());
            //$('html, body').animate({ scrollTop: $('.alert',this._form).offset().top }, 'slow');
            $(".field-error",this._form).focusin();
            //$(".alert",this._form).first().parent(".step").find('.form-control').focus();

        }





    };



    return VerticalView;
});
