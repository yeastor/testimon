define(['jquery'],function($){

    var MenuView = function(menuContainer){
        this._container = menuContainer;
        this._eventMenu = menuContainer.find('.ev-menu');
        this._addButton = menuContainer.find('.ev-actions input');
        this._maxObjectCountInMenu = 10;
    }

    MenuView.prototype = {
        showEventMenuContainer : function(){
            this._container.fadeIn('slow');
        },
        setMenuIfNoObjects: function(text){
            var noObjectHtml = $('<div class="alert alert-warning">'+text+'</div>');
            this._eventMenu.html(noObjectHtml);
        },
        clearErrors: function(){
            this._eventMenu.html("");
        },
        setMenuByMapWisibleObjects: function(objects){
            var i = 1;
            var menu = $('<div class="ev-menu-inner"></div>');
            var maxObjectCountInMenu = this._maxObjectCountInMenu;
            var $this = this;
            objects.each(function(point){
                if (i > maxObjectCountInMenu)
                {var menuItem = $('<div id="event-max">Показаны первые '+maxObjectCountInMenu+' событий</div>');  menuItem.appendTo(menu); return;}
                var menuItem = $this._createMenu(point);
                menuItem.appendTo(menu);

                // open balloon by click
              //  menuItem.find('a').hover(function () {
             //       point.balloon.open();
             //   });

                point.events.add(['balloonopen','balloonclose'], function (e) {
                    if (e.get('type') == 'balloonopen')
                    { $this._showEventContent();  $this._showEventDetails($this._createMenu(point)); }
                    else $this._showEventDetails('');
                });

                i++;
            });
            //alert (objectsInsideMap.length());

            $(".ev-menu").html(menu);
        },
        _createMenu: function(point){
            var eventId = point.properties.get('id');
            var menuItem = $('<div id="event-'+eventId+'">'+point.properties.get('metaDataProperty.AnyMetaData.date')+' <a class="pointMenu" href="'+Routing.generate('site_band_witness_event_get',{ id: eventId})+'">' + point.properties.get('name') + '</a> <br />' +
                point.properties.get('metaDataProperty.AnyMetaData.address')+'</div>');




            var description = $('<p id="event-description-'+point.properties.get('id')+'"></p>');
            var descriptionHtml = $('<p class="event-description">'+point.properties.get('description')+'</p>');

            var showHideDescriptionButton = $('<button class="btn btn-sm btn-default" type="button">Показать описание</button>').click(
                function(){
                    descriptionHtml.toggle('slow');
                }
            )
            descriptionHtml.appendTo(description);
            description.appendTo(menuItem);
            showHideDescriptionButton.appendTo(menuItem);

            return menuItem;
        },
        _showEventContent : function(){
            $('#events-content').removeClass('hidden');
        },
        _showEventDetails : function(html)
        {
            $('#event-detail').html(html);
        },
        showAddMenuButton: function(text){
            if (text === undefined) {
                this._showElement(this._addButton.val("Добавить событие"));
            }
            else
            {
                this._addButton.val(text);
            }
        },
        _showElement: function(obj){
            obj.removeClass('hidden');
        }

    }

    return MenuView;
})