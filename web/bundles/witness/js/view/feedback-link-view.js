/**
 * Created by kupriyanovym on 25.07.2014.
 */
define(['jquery'],function($){

    var FeedBackLinkView = function(fb_link){
        this._fb_link = fb_link;
        this.events = $({});

        this._attachHandlers();
    }

    FeedBackLinkView.prototype = {
        constructor: FeedBackLinkView,
        _attachHandlers: function(){
            this._fb_link.on('click', $.proxy(this._onFbClick,this))
        },
        _onFbClick: function(){

            this.events.trigger($.Event('fblinkclick'));
        }
    }
    return FeedBackLinkView;
})