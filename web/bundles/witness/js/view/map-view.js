define(['jquery'],function($)
{
    var MapView = function(map){

        this._map = map;
        this._clusterer = null;
        this._geoObjects = null;
        this._point = null;
        this.events = $({});

    }

    MapView.prototype = {
        constructor: MapView,
        setLocation: function(result){

            var geoObject = result.geoObjects.get(0);
            this.setCenter(geoObject.properties.get('boundedBy')[0],8);

            //this.bindBoundChange();

        },
        setCenterByAddress: function(result){
            var geoObject = result.geoObjects.get(0);
            this._setMapBounds(geoObject.properties.get('boundedBy'));
        },
        renderClusterer: function(result){
            this._geoObjects = result;
            this._clusterer = result.clusterize();
            this._setClustereOptions();

            this._map.geoObjects.add(this._clusterer); // Добавление геообъектов на карту


            this.events.trigger($.Event('clusterready', {
                geoObjects: result.searchInside(this._map)
            }));
        },
        setCenter: function(coords,zoom)
        {
            this._map.setCenter(coords,zoom, {
                checkZoomRange: true,
                duration: 1
            });
        },
        _setMapBounds: function (bounds) {
            this._map.setBounds(bounds, {
                checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
            });
        },
        clearClusterer: function(){
          if(this._clusterer) {
                    this._clusterer.removeAll();
                    this._geoObjects = null;
          }
        },
        clearPoint: function(){
            if(this._point) {
                this._map.geoObjects
                    .remove(this._point);
                this._point = null;
            }

            return this;
        },
        bindBoundChange: function(){
            this._map.events.add('boundschange',
                $.proxy(this._bindBoundChange, this));
        },
        bindMapClick: function(){
            this._map.events.add('click',  $.proxy(this._onMapClick, this));
        },
        _onMapClick: function(e){
                var coords = e.get('coords');
                if (this._point) {
                    this._point.geometry.setCoordinates(coords);
                }
                else {
                    this._point = new ymaps.Placemark(coords, {
                        iconContent: 'поиск...'
                    }, {
                        preset: 'islands#violetStretchyIcon',
                        draggable: true
                    });

                    this._point.events.add('dragend',$.proxy(this._onPointDraged, this));

                    this._map.geoObjects
                        .add(this._point);
                }

            this.events.trigger($.Event('pointready', {
                coords: this._point.geometry.getCoordinates()
            }));

        },
        _onPointDraged: function(){
            this.events.trigger($.Event('pointready', {
                coords: this._point.geometry.getCoordinates()
            }));
        },
        _bindBoundChange: function(){
            if (this._geoObjects)
            this.events.trigger($.Event('clusterready', {
                geoObjects: this._geoObjects.searchInside(this._map)
            }));
        },
        _setClustereOptions: function(){
            this._clusterer.options.set({
                preset: 'islands#invertedVioletClusterIcons',
                /**
                 * Ставим true, если хотим кластеризовать только точки с одинаковыми координатами.
                 */
                groupByCoordinates: false

            });
        },
        loaderOn: function loaderOn() {
            $("#map-container .loader").show();
        },
        loaderOff: function () {
            $("#map-container .loader").hide();
        },
        getObjectCoordinate: function(geoObjects){
            var geoObject = geoObjects.get(0);
            return geoObject.geometry.getCoordinates();
        },
        getGeoObjects: function(result){
            return result.geoObjects;
        },
        updatePoint: function(result){
            var geoObject = result.geoObjects.get(0);
            if (this._point) {
                this._point.properties
                    .set({
                        iconContent: geoObject.properties.get('name'),
                        balloonContent: geoObject.properties.get('text')
                    })
            }
        },
        getObjectName: function(result){
            var geoObject = result.geoObjects.get(0);
            return geoObject.properties.get('name')
        },
        getObjectText: function(result){
            var geoObject = result.geoObjects.get(0);
            return geoObject.properties.get('text')
        },
        getPoint: function(){
            return this._point;
        },
        getObjectsInsideMap: function(result) {
            return result.searchInside(this._map).getLength();
        },
        addGeoObject: function(){
            myGeoObject = new ymaps.GeoObject({
                geometry: {type: "Point", coordinates: [56.021, 36.983]},
                properties: {
                    clusterCaption: 'Геообъект №2',
                    balloonContentBody: 'Содержимое балуна геообъекта №2.'
                }
            });


            this._geoObjects.add(myGeoObject);
            this.renderClusterer(this._geoObjects);
        }




    }

    return MapView;



})
