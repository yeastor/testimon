define(['jquery'],
function($){

        var SecurityModel = function(){

            this._secPath = null;
            this._regPath = null;

        }

        SecurityModel.prototype = {
            constructor: SecurityModel,
            init: function(secPath,regPath){
                this.setSecPath(secPath);
                this.setRegPath(regPath);

            },
            setSecPath : function(url){

                this._secPath = url;
            },
            setRegPath : function(url){

                this._regPath = url;
            },
            getSecPath: function(){
                return  this._secPath;
            },
            getRegPath: function(){
                return  this._regPath;
            }

        }

    return SecurityModel;
})
