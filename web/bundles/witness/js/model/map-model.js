define(['jquery','ymaps'],function($){

    var MapModel = function(map)
    {
        this._map = map;
        this._geoObject = null;
        this._error = null;
        this._geoQueryResult = null;
        this._xmlResult = null;

    };

    MapModel.prototype = {
        constructor: MapModel,
        getCurrentLocation : function(){

            var promise =  ymaps.geolocation.get({ autoReverseGeocode: true });
            this.clear();
            promise.then($.proxy(this._onSearchSuccess, this),
                $.proxy(this._onSearchFailed, this)
            );
            return promise;
        },
        search: function(request,params){
            var options = this._getGeocodeOptions(params);
            var promise = ymaps.geocode(request, options);

            this.clear();

            promise.then(
                $.proxy(this._onSearchSuccess, this),
                $.proxy(this._onSearchFailed, this)
            );

            return promise;
        },
        query: function(geoObjects){

            var $this = this;
            var result = ymaps.geoQuery(geoObjects);

            result.then(
                function(){ $this._geoQueryResult = result;  },
                $.proxy(this._onSearchFailed, this)
            );
            return result;
        },
        queryNoPromise: function(geoObjects){
            return ymaps.geoQuery(geoObjects);
        },
        xml: function(url){
            var promise = ymaps.geoXml.load(url);

                promise.then(
                    $.proxy(this._onXmlSuccess, this),
                    $.proxy(this._onSearchFailed, this)
                );


            return promise;
        },

        clear: function () {
            this._result = null;
            this._error = null;
        },
        clearClusterer: function(){
            this._geoQueryResult = null;
            this._error = null;
        },
        _onXmlSuccess: function(res){
            this._xmlResult = res.geoObjects;
        },
        _onQuerySuccess: function () {
            alert(result);
        },
        _onSearchSuccess: function (result) {
            this._geoObject = result.geoObjects.get(0);
        },
        _onSearchFailed: function (error) {
            this._error = error;

        },
        getGeoObject : function(){
            return this._geoObject;
        },
        getXmlResults : function(){
            return this._xmlResult;
        },
        getQueryResult : function(){
            return this._geoQueryResult;
        },
        getAddressList: function(){
            var re = /\s*,\s*/;
            var addressList = this._geoObject.properties.get('description').split(re);
            if (addressList.length < 2)
                addressList.push(this.getName);
            return addressList;
        },
        getName: function(){
            return this._geoObject.properties.get('name')
        },
        getCoords: function(){
            return this._geoObject.geometry.getCoordinates();
        },
        getCurrentMapCoords: function(){
            return this._map.getCenter();
        },
        _getGeocodeOptions: function(params)
        {
            var defaultOptions = {
                results: 1 // Если нужен только один результат, экономим трафик пользователей
                };
            if (params === undefined) return defaultOptions;
            else{
                return $.extend({},defaultOptions,params);
            }
        }

    }

    return MapModel;


})