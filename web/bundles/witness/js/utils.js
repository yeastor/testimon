define(['jquery','kladr'],function($){

    var Utils = function(){

    }

    Utils.prototype = {
        constructor: Utils,
        createAddress :function (country,city,address)
        {
            return country+","+city+","+address;
        },
        kladrOn: function(form)
     {

        var cityObj = form.find('.city');
        var addressObj = form.find('.address');
        // Подключение автодополнени// Автодополнение населённых пунктов

        cityObj.kladr({
            token: '52f3362d31608f796e000002',
            key: 'fec91b1ef87f3037ae274e94dde3ffbf7612d3dd',
            type: $.kladr.type.city,
            verify: true,
            check: function( obj ) {

                if (obj){
                    // Изменения родительского объекта для автодополнения улиц
                    addressObj.kladr('parentId', obj.id);
                    addressObj.kladr('parentType',$.kladr.type.city);
                }
                else
                {
                    addressObj.kladr('parentType',false);
                    //cityObj.css('color','red');
                }
            }
        });

        // Автодополнение улиц

        addressObj.kladr({
            token: '52f3362d31608f796e000002',
            key: 'fec91b1ef87f3037ae274e94dde3ffbf7612d3dd',
            type: $.kladr.type.street,
            parentType: $.kladr.type.city,
            parentInput: cityObj,
            valueFormat: function(obj, query) {

                var label = '';
                if(obj.typeShort){
                    label += obj.typeShort + '. ';
                }
                label += obj.name;
                return label;
            }

        });

        cityObj.change();

    },
        getCookie: function getCookie(name) {

        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
        }
    }

    return new Utils();

})