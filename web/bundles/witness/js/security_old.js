define(['jquery','bootstrap'],
    function($){

        var secPath,regPath;
        $this = this;

        return {
                init : function(secPath,regPath,options){
                var $this = this;
                $this.setRegPath(regPath);
                $this.setSecPath(secPath);
                $(window).on('showLoginForm', function(e){
                        $this.showLoginModalFrom(options);
                });

                },
                setSecPath : function(url){

                secPath = url;
                },

                setRegPath : function(url){

                regPath = url;
                },
                showLoginModalFrom : function(options){
                var $this = this;
                $('#loginModal').modal(options);
                this.onFromSubmited($('#login-form'),secPath,function(){$this.onLoginSuccess(); $('#loginModal').modal('hide'); $('#ws_comment_text').focus(); });
                this.onFromSubmited($('#registeration-form'),regPath,function(){$('#registeration-form').html('<div class="alert alert-success">'+data.message+'</div>'); });
                },
                onFromSubmited : function(formContainer,url, successFn, failFn){

                form = $("form",formContainer);
                form.submit(function(e){e.preventDefault();
                    form = $(e.target);
                    $.ajax({
                        type        : form.attr( 'method' ),
                        url         : url,
                        data        : form.serialize(),
                        dataType    : "json",
                        success     : function(data, status, object) {

                            if(data.success == false) {
                                //$('.alert',this).removeClass('hidden').html(data.message);

                                errors = $('<ul></ul>');

                                $.each(data.message,function(i,val){

                                    errors.append("<li>"+data.message[i]+"</li>");

                                })
                                $('.alert',formContainer).removeClass("hidden").html(errors);
                                if (typeof failFn !== 'undefined'){
                                    failFn();
                                }

                            }
                            else {  successFn(data.message);  }
                        },
                        error: function(data, status, object){
                            alert(status);
                            console.log(data.message);
                        }
                    });
                });
            },

            onLoginSuccess : function(){
                $(window).trigger('loginSuccess');
            }


        };

    }


)