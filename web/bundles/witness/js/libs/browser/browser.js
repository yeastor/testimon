/**
 * Created by kupriyanovym on 05.08.2014.
 */
define([],function(){

    var Browser = function(){
        this._browser = null;

    }

    Browser.prototype = {
        constructor: Browser,
        getInfo: function(){
            this._browser =
        "page:" + window.location.href +
        "\r\n screen.width: " + screen.width+'x'+screen.height
        +"\r\n mimeTypes: " + navigator.mimeTypes+
        "\r\n plugins: " + navigator.plugins+
        "\r\n doNotTrack: " + navigator.doNotTrack +
        "\r\n oscpu: " + navigator.oscpu+
        "\r\n vendor: " + navigator.vendor +
        "\r\n vendorSub: " + navigator.vendorSub +
        "\r\n productSub: " + navigator.productSub +
        "\r\n cookieEnabled: " + navigator.cookieEnabled+
        "\r\n buildID: " + navigator.buildID +
        "\r\n battery: " + navigator.battery +
        "\r\n geolocation: " + navigator.geolocation +
        "\r\n appCodeName: " + navigator.appCodeName +
        "\r\n appName: " + navigator.appName +
        "\r\n appVersion: " + navigator.appVersion +
        "\r\n platform: " + navigator.platform +
        "\r\n userAgent: " + navigator.userAgent +
        "\r\n product: " + navigator.product +
        "\r\n language: " + navigator.language +
        "\r\n onLine: " + navigator.onLine ;

            return this._browser;
        }
    }

    return Browser;

})
