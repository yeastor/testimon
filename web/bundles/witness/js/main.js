require.config({
    paths: {
        jquery: '/js/jquery',
        bootstrap: '/js/bootstrap/dist/js/bootstrap.min',
        backbone: 'libs/backbone',
        underscore: 'libs/underscore-min',
        fosjsrouting: '/bundles/fosjsrouting/js/router',
        fosjsroutes: '/js/fos_js_routes',
        ymaps: 'http://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU',
        datepicker: '/js/datepicker/js/bootstrap-datepicker',
        kladr: 'libs/kladr/jquery.kladr.min',
        typeahead: '/bundles/witness/js/libs/typeahead/typeahead'

    },
    shim: {
        'fosjsrouting' : {
            exports: 'Routing'
        },
        'fosjsroutes': ['fosjsrouting'],
        'kladr': ['jquery'],
        datepicker: ['jquery'],
        'bootstrap': ['jquery'],
        'typeahead': ['jquery']

    }

});

